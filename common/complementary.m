%% Takes complement (but not reverse)

function out = complementary (dna)
	out = char([]);
	for i=1:1:length(dna)
		switch dna(i)
			case 'A'
				out = [out, 'T'];
			case 'T'
				out = [out, 'A'];
			case 'C'
				out = [out, 'G'];
			case 'G'
				out = [out, 'C'];
			
			case 'R'
				out = [out, 'Y'];
			case 'Y'
				out = [out, 'R'];
			case 'N'
				out = [out, 'N'];
			case 'B'
				out = [out, 'V'];
			case 'D'
				out = [out, 'H'];
			case 'H'
				out = [out, 'D'];
			case 'V'
				out = [out, 'B'];	
		end
	end
end

