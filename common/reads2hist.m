%%%
% Converts a .reads file to a .hist file. 
% Typically useful for insertion library results


more off;

len = 80;

for fileID = 1
	filename = sprintf('~/sil/%d.reads', fileID);
	file = fopen(filename);
	
	hists = zeros(4,len,1);
	clanID = 1;
	while ~feof(file)
		clanhist = zeros(4,len);
		
		line = fgetl(file);
		while length(line) > 10
			baseNum = dna2num(line);			
			for i=1:1:len
				if baseNum(i) ~= 0
					clanhist(baseNum(i),i)++;
				end
			end
			line = fgetl(file);
		end
		
		hists (:,:,clanID) = clanhist;
		clanID++
	end
	
	fclose(file);
	matfilename = [ filename(1:end-5) '.mat' ];
	save(matfilename, 'hists');
end


