%%

function out = drawBoard (board, outfilename, clow, chigh, libname, colorScheme, is_indel)
	repairInit;
	set(0,'DefaultAxesFontSize', 8);
	imagesc(board);
	
	if ~exist("is_indel") || is_indel == false
		set(gca,'Xtick',1:len);
		set(gca,'YTickLabel', {'A','T','C','G'} )
		set(gca,'Ytick',1:4);
	else
		set(gca, 'Xtick', 0.5:1:len)
		if strcmp(libname,"SIL") || strcmp(libname,"IL") || strcmp(libname,"SML")
			set(gca,'YTickLabel', {'A','T','C','G'} )
			set(gca,'Ytick',1:4);
		else
			set(gca,'YTickLabel', {'AA','AT','AC','AG', 'TA', 'TT', 'TC', 'TG', 'CA', 'CT', 'CC', 'CG', 'GA', 'GT', 'GC', 'GG'} )
			set(gca,'Ytick',1:16);
		end
	end
	
	set(gca,'XTickLabel', seqLabel)
	set(gca,'TickLength',[0 0]);
	axis equal tight
	
	set(gca, 'LineWidth', 1);
	if exist('clow') && exist('chigh') && length(clow) ~= 0 && length(chigh) ~= 0
		caxis([clow chigh]);
	end	
	
	if exist('colorScheme') && strcmp(colorScheme, 'redBlue')
		colormap (bluewhitered(256));
	elseif exist('colorScheme') && strcmp(colorScheme, 'hsv')
		colormap(hsv(15))
	else
		colormap(hot(256));
	end

	
	% Puts a hatching pattern on the black entries in images that corresponds to NaN values.
	% so that they are distinguishable from dar blue/dark red.
	print("./temp.png", '-r300');
	in = imread("./temp.png");
	delete ./temp.png
	collapsed = sum(in,3);
	binary = (collapsed<=1.2*min(min(collapsed)));
	
	% Trimming of the extra white space
	boundary = (collapsed<(min(min(collapsed))+0.2*mean(mean(collapsed))));
	xsum = find(sum(boundary,2));
	ysum = find(sum(boundary,1));
	padding = 40;
	acceptRangex = max(1,min(xsum)-padding):min(max(xsum)+2*padding,size(binary,1));
	acceptRangey = max(1,min(ysum)-2*padding):min(max(ysum)+padding,size(binary,2));
	
	n = 20;
	mask = ones(n,n);
	filtered = conv2(binary,mask,'same');
	filtered = (filtered>250);

	% generate hatching pattern
	[xx,yy] = meshgrid(1:size(in,2),1:size(in,1));
	hatching = (mod(xx-yy, 10)<4);
	red = in(:,:,1);
	red(filtered) = 255*hatching(filtered);
	green = in(:,:,2);
	green(filtered) = 255*hatching(filtered);
	blue = in(:,:,3);
	blue(filtered) = 255*hatching(filtered);

	out = zeros(length(acceptRangex),length(acceptRangey));
	out(:,:,1) = red(acceptRangex,acceptRangey);
	out(:,:,2) = green(acceptRangex,acceptRangey);
	out(:,:,3) = blue(acceptRangex,acceptRangey);
	
	imwrite(uint8(out), outfilename);
end


