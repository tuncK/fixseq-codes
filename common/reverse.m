%% Takes the reverse of a sequence (but not complement)

function out = reverse (dna)
	out = char([]);
	for i=1:1:length(dna)
		out(i) = dna(length(dna)-i+1);
	end
end

