%%

function out = dna2num (dna)
	out = zeros(length(dna),1);
	for i=1:1:length(dna)
		switch dna(i)
			case 'N'
				out(i) = 0;
			case 'A'
				out(i) = 1;
			case 'U'
				out(i) = 2;
			case 'T'
				out(i) = 2;
			case 'C'
				out(i) = 3;
			case 'G'
				out(i) = 4;
		end
	end
end

