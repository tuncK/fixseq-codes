%%%
% Compares the expected MM type or position based on the lookup barcodes
% with the most likely observed mismatch based on the most substituted
% base/position in the clan base frequency histogram.

more off;
addpath ./common

libname = 'SIL';
repairInit;



histsCumulated = [];
for fileID = 1:3
	% Load the hist file with clan histograms
	filename = sprintf('~/june2020/sample%d.hist', fileID);
	load(filename); % source of the "hists" variable, which contains the data

	histsCumulated = cat(3,histsCumulated,hists);
end
clear hists;


% Import secondary barcode map
if ~exist('doubleBarcodeMap.mat') || ~exist('mappedLibname') || ~strcmp(mappedLibname,libname)
	importBarcodeMap;
else
	load doubleBarcodeMap.mat
end


numReadsList = sum(hists(:,1,:),1);
seqDeepEnough = squeeze(numReadsList >= 10)';
if sum(~seqDeepEnough) ~= 0
	fprintf("%d small clans detected and ignored. Something is fishy.\n", sum(~seqDeepEnough) );
end
hists = hists(:,:,seqDeepEnough);
numReadsList = sum(hists(:,1,:),1);
numHists = size(hists,3);

% Calculate the average 2nd barcode for each read.
[~,barcodeMaxima] = max(hists(:,1:barcodeLength,:),[],1);
barcodeMaxima = int8(squeeze(barcodeMaxima)');

differences = hists(:,barcodeLength+1:end,:)./numReadsList - original;
maxChanges = squeeze(max(differences, [], 1));
[maxSubstitutionFreq, maxSubstitutionPos] = max(maxChanges, [], 1);


% Go through the hist file
crosstalkPos = zeros(len,len);
crosstalkBase = zeros(4,4);

for i=1:1:numHists
	% Lookup the barcode table for the 2nd barcode
	realID = find(sum(barcodeTable==barcodeMaxima(i,:),2) == barcodeLength);
	
	if maxSubstitutionFreq(i) > 0.1 && ~isempty(realID)
		% If no hit found, skip.
		% i.e., would be recorded either as U or V event.
		realPos = insertionTable(realID,1);
		realBase = insertionTable(realID,2);
		pos = maxSubstitutionPos(i);
		[~,base] = max(differences(:,pos,i));
		
		crosstalkBase(realBase,base)++;
		crosstalkPos(realPos,pos)++;	
	end
end

% normalise the confusion matrices row by row
crosstalkBase = crosstalkBase ./ sum(crosstalkBase,2);
crosstalkPos = crosstalkPos ./ sum(crosstalkPos,2);


% Print assay accuracy statistics
set(0, 'defaultaxesfontsize', 25)
close all;
pause(0.01);
imagesc(crosstalkBase);
set(gca,'Ydir','normal');
axis square;
colormap(flipud(gray));
xlabel('Detected base');
ylabel('Expected base');
caxis([0 1]);
set(gca,'Xtick',1:len);
set(gca,'XTickLabel',  {'A','T','C','G'})
set(gca,'Ytick',1:4);
set(gca,'YTickLabel', {'A','T','C','G'} )
set(gca,'TickLength',[0 0]);

print('~/SIL-InsType.png', '-r300')


close all;
pause(0.01);
imagesc(crosstalkPos);
set(gca,'Ydir','normal');
axis square;
colormap(flipud(gray));
caxis([0 1]);
xlabel('Detected Ins. position');
ylabel('Expected Ins. position');

print('~/SIL-InsPosition.png', '-r300')


