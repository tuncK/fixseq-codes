%%
% Compares the number of clans per each MM in different libraries.

avgClanCount = zeros(14,1);
stdevClanCount = avgClanCount;
avgReprate = avgClanCount;
stdevReprate = avgClanCount;


allClanCounts = [];
allReprates = [];

for i=3:2:29
	filename = sprintf('~/5MLx matfiles/combined%d-%d.mat', i, i+1);
	load(filename); % load mat file with clan histograms
	
	total = repairedF + repairedV + unrepaired;
	total = reshape(total,1,[]);
	
	reprate = reshape(reprate, 1, []);
	backgroundEfficiency = 0.37398; % minumum repair efficiency of combined3-4.mat for 3ML1 mutS deficient.
	reprate = (reprate - backgroundEfficiency) / (1-backgroundEfficiency);
	
	valid = (~isnan(total) & ~isnan(reprate));
	total = total(valid);
	reprate = reprate(valid);
	allClanCounts = [ allClanCounts, total];
	allReprates = [ allReprates, reprate];
	
	id = (i-1)/2;
	avgClanCount(id) = mean(total);
	stdevClanCount(id) = std(total,1) / sqrt(length(total));
	
	avgReprate(id) = mean(reprate);
	stdevReprate(id) = std(reprate,1) / sqrt(length(reprate));
end

trimerReprate = avgReprate(1)
pentamerReprate = mean(avgReprate([2:8 10:end]))
trimerCount = avgClanCount(1)
pentamerCount = mean(avgClanCount([2:8 10:end]))
efficiencyChange = (pentamerReprate-trimerReprate) / pentamerReprate


set(0, 'defaultaxesfontsize', 22);

% Comparison of number of clans with measured repair efficiency
close all
h(1) = plot(avgClanCount, avgReprate, 'ro', 'markersize', 12);
hold on
h(2) = plot(avgClanCount(1), avgReprate(1), 'bo', 'markersize', 12);
errorbar(avgClanCount, avgReprate, stdevClanCount, stdevReprate, '~>k.');
hold off
set(h, 'markerfacecolor', 'auto');

pearsonR = ( mean((avgClanCount-mean(avgClanCount)).*(avgReprate-mean(avgReprate))) / (std(avgClanCount)*std(avgReprate)) );
xlims = get(gca, 'xlim');
ylims = get(gca, 'ylim');
text(mean(xlims), 0.95*ylims(2), sprintf('\\rho = %.2f', pearsonR), 'fontsize', 25);

p = polyfit(avgClanCount, avgReprate, 1);
yfit = polyval(p, xlims(1):xlims(2));
hold on;
plot(xlims(1):xlims(2), yfit, 'k-', 'linewidth', 1.5);
hold off;

xlabel('Mean #clans / MM');
ylabel('Scaled repair efficiency (\eta_s)');

HLEG = legend(h, '5MLx', '3ML1');
legend boxoff;
legend('location', 'south', 'orientation', 'horizontal');

lchildren = get(HLEG, 'children');
set(lchildren(3), "color", get(lchildren(1),'color') );
set(lchildren(4), "color", get(lchildren(2),'color') );

print('~/numClansPerLibrary.png', '-r300');



% Comparison of repair efficiency vs clan size for individual mismatches
close all
plot(allClanCounts, allReprates, 'r.', 'markersize', 10);

pearsonR = ( mean((allClanCounts-mean(allClanCounts)).*(allReprates-mean(allReprates))) / (std(allClanCounts)*std(allReprates)) );
ylim([0 1.05]);
xlims = get(gca, 'xlim');
text(mean(xlims), 0.8, sprintf('\\rho = %.2f', pearsonR), 'fontsize', 25);

p = polyfit(allClanCounts, allReprates, 1);
yfit = polyval(p, xlims(1):xlims(2));
hold on;
plot(xlims(1):xlims(2), yfit, 'k-', 'linewidth', 1.5);
hold off;

xlabel('Mean #clans / MM');
ylabel('Scaled repair efficiency (\eta_s)');
print('~/numClansVSetas.png', '-r300');




if false
	bar(avgClanCount, 'r', 'edgecolor', 'none');
	hold on;
	h = errorbar(1:14, avgClanCount, stdevClanCount, 'k.');
	set(h, 'linewidth', 2);
	hold off;

	xlim([0 15]);
	set(gca, 'xticklabel', {} );

	labels = {};
	labels{1} = '3ML1';
	for i=2:14
		labels{i} = sprintf('5ML%d', i-1);
	end
	text(1:14, 1500*ones(14,1), labels(1:14), "rotation", 90, "fontsize", 20, "color", "k");

	print('~/numClansPerMM.png', '-r300');
end


