%%
% Generates a plot about random probability of capturing the same barcode


% Probability of having at least a pair accidentally clashing with same barcode
% if error tolerance of DBSCAN is epsilon=0
numClans = 1e5;
barcodeLength = 25;
prob = zeros(4,numClans);
for epsilon =0:1:3
	terms = 1 - (1+nchoosek(barcodeLength,epsilon) * (4^epsilon-1)) * (0:1:(numClans-1))/4^barcodeLength;
	prob(epsilon+1,:) = 1  - exp(cumsum( log( terms ) ));
end

bins = 1:1000:numClans;
prob = prob(:,bins);

close all;
set(0, 'defaultaxesfontsize', 22);
plot(bins/1000,log10(prob(1,:)), 'r-', 'linewidth', 2);
hold on;
plot(bins/1000,log10(prob(2,:)), 'g-', 'linewidth', 2);
plot(bins/1000,log10(prob(3,:)), 'b-', 'linewidth', 2);
plot(bins/1000,log10(prob(4,:)), 'k-', 'linewidth', 2);
hold off;

xlabel('Number of clans (1000)');
ylabel('log_{10}(Confusion probability)');
HLEG = legend('\epsilon=0', '\epsilon=1', '\epsilon=2', '\epsilon=3');
legend('location', 'south');
legend boxoff;
legend('orientation', 'horizontal');

lchildren = get(HLEG, 'children');
set(lchildren(5), "color", get(lchildren(1),'color') );
set(lchildren(6), "color", get(lchildren(2),'color') );
set(lchildren(7), "color", get(lchildren(3),'color') );
set(lchildren(8), "color", get(lchildren(4),'color') );

print('~/barcodeClashProbability.png', '-r300');



% Prob that two random barcodes are identical
barcodeLength = 5:30;
prob = zeros(4,length(barcodeLength));
for epsilon = 0:1:3
	for j=barcodeLength
		prob(epsilon+1,j-4) = nchoosek(j,epsilon) * 4^epsilon / 4^j;
	end
end

close all;
set(0, 'defaultaxesfontsize', 22);
plot(barcodeLength, log10(prob(1,:)), 'r-', 'linewidth', 2);
hold on;
plot(barcodeLength, log10(prob(2,:)), 'g-', 'linewidth', 2);
plot(barcodeLength, log10(prob(3,:)), 'b-', 'linewidth', 2);
plot(barcodeLength, log10(prob(4,:)), 'k-', 'linewidth', 2);
hold off;

xlabel('Barcode length (L)');
ylabel('log_{10}(Confusion probability)');
HLEG = legend('\epsilon=0', '\epsilon=1', '\epsilon=2', '\epsilon=3');
legend('location', 'south');
legend boxoff;
legend('orientation', 'horizontal');

lchildren = get(HLEG, 'children');
set(lchildren(5), "color", get(lchildren(1),'color') );
set(lchildren(6), "color", get(lchildren(2),'color') );
set(lchildren(7), "color", get(lchildren(3),'color') );
set(lchildren(8), "color", get(lchildren(4),'color') );

print('~/barcodeIdenticalProbability.png', '-r300');



return

% Check the number of clans per sample
num = zeros(26,1);
for fileID = 5:30
	filename = sprintf('~/consolidated 5MLx hists/sample%d.hist', fileID)
	file = fopen(filename);
	for i=1:4
		fgetl(file);
	end
	num(fileID-4) = fscanf(file, "%*d %*d %d\n",1);
	fclose(file);
end

mean(num)
std(num,1)



