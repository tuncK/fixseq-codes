%%%
% Compares the substitution prevalence (s) between 
% SIL/DIL samples

more off;

sbins = -0.2:0.05:1.2;
histResults = zeros(7,length(sbins));
for fileID = 1:4
	% load the csv file with prevalence values
	filename = sprintf('~/sil/%d.csv', fileID);
	file = fopen(filename);
	prevalences = fscanf(file, '%f', [1,inf]);
	fclose(file);

	histResults(fileID,:) = histc(prevalences, sbins);
end

% Control library samples
id = fileID;
for fileID = [11 12 18]
	% load the csv file with prevalence values
	filename = sprintf('~/sil/3cl1/%d.csv', fileID);
	file = fopen(filename);
	prevalences = fscanf(file, '%f', [1,inf]);
	fclose(file);

	id++;
	histResults(id,:) = histc(prevalences, sbins);
end

histResultsNorm = histResults ./ sum(histResults,2);

set(0,"defaultaxesfontsize",25);
close all;
h(1) = plot(sbins, histResultsNorm(1:3,:), "r-", "linewidth",1.5)(1);
hold on;
h(2) = plot(sbins, histResultsNorm(4,:), "b--", "linewidth",1.5)(1);
h(3) = plot(sbins, histResultsNorm(5:7,:), "k:", "linewidth",1.5)(1);
hold off;

xlabel("Insertion prevalence")
ylabel("Frequency");
xlim([-0.05 1.05]);

HLEG = legend(h, "wt", '\DeltamutS', 'wt-3CL1');
legend("location", "north")
legend boxoff;
lchildren = get(HLEG, 'children');
set(lchildren(4), "color", get(lchildren(1),'color') );
set(lchildren(5), "color", get(lchildren(2),'color') );
set(lchildren(6), "color", get(lchildren(3),'color') );

print("~/SILclanPrevalences.png", "-r300")


