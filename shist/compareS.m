%%%
% Compares the substitution prevalence (s) between 
% methylated/unmethylated samples.

more off;

libname = "3ML1";
repairInit;
barcodeLength = 7;

sbins = -0.2:0.05:1.2;
histResults = zeros(10,length(sbins));
rowid = 1;

for fileID = [ 1:6 11:12 17:18]
	% Load the hist file with clan histograms
	filename = sprintf('~/consolidated 3ML1 hists/sample%d.hist', fileID)
	load(filename); % source of the "hists" variable, which contains the data
	
	numReadsList = sum(hists(:,1,:),1);
	seqDeepEnough = squeeze(numReadsList >= 10)'; % Should have been unnecessary
	if sum(~seqDeepEnough) ~= 0
		fprintf("%d small clans detected and ignored. Something is fishy.\n", sum(~seqDeepEnough) );
	end
	hists = single(hists(:,:,seqDeepEnough));
	numReadsList = sum(hists(:,1,:),1);
	frequencies = ( hists ./ numReadsList );
	
	% Calculate the substitution position and type
	differences = frequencies(:,barcodeLength+1:end,:) - original;
	maxChanges = squeeze(max(differences, [], 1));
	[maxSubstitutionFreq, maxSubstitutionPos] = max(maxChanges, [], 1);
	
	histResults(rowid,:) = histc(maxSubstitutionFreq, sbins);
	rowid++;
end


histResultsNorm = histResults ./ sum(histResults,2);
% row 1,2: wt t19
% row 3,4: mutS t19
% row 5,6: wt hm19
% row 7,8: muts hm19
% row 9,10: wt mm19


set(0,"defaultaxesfontsize",25);

% Plot for wt
close all;
h(1) = plot(sbins, histResultsNorm(1:2,:), "k-.", "linewidth", 1.5)(1);
hold on;
h(2) = plot(sbins, histResultsNorm(5:6,:), "r-", "linewidth", 1.5)(1);
h(3) = plot(sbins, histResultsNorm(9:10,:), "b--", "linewidth", 1.5)(1);
hold off;

xlabel("Substitution prevalence")
ylabel("Frequency");
xlim([-0.05 1.05]);
HLEG = legend(h, "Unmethylated", 'Hemi-methylated', "Fully-methylated");
legend("location", "north")
legend boxoff;

lchildren = get(HLEG, 'children');
for i=1:1:3
	set(lchildren(i+3), "color", get(lchildren(i),'color') );
end
print("~/wt.png", "-r300")


% Plot for mutS
close all;
h(1) = plot(sbins, histResultsNorm(3:4,:), "k-.", "linewidth", 1.5)(1);
hold on;
h(2) = plot(sbins, histResultsNorm(7:8,:), "r-", "linewidth", 1.5)(1);
hold off;

xlabel("Substitution prevalence")
ylabel("Frequency");
xlim([-0.05 1.05]);
HLEG = legend(h, "Unmethylated", 'Hemi-methylated');
legend("location", "north")
legend boxoff;

lchildren = get(HLEG, 'children');
for i=1:1:2
	set(lchildren(i+2), "color", get(lchildren(i),'color') );
end
print("~/muts.png", "-r300")


