%%%
% Compares the substitution prevalence (s) between 
% DEB3L and DEB3R samples


addpath ./common/

more off;

sbins = -0.2:0.05:1.2;
histResults = zeros(4,length(sbins)); % by using data from all positions
histResultsFirst = histResults; % first 20 MMs only
histResultsLast = histResults; % last 20 MMs only

fileCounter = 1;
for fileID = {'L7', 'L8', 'R7', 'R8'}
	% Load the hist file with clan histograms
	filename = sprintf('~/deb/%s.hist', fileID{})
	load(filename); % source of the "hists" variable, which contains the data
	
	libname = ["DEB3" fileID{}(1)];
	repairInit;	
	
	numReadsList = sum(hists(:,1,:),1);
	seqDeepEnough = squeeze(numReadsList >= 10)'; % Should have been unnecessary
	if sum(~seqDeepEnough) ~= 0
		fprintf("%d small clans detected and ignored. Something is fishy.\n", sum(~seqDeepEnough) );
	end
	hists = single(hists(:,:,seqDeepEnough));
	numReadsList = sum(hists(:,1,:),1);
	frequencies = ( hists ./ numReadsList );
	
	% Calculate the substitution position and type
	differences = frequencies - original;
	maxChanges = squeeze(max(differences, [], 1));
	[maxSubstitutionFreq, maxSubstitutionPos] = max(maxChanges, [], 1);
	
	histResults(fileCounter,:) += histc(maxSubstitutionFreq, sbins);
	histResultsFirst(fileCounter,:) += histc(maxSubstitutionFreq(maxSubstitutionPos<=20), sbins);
	histResultsLast(fileCounter,:) += histc(maxSubstitutionFreq(maxSubstitutionPos>=41), sbins);
	fileCounter++;
end


histResultsNorm = histResults ./ sum(histResults,2);
histResultsFirstNorm = histResultsFirst ./ sum(histResultsFirst,2);
histResultsLastNorm = histResultsLast ./ sum(histResultsLast,2);

% Which of the 3 options to plot?
hist2plot = histResultsLastNorm;

set(0,"defaultaxesfontsize",25);
close all;
h(1) = plot(sbins, hist2plot(1:2,:), "r-", "linewidth",1.5)(1);
hold on;
h(2) = plot(sbins, hist2plot(3:4,:), "b--", "linewidth",1.5)(1);
hold off;

xlabel("Substitution prevalence")
ylabel("Frequency");
xlim([-0.05 1.05]);
HLEG = legend(h, "DEB3L", 'DEB3R');
legend("location", "north")
legend boxoff;

lchildren = get(HLEG, 'children');
set(lchildren(3), "color", get(lchildren(1),'color') );
set(lchildren(4), "color", get(lchildren(2),'color') );


skew = skewness(hist2plot)

%print("~/last20.png", "-r300")


