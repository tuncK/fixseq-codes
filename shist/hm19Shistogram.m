%%%
% Compares the substitution prevalence (s) between different MMR mutants

more off;

addpath ../common
libname = "3ML1";
repairInit;
barcodeLength = 7;


sbins = -0.2:0.05:1.2;
histResults = zeros(1,length(sbins));
fileNum = 1;

% dam-t19 dam-hm19, mutH-t19, mutH-hm
for fileID = [ 9:10 1 2 5 6 11:12]
	% Load the hist file with clan histograms
	filename = sprintf('~/marccOutput/%d.hist', fileID)
	load(filename); % source of the "hists" variable, which contains the data
	
	numReadsList = sum(hists(:,1,:),1);
	seqDeepEnough = squeeze(numReadsList >= 10)'; % Should have been unnecessary
	hists = single(hists(:,:,seqDeepEnough));
	numReadsList = sum(hists(:,1,:),1);
	frequencies = ( hists ./ numReadsList );
	
	% Calculate the substitution position and type
	differences = frequencies(:,barcodeLength+1:end,:) - original;
	maxChanges = squeeze(max(differences, [], 1));
	[maxSubstitutionFreq, maxSubstitutionPos] = max(maxChanges, [], 1);
	
	histResults(fileNum,:) = histc(maxSubstitutionFreq, sbins);
	fileNum++;
end

histResultsNorm = histResults ./ sum(histResults,2);


set(0,"defaultaxesfontsize",25);
close all;
h(1) = plot(sbins, histResultsNorm(1:2,:), "r-", "linewidth",1)(1);
hold on;
h(2) = plot(sbins, histResultsNorm(3:4,:), "m--", "linewidth",1.5)(1);
h(3) = plot(sbins, histResultsNorm(5:6,:), "b-", "linewidth",1.5)(1);
h(4) = plot(sbins, histResultsNorm(7:8,:), "g:", "linewidth",2)(1);
hold off;

xlabel("Substitution prevalence")
ylabel("Frequency");
xlim([-0.05 1.05]);

HLEG = legend(h, '\Deltadam', '\Deltadam-hemi', '\DeltamutH', '\DeltamutH-hemi');
legend("location", "north")
legend boxoff;

lchildren = get(HLEG, 'children');
for i=1:4
	set(lchildren(i+4), "color", get(lchildren(i),'color') );
end

print("~/mutantsShist.png", "-r300")


