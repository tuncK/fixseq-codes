%%
% Plots a clan size histogram for MMR mutants measured with SML

% wt mutS mutL mutH uvrD
fileIDstart = [ 74, 90, 95, 81, 99  ];
fileIDend = [ 77, 91, 98, 83, 101 ];

colors = {'r', 'm', 'g', 'b', 'k'};
bins = 1:10:300;

close all;
set(0, 'defaultaxesfontsize', 22);
hold on;

for i=1:length(fileIDstart)
	sizes = [];
	for fileID = fileIDstart(i):fileIDend(i)
		% Load the hist file with clan histograms
		filename = sprintf('~/SML/sample%d.hist', fileID)
		load(filename); % source of the "hists" variable, which contains the data
		
		tempsizes = squeeze(sum(hists(:,1,:),1));
		sizes = [sizes; tempsizes];
	end

	pdf = histc(sizes,bins) / length(sizes);
	cdf = cumsum(pdf);
	endpoint = find(cdf>0.99)(1);
	
	plot(bins(1:endpoint), cdf(1:endpoint), 'linewidth', 2, 'color', colors{i} );
end

xlim([0 120])
xlabel('Clan size');
ylabel('CDF');

HLEG = legend("wt", '\DeltamutS', '\DeltamutL', '\DeltamutH', '\DeltauvrD');
legend("location", "southeast")
legend boxoff;

lchildren = get(HLEG, 'children');
for i=1:5
	set(lchildren(i+5), "color", get(lchildren(i),'color') );
end


print('~/clansizedist.png', '-r300');


