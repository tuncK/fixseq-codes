%%%
% Compares the substitution prevalence (s) between 
% different mutants


more off;

addpath ../common
libname = "SML";
repairInit;


sbins = -0.2:0.05:1.2;
histResults = zeros(1,length(sbins));
fileNum = 1;

% wt mutH mutS mutL uvrD
for fileID = [ 74:77 81:83 90:91 95:98 99:101 ]
	% Load the hist file with clan histograms
	filename = sprintf('~/mutants/sample%d.hist', fileID)
	load(filename); % source of the "hists" variable, which contains the data
	
	numReadsList = sum(hists(:,1,:),1);
	seqDeepEnough = squeeze(numReadsList >= 10)'; % Should have been unnecessary
	if sum(~seqDeepEnough) ~= 0
		fprintf("%d small clans detected and ignored. Something is fishy.\n", sum(~seqDeepEnough) );
	end
	hists = single(hists(:,:,seqDeepEnough));
	numReadsList = sum(hists(:,1,:),1);
	frequencies = ( hists ./ numReadsList );
	
	% Calculate the substitution position and type
	differences = frequencies - original;
	maxChanges = squeeze(max(differences, [], 1));
	[maxSubstitutionFreq, maxSubstitutionPos] = max(maxChanges, [], 1);
	
	histResults(fileNum,:) = histc(maxSubstitutionFreq, sbins);
	fileNum++;
end

histResultsNorm = histResults ./ sum(histResults,2);


set(0,"defaultaxesfontsize",25);
close all;
h(1) = plot(sbins, histResultsNorm(1:4,:), "r-", "linewidth",1)(1);
hold on;
h(2) = plot(sbins, histResultsNorm(8:9,:), "m--", "linewidth",1.5)(1);
h(3) = plot(sbins, histResultsNorm(10:13,:), "g:", "linewidth",1.5)(1);
h(4) = plot(sbins, histResultsNorm(5:7,:), "b-", "linewidth",2)(1);
h(5) = plot(sbins, histResultsNorm(14:end,:), "k-.", "linewidth",1.5)(1);
hold off;

ylim([0 0.31]);
xlabel("Substitution prevalence")
ylabel("Frequency");
xlim([-0.05 1.05]);

HLEG = legend(h, "wt", '\DeltamutS', '\DeltamutL', '\DeltamutH', '\DeltauvrD');
legend("location", "north")
legend boxoff;

lchildren = get(HLEG, 'children');
for i=1:5
	set(lchildren(i+5), "color", get(lchildren(i),'color') );
end

print("~/mutantsShist.png", "-r300")


