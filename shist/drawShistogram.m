%%%
% Compares the substitution prevalence (s) between 
% 3CL1, wt 3ML1 and mutS 3ML1 samples


more off;

libname = "3ML1";
repairInit;
barcodeLength = 7;


sbins = -0.2:0.05:1.2;
histResults = zeros(1,length(sbins));
histResultsBelow = histResults; % below median
histResultsAbove = histResults; % above median

medianThreshold = 27;


for fileID = [ 5:30 ]
	% Load the hist file with clan histograms
	filename = sprintf('~/consolidated 5MLx hists/sample%d.hist', fileID)
	load(filename); % source of the "hists" variable, which contains the data

	libname = sprintf("5ML%d", ceil((fileID-4)/2));
	repairInit;
	barcodeLength = 6;
	
	numReadsList = sum(hists(:,1,:),1);
	seqDeepEnough = squeeze(numReadsList >= 10)'; % Should have been unnecessary
	hists = single(hists(:,:,seqDeepEnough));
	numReadsList = sum(hists(:,1,:),1);
	frequencies = ( hists ./ numReadsList );
	
	% Calculate the substitution position and type
	differences = frequencies(:,barcodeLength+1:end,:) - original;
	maxChanges = squeeze(max(differences, [], 1));
	[maxSubstitutionFreq, maxSubstitutionPos] = max(maxChanges, [], 1);
	
	sDistribution = histc(maxSubstitutionFreq, sbins);
	histResults += sDistribution;
	
	sDistribution = histc(maxSubstitutionFreq(numReadsList>medianThreshold), sbins);
	histResultsAbove += sDistribution;
	
	sDistribution = histc(maxSubstitutionFreq(numReadsList<medianThreshold), sbins);
	histResultsBelow += sDistribution;
end


histResultsNorm = histResults ./ sum(histResults,2);
histResultsNormAbove = histResultsAbove ./ sum(histResultsAbove,2);
histResultsNormBelow = histResultsBelow ./ sum(histResultsBelow,2);

if false
	set(0,"defaultaxesfontsize",25);
	close all;
	h(1) = plot(sbins, histResultsNorm(1:2,:), "r-", "linewidth",1.5)(1);
	hold on;
	h(2) = plot(sbins, histResultsNorm(3:4,:), "b--", "linewidth",1.5)(1);
	h(3) = plot(sbins, histResultsNorm(22:24,:), "k-.", "linewidth",1.5)(1);
	hold off;

	xlabel("Substitution prevalence")
	ylabel("Frequency");
	xlim([-0.05 1.05]);
	HLEG = legend(h, "3ML1 wt", '3ML1 \DeltamutS', "3CL1 wt");
	legend("location", "north")
	legend boxoff;

	lchildren = get(HLEG, 'children');
	set(lchildren(4), "color", get(lchildren(1),'color') );
	set(lchildren(5), "color", get(lchildren(2),'color') );
	set(lchildren(6), "color", get(lchildren(3),'color') );
else
	set(0,"defaultaxesfontsize",25);
	close all;
	h(1) = plot(sbins, histResultsNormAbove, "m:", "linewidth", 2);
	hold on;
	h(2) = plot(sbins, histResultsNorm, "k-", "linewidth", 1.5);
	h(3) = plot(sbins, histResultsNormBelow, "g--", "linewidth", 1.5);
	hold off;

	xlabel("Substitution prevalence")
	ylabel("Frequency");
	xlim([-0.05 1.05]);
	HLEG = legend(h, "Above", 'Entire', "Below");
	legend("location", "north")
	legend boxoff;

	lchildren = get(HLEG, 'children');
	set(lchildren(4), "color", get(lchildren(1),'color') );
	set(lchildren(5), "color", get(lchildren(2),'color') );
	set(lchildren(6), "color", get(lchildren(3),'color') );
end

print("~/wt5MLxbins.png", "-r300")


