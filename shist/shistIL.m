%%%
% s-histogram analysis for insertion library



sbins = -0.2:0.05:1.2;
histResults = zeros(12,length(sbins));
for fileID = 7:12
	% load the csv file with F/V strand counts
	filename = sprintf('~/IL-MARCCoutput/reads/sample%d.csv', fileID);
	file = fopen(filename);
	prevalence = fscanf(file, "%f", [1,inf]);
	fclose(file);
	
	histResults(fileID,:) = histc(prevalence, sbins);
end

for fileID = 4:9
	% load the csv file with F/V strand counts
	filename = sprintf('~/IL-MARCCoutput/NPL/csf/sample%d.csv', fileID);
	file = fopen(filename);
	prevalence = fscanf(file, "%f", [1,inf]);
	fclose(file);
	
	histResults(fileID-3,:) = histc(prevalence, sbins);
end

histResultsNorm = histResults ./ sum(histResults,2);


set(0,"defaultaxesfontsize",25);
close all;
h(1) = plot(sbins, histResultsNorm(7:9,:), "r-", "linewidth",1.5)(1);
hold on;
h(2) = plot(sbins, histResultsNorm(10:12,:), "b--", "linewidth",1.5)(1);
h(4) = plot(sbins, histResultsNorm(4:6,:), "g-.", "linewidth",1.5)(1);
h(3) = plot(sbins, histResultsNorm(1:3,:), "k:", "linewidth",1.5)(1);
hold off;

xlabel("Insertion prevalence")
ylabel("Frequency");
xlim([-0.05 1.05]);
HLEG = legend(h, "wt", '\DeltamutS', 'wt-NPL', '\DeltamutS-NPL');
legend("location", "north")
legend boxoff;

lchildren = get(HLEG, 'children');
set(lchildren(5), "color", get(lchildren(1),'color') );
set(lchildren(6), "color", get(lchildren(2),'color') );
set(lchildren(7), "color", get(lchildren(3),'color') );
set(lchildren(8), "color", get(lchildren(4),'color') );


print("~/ILclanPrevalences.png", "-r300")


