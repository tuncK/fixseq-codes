%%

addpath ./common/
more off;

filename = "~/dam.mat";

allMMs = {};
efficiency = [];
% Set the parameters depending on the chosen library
libname = '3ML1';
repairInit;
fixedStrand = [fixedStrand "GAGCTC"]; % Add the information about the fixed XhoI cut site.

load(filename);
backgroundEfficiency = 0*0.37398; % minumum repair efficiency of combined3-4.mat for 3ML1 mutS deficient.
reprate = (reprate - backgroundEfficiency) / (1-backgroundEfficiency);

for pos=1:1:len
	for base = 1:1:4
		if isnan(reprate(base,pos))
			continue
		end
		
		allMMs{end+1} = [ fixedStrand(pos) '-' num2dna(base) ];
		efficiency = [ efficiency; reprate(base,pos) ];
	end
end


% Label the mismatches w.r.t. their MM type or context and plot each in a different colour based on the sequence context
set(0, 'defaultaxesfontsize', 22);
MMtypes = {"AA", "AC", "AG", "CC", "CT", "GG", "GT", "TT" };

close all;
hold on;
for i=1:8
	hits1 = regexp(allMMs, sprintf("%s-%s", MMtypes{i}(1), MMtypes{i}(2)) );
	hits2 = regexp(allMMs, sprintf("%s-%s", MMtypes{i}(2), MMtypes{i}(1)) );
	selection = ( (~cellfun(@isempty,hits1)) | (~cellfun(@isempty,hits2)) )';

	selectedRates = efficiency(selection,1);
	numSelected = length(selectedRates);
	avg = mean(selectedRates);
	dev = std(selectedRates,1);
	selectedRates(selectedRates<0) = 0;
	
	switch i
		case 1
			h(i) = plot( i*ones(numSelected,1)+0.4*rand(numSelected,1)-0.2 , selectedRates, "ro", "linewidth", 1.5);
		case 2
			h(i) = plot( i*ones(numSelected,1)+0.4*rand(numSelected,1)-0.2 , selectedRates, "b*", "linewidth", 1.5);
		case 3
			h(i) = plot( i*ones(numSelected,1)+0.4*rand(numSelected,1)-0.2 , selectedRates, "gd", "linewidth", 1.5);
		case 4
			h(i) = plot( i*ones(numSelected,1)+0.4*rand(numSelected,1)-0.2 , selectedRates, "mp", "linewidth", 1.5);
		case 5
			h(i) = plot( i*ones(numSelected,1)+0.4*rand(numSelected,1)-0.2 , selectedRates, "^", "Color", [0.9 0.5 0], "linewidth", 1.5);
		case 6
			h(i) = plot( i*ones(numSelected,1)+0.4*rand(numSelected,1)-0.2 , selectedRates, "k>", "linewidth", 1.5);
		case 7
			h(i) = plot( i*ones(numSelected,1)+0.4*rand(numSelected,1)-0.2 , selectedRates, "cv", "linewidth", 1.5);
		case 8
			h(i) = plot( i*ones(numSelected,1)+0.4*rand(numSelected,1)-0.2 , selectedRates, "<", "Color", [0.5 0.5 0.5], "linewidth", 1.5);
	end
	h2(i)=errorbar(i, avg, dev, 'k^');
end

set(h2(6), "color", [0.9 1 0])
set(h2, 'linewidth', 2, "markerfacecolor", "auto");
set(h,"markerfacecolor","auto")	

hold off;
xlabel('Mismatch Type');
ylabel('Repair efficiency (\eta)');
set(gca,'XTick', 1:8)
set(gca,'XTickLabel', MMtypes)
xlim([0 9]);
ylim([0 1.1]);
pause(0.1)
print('~/typesEfficiency.png','-r300');


