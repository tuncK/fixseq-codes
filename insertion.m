%%%
% Analysis for insertion library

temphists = [];
for fileID = 33:35
	filename = sprintf('~/IL/sample%d.hist', fileID)
	load(filename);
	temphists = cat(3,temphists,hists);
end
hists = temphists;


addpath ./common/

libname = "SML";
repairInit;

lowCutoff = 0.1;
highCutoff = 1-lowCutoff;


% Find the ambiguity relation
ambiguities = zeros(4,len-1);
variantVec = zeros(4*len, 4*len); %(basePos, hashID)
classID = 0;
for i=1:1:4
	for j=1:1:len-1
		if j==1 || original(i,j) ~= 1
			classID++;
			temp = zeros(4,len);
			temp(:,1:j) = original(:,1:j);
			temp(i,j+1) = 1;
			temp(:,(j+2):end) = original(:,j+1:end-1);
			variantVec(:,classID) = reshape(temp,1,[]);
		end
		ambiguities(i,j) = classID;
	end
end


% Plot a map of ambiguious zones
if false
	random = rand(max(max(ambiguities)),1);
	out = random(ambiguities);
	drawBoard(out, '/home/tk/insertionAmbiguity.png', [], [], libname, 'hsv', 1);
	return;
end


% Necessary packages for fitting
pkg load optim;
originalVec = reshape(original,1,[]);
deviationFunc = @(p, t) p*t + (1-p)*originalVec;

repairedVec = zeros(classID,1);
unrepairedVec = repairedVec;

lowquality = 0;
numHists = size(hists,3);

for i=1:1:numHists
	block = hists(:,:,i);
	numreads = sum(block(:,1));
	if numreads < 10
		continue;
	end	
	
	block /= numreads;
	difference = block - original;
	difference (difference < 0) = 0;
	discrepancy = sum(difference>lowCutoff, 1);	
	
	% Skips unmappable clans
	if ~isempty(find(discrepancy == 1)) && max(discrepancy)<2
		% Check from 5' end for the first occurance of discrepancy
		insertionPos = find(discrepancy > lowCutoff, 1);
		insertedBase = find(difference(:,insertionPos) > lowCutoff);
		penetration = difference(insertedBase, insertionPos);
		
		if insertionPos == 1
			% False read
			continue;
		else
			hashIndex = ambiguities(insertedBase, insertionPos-1);
		end
		
		% Perform a fitting to find ratio of two species.
		observedVec = reshape(block,1,[]);				
		settings = optimset ('lbound', 0, 'ubound', 1);
		updatedPenetration = nonlin_curvefit(deviationFunc, penetration, variantVec(:,hashIndex)', observedVec, settings);
				
		variantHist = reshape(variantVec(:,hashIndex),4,[]);
		calcHistogram = updatedPenetration*variantHist + (1-updatedPenetration)*original;
		modelErrorHist = calcHistogram - block;
		RMSerror = sqrt (sum(sum( modelErrorHist.^2)) / (len*4) );
		
		if RMSerror < 0.05 % high quality model fit
			if updatedPenetration >= highCutoff
				repairedVec(hashIndex)++;
			elseif updatedPenetration > lowCutoff
				unrepairedVec(hashIndex)++;
			end
		else
			lowquality++;
		end
	end
end


% Reconstruction of the matrix view
repaired = 0*ambiguities;
unrepaired = 0*ambiguities;
for i=1:1:classID
	repaired(ambiguities==i) = repairedVec(i);
	unrepaired(ambiguities==i) = unrepairedVec(i);
end

total = unrepaired+repaired;
total(total==0) = nan;
%drawBoard(total, '~/wtClancount.png', -5, 200,libname,[],1);


reprate = 2*repaired ./ (2*repaired+unrepaired);
reprate(total==0) = nan;
drawBoard(reprate, '~/wtEfficiency.png', -0.05 , 1, libname, [], 1 );

nanmean(reshape(reprate,1,[]))
nanstd(reshape(reprate,1,[])) /sqrt(classID)


