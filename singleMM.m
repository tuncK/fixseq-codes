%%%

load(filename);

libname = 'DEB3L';
repairInit;

repaired = zeros(size(original));
unrepaired = repaired;
consensusCount = 0;
lowquality = 0;

lowCutoff = 0.1;
highCutoff = 1-lowCutoff;


numHists = size(hists,3);
biases = nan*zeros(numHists,1);
sizes = nan*zeros(numHists,1);
for i=1:1:numHists
	block = hists(:,:,i);
	numreads = sum(block(:,1));
	if numreads < 10
		continue;
	end
	sizes(i) = numreads;

	block /= numreads;
	difference = block - original;
	difference (difference < 0) = 0;
	
	maxchange = max(difference);
	[freq,pos] = max(maxchange);
	[~,base] = max(difference(:,pos));

	% Check for multiple mismatches or errors
	quality = maxchange / (1e-10+freq);
	quality(pos) = 0;
	if max(quality) > 0.3
		% second peak, faulty cluster
		lowquality++;
	else
		biases(i) = freq;
		if freq >= highCutoff
			repaired(base,pos)++;
		elseif freq > lowCutoff
			unrepaired(base,pos)++;
		else
			% Not mappable
			consensusCount++;
		end
	end
end


reprate = 2*repaired ./ (2*repaired + unrepaired);
total = repaired + unrepaired;
total(original==1) = NaN;



