%%

mappedLibname = libname;

if strcmp(libname,'3ML1') || strcmp(libname, 'inv3ML1')
	% Trimer library case
	file = fopen('./3ML1-sequences-twist.txt', 'r');
	barcodeLength = 7;
	barcodeTable = zeros(4007,barcodeLength);
	mismatchTable = zeros(4007,2);
	
	for ii=1:1:4007
		line = fgetl(file);
		barcodeTable(ii,:) = line(34:40);
		lib = line(41:106);
		mismatchTable(ii,1) = find(sequence ~= lib); % position of substitution
		mismatchTable(ii,2) = dna2num(lib(mismatchTable(ii,1))); % Substituted base
		
		% Replace the int values of ATCG by 1,2,3,4.
		barcodeTable(barcodeTable==65) = 1;
		barcodeTable(barcodeTable==84) = 2;
		barcodeTable(barcodeTable==67) = 3;
		barcodeTable(barcodeTable==71) = 4;
		barcodeTable = int8(barcodeTable);
		save ('doubleBarcodeMap.mat', 'barcodeTable', 'mismatchTable', 'barcodeLength');
	end

elseif length(strfind(libname,'5ML'))>0
	% Pentamer library case
	file = fopen('./5ML-sequences-genescript.txt', 'r');
	barcodeLength = 6;
	barcodeTable = zeros(756,barcodeLength);
	mismatchTable = zeros(756,2);
	
	% Each sublibrary is 756 elements and are listed in the order:
	% 10, 11, 12, 13, 1, 2, 3, 4, 5, ..., 9
	% The rest of the file will be skipped.
	sublibraryIdx = str2num(libname(4:end)); %5ML(?)
	fileSkipIdx = 756*[ 4:12 0:3 ];
	for ii = 1:1:fileSkipIdx(sublibraryIdx)
		fgetl(file);
	end
	
	for ii=1:1:756
		line = fgetl(file);
		barcodeTable(ii,:) = line(27:32);
		lib = line(33:116);
		mismatchTable(ii,1) = find(sequence ~= lib); % position of substitution
		mismatchTable(ii,2) = dna2num(lib(mismatchTable(ii,1))); % Substituted base
	end
	
	% Replace the int values of ATCG by 1,2,3,4.
	barcodeTable(barcodeTable==65) = 1;
	barcodeTable(barcodeTable==84) = 2;
	barcodeTable(barcodeTable==67) = 3;
	barcodeTable(barcodeTable==71) = 4;
	barcodeTable = int8(barcodeTable);
	save ('doubleBarcodeMap.mat', 'barcodeTable', 'mismatchTable', 'barcodeLength');
	
	
elseif length(strfind(libname,'SSL'))>0
	% Heptamer library case (SSL1-2-3-4)
	file = fopen('./SSL-sequences-agilent.txt', 'r');
	barcodeLength = 7;
	barcodeTable = zeros(720,barcodeLength);
	mismatchTable = zeros(720,2);
	
	% Each sublibrary is 720 elements and are listed in the order:
	% SSL1, SSL2, SSL3, SSL4
	% The rest of the file will be skipped.
	sublibraryIdx = str2num(libname(4:end)); %SSL(?)
	fileSkipIdx = 720*[ 0:1:3 ];
	for ii = 1:1:fileSkipIdx(sublibraryIdx)
		fgetl(file);
	end
	
	for ii=1:1:720
		line = fgetl(file);
		barcodeTable(ii,:) = line(27:33);
		lib = line(34:113);
		mismatchTable(ii,1) = find(sequence ~= lib); % position of substitution
		mismatchTable(ii,2) = dna2num(lib(mismatchTable(ii,1))); % Substituted base
	end
	
	% Replace the int values of ATCG by 1,2,3,4.
	barcodeTable(barcodeTable==65) = 1;
	barcodeTable(barcodeTable==84) = 2;
	barcodeTable(barcodeTable==67) = 3;
	barcodeTable(barcodeTable==71) = 4;
	barcodeTable = int8(barcodeTable);
	save ('doubleBarcodeMap.mat', 'barcodeTable', 'mismatchTable', 'barcodeLength');


elseif strcmp(libname,'SIL')
	% Trimer library case
	file = fopen('SIL-DIL-sequencesOrdered.txt', 'r');
	barcodeLength = 7;
	varStrandLength = 67;
	barcodeTable = zeros(3300,barcodeLength);
	insertionTable = zeros(3300,2);
	insertionList = zeros(3300,varStrandLength); % Lists all the sequences expected from the variable strand, in the number form.
		
	for ii=1:1:3300
		line = fgetl(file);
		barcodeTable(ii,:) = line(27:33);
		lib = line(34:100);
		insertionList(ii,:) = dna2num(lib);
		insertionTable(ii,1) = find(sequence ~= lib)(1)-1; % position of insertion, substitution follows this position.
		insertionTable(ii,2) = dna2num(lib(insertionTable(ii,1)+1)); % inserted base
	end
			
	% Replace the int values of ATCG by 1,2,3,4.
	barcodeTable(barcodeTable==65) = 1;
	barcodeTable(barcodeTable==84) = 2;
	barcodeTable(barcodeTable==67) = 3;
	barcodeTable(barcodeTable==71) = 4;
	barcodeTable = int8(barcodeTable);
	insertionTable = int8(insertionTable);
	insertionList = int8(insertionList);
	save ('doubleBarcodeMap.mat', 'barcodeTable', 'insertionTable', 'insertionList', 'barcodeLength');
	
	
elseif strcmp(libname,'DIL')
	% Trimer library case
	file = fopen('SIL-DIL-sequencesOrdered.txt', 'r');
	barcodeLength = 8;
	varStrandLength = 68;
	barcodeTable = zeros(7920,barcodeLength);
	insertionTable = zeros(7920,2);
	insertionList = zeros(7920,varStrandLength); % Lists all the sequences expected from the variable strand, in the number form.
	
	% Skip SIL segment in the file
	for ii=1:3300
		fgetl(file);
	end
	
	for ii=1:1:7920
		line = fgetl(file);
		barcodeTable(ii,:) = line(27:34);
		lib = line(35:102);
		insertionList(ii,:) = dna2num(lib);
		if ~strcmp(lib,sequence)
			insertionTable(ii,1) = find(sequence ~= lib)(1)-1; % position of insertion
		else
			insertionTable(ii,1) = len-4;
		end
		% Inserted base pairs:
		pair = lib( (insertionTable(ii,1)+1):(insertionTable(ii,1)+2));
		insertionTable(ii,2) = (dna2num(pair(1))-1)*4 + dna2num(pair(2));
	end

	% Replace the int values of ATCG by 1,2,3,4.
	barcodeTable(barcodeTable==65) = 1;
	barcodeTable(barcodeTable==84) = 2;
	barcodeTable(barcodeTable==67) = 3;
	barcodeTable(barcodeTable==71) = 4;
	barcodeTable = int8(barcodeTable);
	insertionTable = int8(insertionTable);
	insertionList = int8(insertionList);
	save ('doubleBarcodeMap.mat', 'barcodeTable', 'insertionTable', 'insertionList', 'barcodeLength');
end

fclose(file);



