%%%
% Analysis of double-barcoded MM library output
% Individual MMs are histogrammed individually
% Deduces the repaired/unrepaired cutoffs dynamically.
% Uses the min/max peak levels of the neighbours to search for peaks.


more off;

addpath ../common


for fileID=1:2:12

libname = "3ML1";
repairInit;

% Load the hist file with clan histograms
if true %~exist('hists') 
	filename = sprintf('~/marccOutput/%d.hist', fileID);	
	load(filename); % source of the "hists" variable, which contains the data
	hists = single(hists);
	
	filename2 = sprintf('~/marccOutput/%d.hist', fileID+1);
	temphists = single(hists);
	load(filename2);
	hists = single(cat(3,temphists,hists));
	clear temphists;
end


% Import secondary barcode map
if ~exist('doubleBarcodeMap.mat') || ~exist('mappedLibname') || ~strcmp(mappedLibname,libname)
	importBarcodeMap;
else
	load doubleBarcodeMap.mat;
end


% Reverse-complement the sequence histograms if the inverted library is used
% so that it conforms with the usual prototype.
if strcmp(libname, 'inv3ML1')
	totalLength = len + barcodeLength;
	tempHists = hists;
	for i=1:1:totalLength
		tempHists(1,i,:) = hists(2,totalLength-i+1,:);
		tempHists(2,i,:) = hists(1,totalLength-i+1,:);
		tempHists(3,i,:) = hists(4,totalLength-i+1,:);
		tempHists(4,i,:) = hists(3,totalLength-i+1,:);
	end
	hists = tempHists;
	clear tempHists;
end


numReadsList = sum(hists(:,1,:),1);
seqDeepEnough = squeeze(numReadsList >= 10)'; % Should have been unnecessary
if sum(~seqDeepEnough) ~= 0
	fprintf("%d small clans detected and ignored. Something is fishy.\n", sum(~seqDeepEnough) );
end
hists = hists(:,:,seqDeepEnough);
numReadsList = squeeze(sum(hists(:,1,:),1));
numHists = size(hists,3);

% Calculate the average 2nd barcode for each read.
[~,barcodeMaxima] = max(hists(:,1:barcodeLength,:),[],1);
barcodeMaxima = int8(squeeze(barcodeMaxima)');


mmType = nan*zeros(numHists,2); % row 1: MM position, row 2: MM identity.
for i=1:1:numHists
	% Lookup the barcode table for the 2nd barcode
	realID = find(sum(barcodeTable==barcodeMaxima(i,:),2) == barcodeLength);
	if ~isempty(realID) 
		% A matching barcode found, check the quality of the clans
		% Record the identity corresponding to the barcode
		mmType(i,:) = mismatchTable(realID,:);
	end
end


bins = -0.1:0.025:1.1;
tabulatedHistograms = zeros(len,length(bins));
for i=1:1:len
	for j=1:4
		chosenClanID = find(mmType(:,1)==i & mmType(:,2)==j);
		if length(chosenClanID) == 0
			% No hits found, skip
			% Either proper base pairing, or data unavailable.
			continue;
		end
		
		N = numReadsList(chosenClanID); % total #reads in the clan
		n = squeeze( hists(j,i+barcodeLength,chosenClanID) ); % num substituted reads
		subsPrevalence = n./N;
		counts = histc(subsPrevalence,bins)';
		tabulatedHistograms(i,:) += counts;
	end
	tabulatedHistograms(i,:) /= sum(tabulatedHistograms(i,:));
end


load("~/3ML1-calibration.mat");
out = zeros(size(tabulatedHistograms,1),size(tabulatedHistograms,2),3);
for i=1:size(out,1)
	%blue
	out(i,:,1) -= tabulatedHistograms(i,:) .* (bins<=0.1);
	out(i,:,2) -= tabulatedHistograms(i,:) .* (bins<=0.1);
	
	%green
	out(i,:,1) -= tabulatedHistograms(i,:) .* (bins>0.1) .* (bins<highCutoff(i));
	out(i,:,3) -= tabulatedHistograms(i,:) .* (bins>0.1) .* (bins<highCutoff(i));
	
	%red
	out(i,:,2) -= tabulatedHistograms(i,:) .* (bins>=highCutoff(i));
	out(i,:,3) -= tabulatedHistograms(i,:) .* (bins>=highCutoff(i));
end


Fcount = sum(sum( tabulatedHistograms .* (bins<=0.1) ));
Ucount = sum(sum( tabulatedHistograms .* (bins>0.1) .* (bins<highCutoff(i)) ));
Vcount = sum(sum( tabulatedHistograms .* (bins>=highCutoff(i)) ));
totalCount = Fcount + Ucount + Vcount;

set(0, 'defaultaxesfontsize', 22);
imshow(1+5*out)
text(0.05*length(bins), 1.06*len, sprintf('%2d%%', round(100*Fcount/totalCount)), 'fontsize', 25, "color", "b");
text(0.4*length(bins), 1.06*len, sprintf('%2d%%', round(100*Ucount/totalCount)), 'fontsize', 25, "color", [0 0.8 0] );
text(0.75*length(bins), 1.06*len, sprintf('%2d%%', round(100*Vcount/totalCount)), 'fontsize', 25, "color", "r");
axis on
set(gca, 'ydir', 'normal')
xlabel('Substitution prevalence');
ylabel('Mismatch position (bp)');
set(gca, 'xtick', [ find(bins==0) find(bins==1) ])
set(gca, 'xticklabel', {'0', '1'})
pause(0.05);
print(sprintf('~/clanPrevalences%d.png',fileID));


imshow(1-4*tabulatedHistograms);
axis on
set(gca, 'ydir', 'normal')
xlabel('Substitution prevalence');
ylabel('Mismatch position (bp)');
set(gca, 'xtick', [ find(bins==0) find(bins==1) ])
set(gca, 'xticklabel', {'0', '1'})
pause(0.05);
print(sprintf('~/clanPrevalences%dGrayscale.png',fileID));


end


