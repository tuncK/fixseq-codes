%%%
% Analysis of double-barcoded insertion library output


clear all;
close all;

libname = 'SIL';
addpath ../common/
repairInit;

% Import secondary barcode map
if ~exist('doubleBarcodeMap.mat') || ~exist('mappedLibname') || (mappedLibname~=libname)
	importBarcodeMap;
else
	load doubleBarcodeMap.mat;
end


% Base combinations that can be put in each spot.
% 4, if single base insertions
if strcmp(libname,"SIL")
	% Generate a list of all expected sequences.
	ambiguities = zeros(4,len);
	variantVec = zeros(4*len, 4*len); %(basePos, hashID)
	sequenceNum = dna2num(sequence);
	for i=1:1:len-1
		for base1 = 1:4
			newseq = [ sequenceNum(1:i); base1; sequenceNum(i+1:end-1) ];
			hits = find( sum(insertionList==newseq',2)==size(insertionList,2) );
			
			if length(hits) > 0
				classID = hits(1);				
				ambiguities(base1,i) = classID;
				libElement = zeros(4,len);
				for j=1:1:len
					libElement(newseq(j),j) = 1;
				end
				variantVec(:,classID) = reshape(libElement,1,[]);
			end
		end
	end


% 16, if double base insertions
elseif strcmp(libname,"DIL")
	% Generate a list of all expected sequences.
	ambiguities = zeros(16,len);
	variantVec = zeros(4*len, 16*len); %(basePos, hashID)
	sequenceNum = dna2num(sequence);
	for i=1:1:len-2
		for base1 = 1:4
			for base2= 1:4
				newseq = [ sequenceNum(1:i); base1; base2; sequenceNum(i+1:end-2) ];
				hits = find( sum(insertionList==newseq',2)==size(insertionList,2) );
				
				if length(hits) > 0
					classID = hits(1);
					ambiguities(4*(base1-1)+base2,i) = classID;
					libElement = zeros(4,len);
					for j=1:1:len
						libElement(newseq(j),j) = 1;
					end
					variantVec(:,classID) = reshape(libElement,1,[]);
				end
			end
		end
	end
end


% Plot a map of ambiguious zones
if false
	temp = ambiguities(:,1:65);
	random = rand(max(max(temp)),1);
	out = random(temp);
	drawBoard(out, '/home/tk/insertionAmbiguity.png', 0, 1, libname, 'hsv', true);
end


for fileID = 1:4

% load mat file with clan histograms
if true % ~exist('hists')
	filename = sprintf('~/MARCCoutput/%d.hist', fileID);
	load(filename); % source of the "hists" variable, which contains the data
end
hists = hists(:,1:74,:); % to reduce data quantity, DEBUG ONLY

numReadsList = sum(hists(:,1,:),1);
seqDeepEnough = (numReadsList >= 10); % Should have been unnecessary
if sum(~seqDeepEnough) ~= 0
	fprintf("%d small clans detected and ignored. Something is fishy.\n", sum(~seqDeepEnough) );
end

hists = hists(:,:,seqDeepEnough);
numReadsList = numReadsList(:,:,seqDeepEnough);
numHists = size(hists,3);

% Calculate the average 2nd barcode for each read.
[~,barcodeMaxima] = max(hists(:,1:barcodeLength,:),[],1);
barcodeMaxima = squeeze(barcodeMaxima)';
insertionType = nan*zeros(numHists,2); % row 1: insertion position, row 2: insertion identity

% Calculate the substitution position and type
frequencies = ( hists(:,(barcodeLength+1):end,:) ./ numReadsList );
differences = frequencies - original;

hashIndeces = nan*zeros(numHists,1);
prevalences = hashIndeces;

% Materials necessary for fitting 
pkg load optim;
settings = optimset ('lbound', 0, 'ubound', 1);
originalVec = reshape(original,1,[]);
deviationFunc = @(p,x) p(1)*x + (1-p(1))*originalVec;

for i=1:1:numHists
	% Lookup the barcode table for the 2nd barcode
	realID = find(sum(barcodeTable == barcodeMaxima(i,:),2) == barcodeLength);
	if isempty(realID)
		continue;
	end
	
	% Else, check if the observed clan histogram makes sense.
	insertionType(i,:) = insertionTable(realID,:);
	realPos = insertionTable(realID,1);
	realBase = insertionTable(realID,2);
	hashIndeces(i) = ambiguities(realBase, realPos);

	% Perform a fitting to find ratio of two species.
	penetration = 1 - differences( sequenceNum(realPos), realPos, i); % initial guess
	observedVec = reshape(frequencies(:,:,i),1,[]);				
	updatedPenetration = nonlin_curvefit(deviationFunc, penetration, variantVec(:,hashIndeces(i))', observedVec, settings);
	variantHist = reshape(variantVec(:,hashIndeces(i)),4,[]);
	calcHistogram = updatedPenetration*variantHist + (1-updatedPenetration)*original;
	modelErrorHist = calcHistogram - frequencies(:,:,i);
	RMSerror = sqrt( sum(sum( modelErrorHist.^2)) / (len*4) );
	
%	if RMSerror < 0.05 % high quality model fit, record.
		prevalences(i) = updatedPenetration;
%	end
end


% Materials necessary for fitting 
settings = optimset ('lbound', [0;0;-1;0], 'ubound', [1;1;2;inf]);
bins = -0.1:0.025:1.1;
dbins = bins(2)-bins(1);
p_init = [0.2; 0.8; 0.9; 0.1];
fitfunction = @(p,x) p(1)*(x==dbins) + p(2)*exp(-(x-p(3)).^2/p(4));
highPeakPos = nan*zeros(size(ambiguities));
highPeakSpread = highPeakPos;

tabulatedHistograms = zeros(len-2,length(bins));
for hash=unique(hashIndeces(~isnan(hashIndeces)))'
	chosenClanID = find(hashIndeces==hash);
	subsPrevalence = prevalences(chosenClanID);
	
	[ii,jj] = find(ambiguities==hash);
	base = ii(1);
	pos = jj(1);

	subsPrevalence = prevalences(chosenClanID);
	counts = histc(subsPrevalence,bins)';
	tabulatedHistograms(pos,:) += counts;
	
	[p,fit] = nonlin_curvefit (fitfunction, p_init, bins+dbins, counts./sum(counts), settings);
	highPeakPos(base,pos) = p(3);
	highPeakSpread(base,pos) = p(4);
end

tabulatedHistograms = tabulatedHistograms ./ sum(tabulatedHistograms,2);
likelyUnrepaired = (highPeakSpread>0.05); % The spread is too high, dominantly unrepaired
highPeakPos (likelyUnrepaired) = nan;
posList = reshape(highPeakPos', 1, []);
xIndeces = repmat(1:len, 1, size(ambiguities,1) );
pp = polyfit( xIndeces(~isnan(posList)), posList(~isnan(posList)), 6 );
pFit = polyval(pp,1:len);
highCutoff = pFit - 0.15;

out = zeros(size(tabulatedHistograms,1),size(tabulatedHistograms,2),3);
for i=1:size(out,1)
	%blue
	out(i,:,1) -= tabulatedHistograms(i,:) .* (bins<=0.1);
	out(i,:,2) -= tabulatedHistograms(i,:) .* (bins<=0.1);
	
	%green
	out(i,:,1) -= tabulatedHistograms(i,:) .* (bins>0.1) .* (bins<highCutoff(i));
	out(i,:,3) -= tabulatedHistograms(i,:) .* (bins>0.1) .* (bins<highCutoff(i));
	
	%red
	out(i,:,2) -= tabulatedHistograms(i,:) .* (bins>=highCutoff(i));
	out(i,:,3) -= tabulatedHistograms(i,:) .* (bins>=highCutoff(i));
end

Fcount = sum(sum( tabulatedHistograms .* (bins<=0.1) ));
Ucount = sum(sum( tabulatedHistograms .* (bins>0.1) .* (bins<highCutoff(i)) ));
Vcount = sum(sum( tabulatedHistograms .* (bins>=highCutoff(i)) ));
totalCount = Fcount + Ucount + Vcount;

set(0, 'defaultaxesfontsize', 22);
imshow(1+5*out)
text(0.05*length(bins), 1.05*len, sprintf('%2d%%', round(100*Fcount/totalCount)), 'fontsize', 25, "color", "b");
text(0.4*length(bins), 1.05*len, sprintf('%2d%%', round(100*Ucount/totalCount)), 'fontsize', 25, "color", [0 0.8 0] );
text(0.75*length(bins), 1.05*len, sprintf('%2d%%', round(100*Vcount/totalCount)), 'fontsize', 25, "color", "r");
axis on
set(gca, 'ydir', 'normal')
xlabel('Substitution prevalence');
ylabel('Mismatch position (bp)');
set(gca, 'xtick', [ find(bins==0) find(bins==1) ])
set(gca, 'xticklabel', {'0', '1'})
pause(0.05);
print('~/clanPrevalences.png');


repairedF = 0*ambiguities;
repairedV = 0*ambiguities;
unrepaired = 0*ambiguities;
for hash=unique(hashIndeces(~isnan(hashIndeces)))'
	chosenClanID = find(hashIndeces==hash);
	subsPrevalence = prevalences(chosenClanID);
	
	[ii,jj] = find(ambiguities==hash);
	pos = jj(1);

	repairedF(ambiguities==hash) = sum( subsPrevalence <= 0.1);
	repairedV(ambiguities==hash) = sum( subsPrevalence >= highCutoff(pos) );
	unrepaired(ambiguities==hash) = sum( subsPrevalence > 0.1 & subsPrevalence < highCutoff(pos) );
end

% Calculate the repair statistics
total = repairedF + repairedV + unrepaired;
reprate = (repairedF + repairedV) ./ total;
bias = (repairedV-repairedF) ./ (repairedF + repairedV);
matfilename = [filename(1:end-5) '.mat'];
save(matfilename, 'reprate', 'bias', 'repairedF', 'repairedV', 'unrepaired', 'libname');

end


% Print repair statistics
close all;
drawBoard (reprate, '~/efficiency.png', 0 , 1, libname, [], true);
return


close all;
pause(0.01);
drawBoard (bias, '~/bias.png', -1 , 1, libname, 'redBlue', true);

close all;
pause(0.01);
drawBoard (total, '~/total.png', 0, 100, libname, [], true);


