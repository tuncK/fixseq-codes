%%%
% Analysis of double-barcoded MM library output
% Individual MMs are histogrammed individually
% Deduces the repaired/unrepaired cutoffs dynamically.
% Uses the min/max peak levels of the neighbours to search for peaks.

clear all
warning('off','all') %Turn-off all warnings (precision loss for nchoosek)
more off;
addpath ../common/

recalculateCalibration = false;

for fileID = 1:1:12
	%libname = sprintf( 'SSL%d', ceil(fileID/4) );
	libname = '3ML1';
	repairInit;

% Load the hist file with clan histograms
if ~exist('hists') 
	filename = sprintf('~/marccOutput/%d.hist', fileID);
	load(filename); % source of the "hists" variable, which contains the data
	hists = single(hists);
end

%if recalculateCalibration	
%	filename2 = sprintf('~/marccOutput/%d.hist', fileID+1);
%	temphists = single(hists);
%	load(filename2);
%	hists = single(cat(3,temphists,hists));
	
%	filename3 = sprintf('~/SSLhists/%d.hist', fileID+2);
%	temphists = single(hists);
%	load(filename3);
%	hists = single(cat(3,temphists,hists));
%	clear temphists;
%end


% Import secondary barcode map
if ~exist('doubleBarcodeMap.mat') || ~exist('mappedLibname') || ~strcmp(mappedLibname,libname)
	importBarcodeMap;
else
	load doubleBarcodeMap.mat
end


% Reverse-complement the sequence histograms if the inverted library is used
% so that it conforms with the usual prototype.
if strcmp(libname, 'inv3ML1')
	totalLength = len + barcodeLength;
	tempHists = hists;
	for i=1:1:totalLength
		tempHists(1,i,:) = hists(2,totalLength-i+1,:);
		tempHists(2,i,:) = hists(1,totalLength-i+1,:);
		tempHists(3,i,:) = hists(4,totalLength-i+1,:);
		tempHists(4,i,:) = hists(3,totalLength-i+1,:);
	end
	hists = tempHists;
	clear tempHists;
end


numReadsList = sum(hists(:,1,:),1);
seqDeepEnough = squeeze(numReadsList >= 10)'; % Should have been unnecessary
if sum(~seqDeepEnough) ~= 0
	fprintf("%d small clans detected and ignored. Something is fishy.\n", sum(~seqDeepEnough) );
end
hists = hists(:,:,seqDeepEnough);
numReadsList = squeeze(sum(hists(:,1,:),1));
numHists = size(hists,3);

% Calculate the average 2nd barcode for each read.
[~,barcodeMaxima] = max(hists(:,1:barcodeLength,:),[],1);
barcodeMaxima = int8(squeeze(barcodeMaxima)');


mmType = nan*zeros(numHists,2); % row 1: MM position, row 2: MM identity.
for i=1:1:numHists
	% Lookup the barcode table for the 2nd barcode
	realID = find(sum(barcodeTable==barcodeMaxima(i,:),2) == barcodeLength);
	if ~isempty(realID) 
		% A matching barcode found, check the quality of the clans
		% Record the identity corresponding to the barcode
		mmType(i,:) = mismatchTable(realID,:);
	end
end


highCutoffMatfilename = [ '~/marccOutput/' libname '-calibration.mat'];
if recalculateCalibration
	% Materials necessary for fitting
	pkg load optim;
	settings = optimset ('lbound', [0;0;-1;0], 'ubound', [1;1;2;inf]);

	bins = -0.1:0.025:1.1;
	dbins = bins(2)-bins(1);
	p_init = [0.2; 0.8; 0.9; 0.1];
	fitfunction = @(p,x) p(1)*(x==dbins) + p(2)*exp(-(x-p(3)).^2/p(4));
	highPeakPos = nan*zeros(size(original));
	highPeakSpread = highPeakPos;
	
	mkdir ('/home/tk/output');
	tabulatedHistograms = zeros(len,length(bins));
	for i=1:1:len
		for j=1:4
			chosenClanID = find(mmType(:,1)==i & mmType(:,2)==j);
			if length(chosenClanID) == 0
				% No hits found, skip
				% Either proper base pairing, or data unavailable.
				continue;
			end
			
			N = numReadsList(chosenClanID); % total #reads in the clan
			n = squeeze( hists(j,i+barcodeLength,chosenClanID) ); % num substituted reads
			subsPrevalence = n./N;
			counts = histc(subsPrevalence,bins)';
			tabulatedHistograms(i,:) += counts;
			counts /= sum(counts);
				
			[p,fit] = nonlin_curvefit (fitfunction, p_init, bins+dbins, counts, settings);
			highPeakPos(j,i) = p(3);
			highPeakSpread(j,i) = p(4);
			
			if false
				bar(bins, counts, 'k', 'edgecolor', 'none');
			%	hold on;
			%	plot(bins, fit, 'r-', 'linewidth', 1.5);
			%	hold off
				
				xlim([-0.1 1.1]);
				ymax = get(gca, 'ylim')(2);
				ylim( [0 ymax] );
				xlabel('Substitution prevalence');
				ylabel('Frequency');
				title(sprintf("%.2f %.2f", p(3), p(4)));
				pause(0.05);
				print(sprintf('~/output/pos%dtype%d.png', i, j));
			end
		end
		tabulatedHistograms(i,:) /= sum(tabulatedHistograms(i,:));
	end

	likelyUnrepaired = (highPeakSpread>0.05); % The spread is too high, dominantly unrepaired
	highPeakPos (likelyUnrepaired) = nan;
	posList = reshape(highPeakPos', 1, []);
	xIndeces = repmat(1:1:len,1,4);
	pp = polyfit( xIndeces(~isnan(posList)), posList(~isnan(posList)), 6 );
	pFit = polyval(pp,1:len);
	highCutoff = pFit - 0.15;

	save(highCutoffMatfilename, 'highCutoff');
end

clear highCutoff;
load(highCutoffMatfilename);

if recalculateCalibration
	out = zeros(size(tabulatedHistograms,1),size(tabulatedHistograms,2),3);
	for i=1:size(out,1)
		%blue
		out(i,:,1) -= tabulatedHistograms(i,:) .* (bins<=0.1);
		out(i,:,2) -= tabulatedHistograms(i,:) .* (bins<=0.1);
		
		%green
		out(i,:,1) -= tabulatedHistograms(i,:) .* (bins>0.1) .* (bins<highCutoff(i));
		out(i,:,3) -= tabulatedHistograms(i,:) .* (bins>0.1) .* (bins<highCutoff(i));
		
		%red
		out(i,:,2) -= tabulatedHistograms(i,:) .* (bins>=highCutoff(i));
		out(i,:,3) -= tabulatedHistograms(i,:) .* (bins>=highCutoff(i));
	end

	Fcount = sum(sum( tabulatedHistograms .* (bins<=0.1) ));
	Ucount = sum(sum( tabulatedHistograms .* (bins>0.1) .* (bins<highCutoff(i)) ));
	Vcount = sum(sum( tabulatedHistograms .* (bins>=highCutoff(i)) ));
	totalCount = Fcount + Ucount + Vcount;

	set(0, 'defaultaxesfontsize', 22);
	imshow(1+5*out)
	text(0.05*length(bins), 1.06*len, sprintf('%2d%%', round(100*Fcount/totalCount)), 'fontsize', 25, "color", "b");
	text(0.4*length(bins), 1.06*len, sprintf('%2d%%', round(100*Ucount/totalCount)), 'fontsize', 25, "color", [0 0.8 0] );
	text(0.75*length(bins), 1.06*len, sprintf('%2d%%', round(100*Vcount/totalCount)), 'fontsize', 25, "color", "r");
	axis on
	set(gca, 'ydir', 'normal')
	xlabel('Substitution prevalence');
	ylabel('Mismatch position (bp)');
	set(gca, 'xtick', [ find(bins==0) find(bins==1) ])
	set(gca, 'xticklabel', {'0', '1'})
	pause(0.05);
	print(sprintf('~/clanPrevalences%d.png',fileID));
end


% Plots a chart representing the choice of low/high thresholds
if false
	plot(xIndeces, posList, "b*");
	hold on;
	plot(1:len, pFit, 'r-', "linewidth",2);
	hold off
	xlabel('Base position');
	ylabel('Peak center position')
	ylim([0 1.05])
	print(sprintf("~/peakCenterPos%d.png", fileID));
end


if false
	thresholds = zeros(100,len);
	thresholds(1:10,:) = 1;
	for i=1:1:len
		thresholds(round(100*highCutoff(i)):end,i) = 2;
	end
	imagesc(thresholds');
	xlabel("Substitution prevalence (%)");
	ylabel("Base position");
	colormap(hsv)
	caxis([-1 2])
	print("~/thresholds.png");
end


% Evaluate the repair efficiency using these dynamic thresholds
unrepaired = zeros(size(original));
repairedF = unrepaired;
repairedV = unrepaired;
for i=1:1:len
	for j=1:4
		chosenClanID = find(mmType(:,1)==i & mmType(:,2)==j);
		if length(chosenClanID) == 0
			% No hits found, skip
			% Either proper base pairing, or data unavailable.
			continue;
		end
		
		N = squeeze( sum(hists(:,i+barcodeLength,chosenClanID),1) ); % num total reads
		n = squeeze( hists(j,i+barcodeLength,chosenClanID) ); % num substituted reads
		subsPrevalence = n./N;
		
		repairedF(j,i) = sum( subsPrevalence <= 0.1);
		repairedV(j,i) = sum( subsPrevalence >= highCutoff(i) );
		unrepaired(j,i) = sum( subsPrevalence > 0.1 & subsPrevalence < highCutoff(i) );
	end
end

total = repairedF + repairedV + unrepaired;
total(original==1) = NaN;
reprate = (repairedF + repairedV) ./ total;
bias = (repairedV-repairedF) ./ (repairedF + repairedV);
matfilename = [filename(1:end-5) '.mat'];
save(matfilename, 'reprate', 'bias', 'repairedF', 'repairedV', 'unrepaired', 'libname');


if recalculateCalibration
	% Print repair statistics
	close all;
	drawBoard (reprate, sprintf('~/efficiency%d.png',fileID), 0 , 1, libname);
	pkg load statistics
	avg = nanmean(reshape(reprate,1,[]));
	dev = nanstd(reshape(reprate,1,[]),1);
	fprintf('%f +/- %f\n', avg,dev);
	continue;

	close all;
	pause(0.01);
	drawBoard (total, sprintf('~/clancount%d.png',fileID), [] , [], libname);

	close all;
	pause(0.01);
	drawBoard (bias, sprintf('~/bias%d.png',fileID), -1 , 1, libname, 'redBlue');
	close all;
end

end


