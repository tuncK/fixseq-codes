%%%
% Analysis of double-barcoded insertion library output

clear all;
close all;

libname = 'SIL';
addpath ../common/
repairInit;


% Base combinations that can be put in each spot.
% 4, if single base insertions
if strcmp(libname,"SIL")
	numBases = 4;
	file = fopen("~/sil/silMMIDmatrix.csv");
else
	numBases = 16;
	file = fopen("~/dil/dilMMIDmatrix.csv");
end
MMIDs = fscanf(file, "%d", [3,inf]);
fclose(file);

ambiguities = zeros(numBases,len);
for i=1:size(MMIDs,2);
	ambiguities(MMIDs(2,i)+1, MMIDs(1,i)) = MMIDs(3,i);
	% '+1's because C++ arrays start at index 0, and Octave arrays at 1.
end

% Plot a map of ambiguious zones
if false
	random = rand(max(max(ambiguities))+1,1);
	out = random(ambiguities+min(min(ambiguities))+1);
	out(ambiguities==0) = nan;
	drawBoard(out, '/home/tk/insertionAmbiguity.png', 0, 1, libname, 'hsv', true);
	return
end

clans = [];
for fileID = 4
	% load the csv file with F/V strand counts
	if strcmp(libname,"SIL")
		filename = sprintf('~/sil/%d.csv', fileID);
	else
		filename = sprintf('~/dil/%d.csv', fileID);
	end
	file = fopen(filename);
	temp = fscanf(file, "%d", [3,inf]);
	fclose(file);
	clans = [clans, temp];
end

	
numReadsList = clans(1,:) + clans(2,:);
seqDeepEnough = (numReadsList >= 10);
clans = clans(:,seqDeepEnough);
numReadsList = clans(1,:) + clans(2,:);
prevalences = clans(1,:) ./ (clans(1,:)+clans(2,:));

repairedF = 0*ambiguities;
repairedV = 0*ambiguities;
unrepaired = 0*ambiguities;

for mmid = unique(reshape(ambiguities,1,[]));
	chosenClanID = find(clans(3,:)==mmid);
	subsPrevalence = prevalences(chosenClanID);
	
	repairedF(ambiguities==mmid) = sum( subsPrevalence <= 0.1);
	repairedV(ambiguities==mmid) = sum( subsPrevalence >= 0.9 );
	unrepaired(ambiguities==mmid) = sum( subsPrevalence > 0.1 & subsPrevalence < 0.9 );
end

% Calculate the repair statistics
total = repairedF + repairedV + unrepaired;
reprate = (repairedF + repairedV) ./ total;
bias = (repairedV-repairedF) ./ (repairedF + repairedV);
matfilename = [filename(1:end-5) '.mat'];
save(matfilename, 'reprate', 'bias', 'repairedF', 'repairedV', 'unrepaired', 'libname');


s= reprate(~isnan(reprate));
mean(s)
std(s,1)
length(s)


% Print repair statistics
close all;
drawBoard (reprate, '~/mutSSILefficiency.png', 0 , 1, libname, [], true);

close all;
pause(0.01);
drawBoard (bias, '~/wtDILbias.png', -1 , 1, libname, 'redBlue', true);

close all;
return


close all;
pause(0.01);
drawBoard (total, '~/total.png', 0, 100, libname, [], true);


