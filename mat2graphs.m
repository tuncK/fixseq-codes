%%
% Draws presentable graphs for repair efficieny etc. for a provided *.mat file



for id=1
	matfilename = sprintf("/home/tk/3ML1 matfiles/combined%d-%d.mat",id,id+1);
	libname = sprintf('5ML%d', ceil((id-4)/2));
	libname = '3ML1';
	
	repairInit;
	load(matfilename);
%	backgroundEfficiency = 0.37398; % minumum repair efficiency of combined3-4.mat for 3ML1 mutS deficient.
%	reprate = (reprate - backgroundEfficiency) / (1-backgroundEfficiency);

	% Print repair statistics
%	close all;
%	filename = sprintf('~/efficiency%d.png', id);
%	drawBoard (reprate, filename, 0 , 1, libname);
end


unrepaired = 1 + 0*unrepaired;

drawBoard (unrepaired, '~/unrepaired.png', 0, 5000, libname);
drawBoard (repairedF, '~/repairedF.png', 0, 5000, libname);
drawBoard (repairedV, '~/repairedV.png', 0, 5000, libname);


return

close all;
pause(0.01);
drawBoard (bias, '~/Bias.png', -1 , 1, libname, 'redBlue');

close all;
pause(0.01);
total = unrepaired + repairedF + repairedV;
drawBoard (total, '~/Clancount.png', 0, 5000, libname);




return

% Optional code to generate colorbars
set(0,"defaultaxesfontsize",25);
imagesc(total)
colormap(hot(256));
caxis([0 5000]);
colorbar("location", "southoutside");
print("~/a.png", "-r300");



