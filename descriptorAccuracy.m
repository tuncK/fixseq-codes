%%%
% Analyses the outliers that are difficult to repair for double barcoded libraries.

addpath ./common/
more off;

allPentamers = {};
allHeptamers = {};
pentamerEfficiency = [];
heptamerEfficiency = [];


% Load repair efficiency data
file = fopen("./common/heptamerSSL.csv");
fgetl(file);
for i=1:825
	pattern = fscanf(file, "%2*c, %7c, %7c");
	allHeptamers{end+1} = [pattern(1:7) "-" pattern(11)];
	heptamerEfficiency = [heptamerEfficiency, fscanf(file, "%f,%*f,%*f,\n") ];
end
fclose(file);

file = fopen("./common/pentamer5MLx.csv");
fgetl(file);
for i=1:3072
	pattern = fscanf(file, "%2*c, %5c, %5c");
	allPentamers{end+1} = [pattern(1:5) "-" pattern(8)];
	pentamerEfficiency = [pentamerEfficiency, fscanf(file, "%f,%*f,%*f,\n") ];
end
fclose(file);


set(0, 'defaultaxesfontsize', 20);
for MMtype = { "AA", "AC", "AG" }
	close all;
	hold on;
	
	cumulatedRates = [];
	for pentamerSought = {"AAAAC", "TAAAA", "TAAAG"}
		hits = regexp(allHeptamers, sprintf("[ACGT]%s[ACGT]-%s", pentamerSought{}, MMtype{}(2)) );
		selection = ( (~cellfun(@isempty,hits)) )';
		selectedRates = heptamerEfficiency(selection);
		avg = mean(selectedRates);
		cumulatedRates = [cumulatedRates selectedRates-avg];
	end
	plot(ones(length(cumulatedRates),1), cumulatedRates, 'r*');
	std(cumulatedRates,1)

	hits = regexp(allHeptamers, sprintf("[ACGT][ACGT]AAA[ACGT][ACGT]-%s", MMtype{}(2)) );
	selection = ( (~cellfun(@isempty,hits)) )';
	selectedRates = heptamerEfficiency(selection);
	avg = mean(selectedRates);
	plot(2*ones(length(selectedRates),1), selectedRates-avg, 'r^', 'markerfacecolor', 'auto');
	std(selectedRates,1)

	hits = regexp(allPentamers, sprintf("[ACGT]AAA[ACGT]-%s", MMtype{}(2)) );
	selection = ( (~cellfun(@isempty,hits)) )';
	selectedRates = pentamerEfficiency(selection);
	avg = mean(selectedRates);
	plot(3*ones(length(selectedRates),1), selectedRates-avg, 'bd', 'markerfacecolor', 'auto');
	std(selectedRates,1)
	
	hits = regexp(allHeptamers, sprintf("[ACGT][ACGT][ACGT]A[ACGT][ACGT][ACGT]-%s", MMtype{}(2)) );
	selection = ( (~cellfun(@isempty,hits)) )';
	selectedRates = heptamerEfficiency(selection);
	avg = mean(selectedRates);
	h(1) = plot(4*ones(length(selectedRates),1), selectedRates-avg, 'r>', 'markerfacecolor', 'auto');
	std(selectedRates,1)
	
	hits = regexp(allPentamers, sprintf("[ACGT][ACGT]A[ACGT][ACGT]-%s", MMtype{}(2)) );
	selection = ( (~cellfun(@isempty,hits)) )';
	selectedRates = pentamerEfficiency(selection);
	avg = mean(selectedRates);
	h(2) = plot(5*ones(length(selectedRates),1), selectedRates-avg, 'b<', 'markerfacecolor', 'auto');
	std(selectedRates,1)
	
	plot(0:1:6, zeros(7,1), 'k-');
	hold off;
	
	xlim([0 6]);
	set(gca,'Xtick',1:5);
	set(gca,'XTickLabel', {'5mer', '3mer', '3mer', '1mer', '1mer'} )
	xlabel('Constant central motif')
	ylabel('\eta_s - <\eta_s>');
	
	HLEG = legend(h, 'SSLx', '5MLx');
	legend boxoff;
	lchildren = get(HLEG, 'children');
	for i=1:2
		set(lchildren(i+2), "color", get(lchildren(i),'color') );
	end
	
	print(sprintf('~/%s.png', MMtype{}), '-r300');
end


