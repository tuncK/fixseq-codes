%%

pkg load statistics

load('~/SML/68-70.mat');
wtmm = reshape(reprate(:,1:50),[],1);

load('~/SML/74-77.mat');
wt = reshape(reprate(:,1:50),[],1);

load('~/SML/78-80.mat');
hmm = reshape(reprate(:,1:50),[],1);

load('~/SML/81-83.mat');
h = reshape(reprate(:,1:50),[],1);

load('~/SML/84-86.mat');
smm = reshape(reprate(:,1:50),[],1);

load('~/SML/90-91.mat');
s = reshape(reprate(:,1:50),[],1);

load('~/SML/95-98.mat');
l = reshape(reprate(:,1:50),[],1);

load('~/SML/99-101.mat');
uvrd = reshape(reprate(:,1:50),[],1);

load('~/SML/105-107.mat');
uvrb = reshape(reprate(:,1:50),[],1);


set(0, 'defaultaxesfontsize', 22);
pos = rand(200,1)/5;

% Calculation of p-values w.r.t. wt unmethylated
isValid = ~isnan(wtmm);
means = [ mean(wt(isValid)), mean(s(isValid)), mean(l(isValid)), mean(h(isValid)), mean(uvrd(isValid)), mean(uvrb(isValid)), mean(wtmm(isValid)), mean(smm(isValid)), mean(hmm(isValid)) ];
stdevs = [ std(wt(isValid))  std(s(isValid))  std(l(isValid))  std(h(isValid))  std(uvrd(isValid))  std(uvrb(isValid))  std(wtmm(isValid))  std(smm(isValid)) std(hmm(isValid)) ] / sqrt(sum(isValid));

for i=1:9
	pvalue = normcdf( -abs(means(i)-means(1)) , 0, sqrt(stdevs(1)^2+stdevs(i)^2) );
	printf( '%d\t%.2f\t%.2f\n', i, means(i), stdevs(i) )
end

p1_7 = normcdf( -abs(means(7)-means(1)) , 0, sqrt(stdevs(1)^2+stdevs(7)^2) )
p2_8 = normcdf( -abs(means(8)-means(2)) , 0, sqrt(stdevs(2)^2+stdevs(8)^2) )
p4_9 = normcdf( -abs(means(9)-means(4)) , 0, sqrt(stdevs(4)^2+stdevs(9)^2) )


plot(pos,wt, 'b^', 'markerfacecolor', 'auto');
hold on;
plot(pos+1,s, 'rs', 'markerfacecolor', 'auto');
plot(pos+2,l, 'gd', 'markerfacecolor', 'auto');
plot(pos+3,h, 'mo', 'markerfacecolor', 'auto');
plot(pos+4,uvrd, 'cv', 'markerfacecolor', 'auto');
plot(pos+5,uvrb, 'k+', 'markerfacecolor', 'auto');
plot(pos+6,wtmm, 'b^', 'markerfacecolor', 'auto');
plot(pos+7,smm, 'rs', 'markerfacecolor', 'auto');
plot(pos+8,hmm, 'mo', 'markerfacecolor', 'auto');


line([5.6 5.6], [0 1.1], 'color', 'k', 'linewidth', 2)
text(0.8,1.05, 'Unmethylated', 'fontsize', 25);
text(6,1.05, 'Methylated', 'fontsize', 25);

for i=1:9
	rectangle('position', [i-1.3 means(i) 0.8 0.01 ], 'facecolor', 'k');
end
hold off

## set up xtick labels and rotate
xticklabel = ["\ \ \ wt"; '\DeltamutS'; '\DeltamutL'; '\DeltamutH'; '\DeltauvrD'; '\DeltauvrB'; "\ \ \ wt"; '\DeltamutS'; '\DeltamutH'];
set(gca, 'xtick', []);
color = ['b';'r';'g';'m';'c';'k';'b';'r';'m'];
for i=1:9
	hnew = text(i-0.8, -0.08, xticklabel(i,:), 'fontsize', 25, 'color', color(i), 'rotation', 30, 'horizontalalignment', 'center');
end
set(gca,'position', [0.13 0.15 0.77 0.81])

xlim([-0.5 9])
ylim([0 1.1])
ylabel('Repair efficiency (\eta`)');

print('~/SMLglobalComparison.png', '-r300');



