%%%
% Checks if there is a correlation between the GC content of the library
% consensus sequence and the shift in the high peak position

addpath ./common/
more off;

set(0,"defaultaxesfontsize", 25)

if false
	sequences = {};
	GCpositions = [];
	for libID=1:1:13
		% Set the parameters depending on the chosen library
		libname = sprintf("5ML%d", libID);
		repairInit;
		fixedStrand = [fixedStrand "GAGCTC"]; % Add the information about the fixed XhoI cut site.
		sequences{end+1} = fixedStrand;
		GCpositions = [GCpositions; (fixedStrand=="G" | fixedStrand=="C")];
	end

	% Quantify GC amount
	GCcontent = 100*sum(GCpositions,2) / length(fixedStrand);
	GCmoment = sum(GCpositions .* (1:length(fixedStrand)), 2 ) ./ sum(GCpositions,2);

	cmap = colormap("hot");
	colorID = 1 + floor( (size(cmap,1)-1)*(GCcontent-min(GCcontent)) / range(GCcontent) );

	close all;
	hold on;
	curves = zeros(84,26);
	for i=5:30
		load( sprintf("~/5MLx matfiles/sample%d-calibration.mat",i) );
		curves(:,i-4) = highCutoff;
		
		color = cmap(colorID(ceil((i-4)/2)),:);
		plot(highCutoff, "color", color, "linewidth", 1.5);
	end
	hold off;

	xlabel("MM position (bp)");
	ylabel("V-peak position");
	xlim([0 90])

	colormap("hot")
	caxis([min(GCcontent) max(GCcontent)])
	h = colorbar ('location', 'northoutside');
	title('Overall GC content (%)', 'fontweight','normal');
	print("~/FpeakVSpos.png", "-r300");
	
	return
end

% Plot local GC content vs repair efficiency
window = 9;

efficiency = [];
GCcontent = [];
avgEfficiency = [];
globalGCcontent = [];


filename = sprintf("~/combined1-2.mat", libID);
load(filename);
backgroundEfficiency = 0.37398; % minumum repair efficiency of combined3-4.mat for 3ML1 mutS deficient.
reprate = (reprate - backgroundEfficiency) / (1-backgroundEfficiency);

% Set the parameters depending on the chosen library
libname = '3ML1';
repairInit;
fixedStrand = [fixedStrand "GAGCTC"]; % Add the information about the fixed XhoI cut site.
isGC = (fixedStrand == 'G' | fixedStrand == 'C');
localGC = conv(isGC, ones(window,1)/window, 'same')*100; % in percentage

tempReprate = reshape(reprate(:,floor(window/2):len),1,[]);
tempGC = reshape(repmat(localGC(floor(window/2):len),4,1),1,[]);
selection = (~isnan(tempReprate));
efficiency = [efficiency tempReprate(selection) ];
GCcontent = [GCcontent tempGC(selection) ];
	
avgEfficiency = [avgEfficiency mean(tempReprate(selection)) ];
globalGCcontent = [globalGCcontent 100*sum(isGC)/length(fixedStrand) ];



for libID=1:1:4
	filename = sprintf("~/SSLmatfiles/SSL%d.mat", libID);
	load(filename);
	backgroundEfficiency = 0.37398; % minumum repair efficiency of combined3-4.mat for 3ML1 mutS deficient.
	reprate = (reprate - backgroundEfficiency) / (1-backgroundEfficiency);

	% Set the parameters depending on the chosen library
	libname = sprintf("SSL%d", libID);
	repairInit;
	fixedStrand = [fixedStrand "GAGCTC"]; % Add the information about the fixed XhoI cut site.
	isGC = (fixedStrand == 'G' | fixedStrand == 'C');
	localGC = conv(isGC, ones(window,1)/window, 'same')*100; % in percentage
		
	tempReprate = reshape(reprate(:,floor(window/2):len),1,[]);
	tempGC = reshape(repmat(localGC(floor(window/2):len),4,1),1,[]);
	selection = (~isnan(tempReprate));
	efficiency = [efficiency tempReprate(selection) ];
	GCcontent = [GCcontent tempGC(selection) ];
	
	avgEfficiency = [avgEfficiency mean(tempReprate(selection)) ];
	globalGCcontent = [globalGCcontent 100*sum(isGC)/length(fixedStrand) ];
end


for libID=1:1:13
	filename = sprintf("~/5MLx matfiles/combined%d-%d.mat", 2*libID+3, 2*libID+4);
	load(filename);
	backgroundEfficiency = 0.37398; % minumum repair efficiency of combined3-4.mat for 3ML1 mutS deficient.
	reprate = (reprate - backgroundEfficiency) / (1-backgroundEfficiency);

	% Set the parameters depending on the chosen library
	libname = sprintf("5ML%d", libID);
	repairInit;
	fixedStrand = [fixedStrand "GAGCTC"]; % Add the information about the fixed XhoI cut site.
	isGC = (fixedStrand == 'G' | fixedStrand == 'C');
	localGC = conv(isGC, ones(window,1)/window, 'same')*100; % in percentage
		
	tempReprate = reshape(reprate(:,floor(window/2):len),1,[]);
	tempGC = reshape(repmat(localGC(floor(window/2):len),4,1),1,[]);
	selection = (~isnan(tempReprate));
	efficiency = [efficiency tempReprate(selection) ];
	GCcontent = [GCcontent tempGC(selection) ];
	
	avgEfficiency = [avgEfficiency mean(tempReprate(selection)) ];
	globalGCcontent = [globalGCcontent 100*sum(isGC)/length(fixedStrand) ];
end


% Fitting to find the positive/negative correlation
p = polyfit(GCcontent, efficiency,1);
yfit = polyval(p, 0:1:100);
pearsonRho = mean( (GCcontent-mean(GCcontent)) .* (efficiency-mean(efficiency)) ) / (std(GCcontent,1)*std(efficiency,1));

set(0,"defaultaxesfontsize", 20)
plot(GCcontent+5*rand(1,length(GCcontent)), efficiency, 'r.');
hold on;
plot(0:1:100, yfit, 'k-', 'linewidth', 2);
hold off
text(50, 0.7, sprintf('\\rho = %.2f', pearsonRho), 'fontsize', 25);

xlabel('Local GC content (%)');
ylabel('\eta_s');
xlim([0 100]);
print('~/ATcontentVSetas.png', '-r300')


% Fitting to find the positive/negative correlation
p = polyfit(globalGCcontent, avgEfficiency,1);
yfit = polyval(p, 0:1:100);
pearsonRho = mean( (globalGCcontent-mean(globalGCcontent)) .* (avgEfficiency-mean(avgEfficiency)) ) / (std(globalGCcontent,1)*std(avgEfficiency,1));

set(0,"defaultaxesfontsize", 25)
plot(globalGCcontent(1), avgEfficiency(1), 'm^', 'markerfacecolor', 'auto', 'markersize', 10);
hold on;
plot(globalGCcontent(2:5), avgEfficiency(2:5), 'ro', 'markerfacecolor', 'auto', 'markersize', 10);
plot(globalGCcontent(6:end), avgEfficiency(6:end), 'bd', 'markerfacecolor', 'auto', 'markersize', 10);
plot(0:1:100, yfit, 'k-', 'linewidth', 2);
hold off
text(50, 0.7, sprintf('\\rho = %.2f', pearsonRho), 'fontsize', 25);

HLEG = legend('3ML1','SSLx', '5MLx');
legend("location", "north", 'orientation', 'horizontal')
legend boxoff;
lchildren = get(HLEG, 'children');
for i=1:3
	set(lchildren(i+3), "color", get(lchildren(i),'color') );
	set( lchildren(i), 'markersize', 2 );
end

xlabel('Total GC content (%)');
ylabel('<\eta_s>');
xlim([0 100]);
print('~/globalATcontentVSmeanetas.png', '-r300')


