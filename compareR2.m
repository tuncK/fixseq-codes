%%

if true
	filenamex = "/home/tk/marccOutput/wt.mat";
	filenamey = "/home/tk/matfiles/combined9-10.mat";
	
	labelx = 'wt';
	labely = '\Deltadam';
	
	load(filenamex);
	backgroundEfficiency = 0*0.37398; % minumum repair efficiency of combined3-4.mat for 3ML1 mutS deficient.
	reprate = (reprate - backgroundEfficiency) / (1-backgroundEfficiency);
	reprate1 = reprate;
	bias1 = bias;

	load(filenamey);
	reprate = (reprate - backgroundEfficiency) / (1-backgroundEfficiency);
	reprate2 = reprate;
	bias2 = bias;
end

if false
	labelx = "Replicate A";
	labely = 'Replicate B';

	reprate1 = [];
	reprate2 = [];
	bias1 = [];
	bias2 = [];
	for i=5:2:30
		filenamex = sprintf("/home/tk/5MLx matfiles/sample%d.mat",i);
		load(filenamex);
		reprate1 = [reprate1; reprate];
		bias1 = [bias1; bias];
		
		filenamey = sprintf("/home/tk/5MLx matfiles/sample%d.mat",i+1);
		load(filenamey);
		reprate2 = [reprate2; reprate];
		bias2 = [bias2; bias];
	end

	backgroundEfficiency = 0.37398; % minumum repair efficiency of combined3-4.mat for 3ML1 mutS deficient.
	reprate1 = (reprate1 - backgroundEfficiency) / (1-backgroundEfficiency);
	reprate2 = (reprate2 - backgroundEfficiency) / (1-backgroundEfficiency);
end

if false
	reprate1 = [];
	reprate2 = [];
	bias1 = [];
	bias2 = [];
	idxList = reshape( [ 1 2 1 3 2 3 2 1 3 1 3 2 ]' + (0:4:12), 1, []);
	
	for i= 1:2:length(idxList);
		idx = idxList(i);
		idy = idxList(i+1);
		
		filenamex = sprintf("/home/tk/SSLhists/%d.mat", idx);
		load(filenamex);
		reprate1 = [reprate1; reprate];
		bias1 = [bias1; bias];
		
		filenamey = sprintf("/home/tk/SSLhists/%d.mat", idy);
		load(filenamey);
		reprate2 = [reprate2; reprate];
		bias2 = [bias2; bias];
	end

	backgroundEfficiency = 0.37398; % minumum repair efficiency of combined3-4.mat for 3ML1 mutS deficient.
	reprate1 = (reprate1 - backgroundEfficiency) / (1-backgroundEfficiency);
	reprate2 = (reprate2 - backgroundEfficiency) / (1-backgroundEfficiency);
end


% Only data points valid in both sets are comparable, remove others.
repdata1 = reshape(reprate1,[],1);
repdata2 = reshape(reprate2,[],1);

biasdata1 = reshape(bias1,[],1);
biasdata2 = reshape(bias2,[],1);


valid12 = (~isnan(repdata1) & ~isnan(repdata2));
repdata1 = repdata1(valid12);
repdata2 = repdata2(valid12);

valid12 = (~isnan(biasdata1) & ~isnan(biasdata2));
biasdata1 = biasdata1(valid12);
biasdata2 = biasdata2(valid12);


% Coefficient of determination
repR2 = 1 - mean( (repdata1-repdata2).^2 ) / (std(repdata1)*std(repdata2));
biasR2 = 1 - mean( (biasdata1-biasdata2).^2 ) / (std(biasdata1)*std(biasdata2));

rmsRate = sqrt(mean( (repdata1-repdata2).^2 ))
rmsBias = sqrt(mean( (biasdata1-biasdata2).^2 ))

pkg load statistics
meanBias = mean(biasdata1-biasdata2);
stdBias = sqrt(std(biasdata1,1)^2+std(biasdata2,1)^2);
zBias = meanBias / stdBias
pBias = 1-2*normcdf(zBias)


set(0, 'defaultaxesfontsize', 25);

if false
	pkg load statistics
	close all;
	bins = -0.1:0.05:1.1;
	counts = hist3( [repdata1,repdata2], {bins,bins} );
	imagesc(bins, bins, log10(counts));
	hold on
	plot(0:1, 0:1, 'r-', 'linewidth', 3);
	hold off
	text(0, 0.8, sprintf('R^2 = %.3f', repR2), 'fontsize', 25);
	set(gca,'Ydir','normal');
	axis square;
	colormap(flipud(gray));
	caxis([0 5])
	xlabel('\eta_s replicate 1');
	ylabel('\eta_s replicate 2');
	colorbar
	print("~/color.png", "-r300")
	return
end


close all;
plot(repdata1, repdata2, 'bo', 'linewidth', 2);
hold on
plot(0:1, 0:1, 'k-', 'linewidth', 3);
hold off
text(0, 0.8, sprintf('R^2 = %.3f', repR2), 'fontsize', 25);
xlim([-0.1 1.1]);
ylim([-0.1 1.1]);
xlabel(sprintf('\\eta %s', labelx));
ylabel(sprintf('\\eta %s', labely));
print('~/Efficiency.png', '-r300');



close all;
plot(biasdata1, biasdata2, 'bo', 'linewidth', 2);
hold on;
plot(-1:1, -1:1, 'k-', 'linewidth', 3);
hold off
text(-1, 0.7, sprintf('R^2 = %.3f', biasR2), 'fontsize', 25);
xlim([-1.1 1.1]);
ylim([-1.1 1.1]);
xlabel(sprintf('\\beta %s', labelx));
ylabel(sprintf('\\beta %s', labely));
print('~/Bias.png', '-r300');


