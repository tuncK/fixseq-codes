%%


set(0,'DefaultAxesFontSize',25);
acceptableRange = 1:66; % for 3ML1
%acceptableRange = 1:84 % for 5MLx


% wt
filenamex = '~/3ML1 matfiles/combined1-2.mat';
load(filenamex);
datax = reshape(reprate(:,acceptableRange)', 1, []);

% Ignore entries that are NaN
isValid = (~isnan(datax));
datax = datax(isValid);
rangex = repmat(acceptableRange,1,4);
rangex = rangex(isValid);

close all
plot(rangex, datax, 'bd', 'MarkerFaceColor', 'auto');
ylim([-0.1 1.1]);
xlim([0 rangex(end)*1.05]);
xlabel('Base position');
ylabel('Repair efficiency (\eta)');
print('~/wtLevelBeforeCorrection.png', '-r300')


% mutS data
filenamex = '~/3ML1 matfiles/combined3-4.mat';
load(filenamex);
datay = reshape(reprate(:,acceptableRange)', 1, []);

% Ignore entries that are NaN
isValid = (~isnan(datay));
datay = datay(isValid);
rangey = repmat(acceptableRange,1,4);
rangey = rangey(isValid);
minLevel = min(datay);

close all
plot(rangey, datay, 'r^', 'MarkerFaceColor', 'auto');
hold on;
plot(rangey, minLevel*ones(size(rangey)), "k.", 'markersize', 10);
hold off;
ylim([-0.1 1.1]);
xlim([0 rangey(end)*1.05]);
xlabel('Base position');
ylabel('Repair efficiency (\eta)');
print('~/mutsLevelBeforeCorrection.png', '-r300')


% Replot wt data
close all
plot(rangex, (datax-minLevel)/(1-minLevel), 'bd', 'MarkerFaceColor', 'auto');
ylim([-0.1 1.1]);
xlim([0 rangex(end)*1.05]);
xlabel('Base position');
ylabel('Repair efficiency (\eta_s)');
print('~/wtLevelAfterCorrection.png', '-r300')


% Replot muts data
close all
plot(rangey, (datay-minLevel)/(1-minLevel), 'r^', 'MarkerFaceColor', 'auto');
ylim([-0.1 1.1]);
xlim([0 rangey(end)*1.05]);
xlabel('Base position');
ylabel('Repair efficiency (\eta_s)');
print('~/mutsLevelAfterCorrection.png', '-r300')


