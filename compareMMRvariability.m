%%%
% Compares the repair efficiency of CC vs GG mismatches in different MMR mutants to deduce the contrast.

addpath ./common/
more off;

libname = '3ML1';
repairInit;


strains = { 'muts', 'mutl', 'muth', 'uvrd', 'wt' };

close all;
set(0, 'defaultaxesfontsize', 22);
hold on;
for fileID=1:length(strains)
	filename = sprintf("~/mmr/%s.mat", strains{fileID});
	load(filename);
	
	CCefficiency = reprate(3,find(fixedStrand=='C'));
	GGefficiency = reprate(4,find(fixedStrand=='G'));
	
	h(1) = plot( fileID+0.4*rand(size(CCefficiency))-0.2 , CCefficiency, "ro", "linewidth", 1.5);
	h(2) = plot( fileID+0.4*rand(size(GGefficiency))-0.2 , GGefficiency, "b^", "linewidth", 1.5);
	set(h,"markerfacecolor","auto")	
	
	h2(1) = errorbar( fileID, mean(CCefficiency), std(CCefficiency,1), 'kd');
	h2(2) = errorbar( fileID, mean(GGefficiency), std(GGefficiency,1), 'kd');
	set(h2, 'linewidth', 2);
	set(h2,"markerfacecolor","auto")
	
	fprintf("%s CC: %f +/- %f \t GG: %f +/- %f\n", strains{fileID}, mean(CCefficiency), std(CCefficiency,1), mean(GGefficiency), std(GGefficiency,1) );
end
hold off;

xlabel('Cell strain');
ylabel('Repair efficiency (\eta)');
set(gca, 'XTick', 1:5)
cellTypes = { '\DeltamutS', '\DeltamutL', '\DeltamutH', '\DeltauvrD', 'wt' };
set(gca, 'XTickLabel', cellTypes)
xlim([0.5 5.5]);
ylim([0 1]);

HLEG = legend(h, 'CC mismatches', 'GG mismatches');
legend("location", "northoutside", 'orientation', 'horizontal')
legend boxoff;
lchildren = get(HLEG, 'children');
for i=1:2
	set(lchildren(i+2), "color", get(lchildren(i),'color') );
end

pause(0.1)
print('~/CC-GG-contrast.png','-r300');



