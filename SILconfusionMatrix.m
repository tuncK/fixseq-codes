%%

isSIL = true;

data = [];
if isSIL
	for fileID=4
		filename = sprintf('~/sil/%d.csv', fileID);
		file = fopen(filename);
		x = fscanf(file, '%d', [4,inf])';
		fclose(file);
		data = [data; x];
	end
	numCases = 4;
else
	for fileID=5:11
		filename = sprintf('~/dil/%d.csv', fileID);
		file = fopen(filename);
		x = fscanf(file, '%d', [4,inf])';
		fclose(file);
		data = [data; x];
	end
	numCases = 16;
end


len = 65;
pkg load statistics
crosstalkPos = hist3(x(:,[1 3]), {1:len 1:len});
crosstalkBase = hist3(x(:,[2 4]), {0:numCases-1 0:numCases-1});

% Normalise the confusion matrices row by row
crosstalkBase = crosstalkBase ./ sum(crosstalkBase,2);
crosstalkPos = crosstalkPos ./ sum(crosstalkPos,2);

% Print assay accuracy statistics
set(0, 'defaultaxesfontsize', 25)
close all;
pause(0.01);
imagesc(crosstalkBase);
set(gca,'Ydir','normal');
axis square;
colormap(flipud(gray));
xlabel('Detected base');
ylabel('Expected base');
caxis([0 1]);
set(gca,'Xtick',1:len);
set(gca,'Ytick',1:numCases);

if isSIL
	set(gca,'XTickLabel',  {'A','T','C','G'})
	set(gca,'YTickLabel', {'A','T','C','G'} )
else
	set(gca,'XTickLabel', {'AA','AT','AC','AG', 'TA', 'TT', 'TC', 'TG', 'CA', 'CT', 'CC', 'CG', 'GA', 'GT', 'GC', 'GG'} );
	set(gca,'YTickLabel', {'AA','AT','AC','AG', 'TA', 'TT', 'TC', 'TG', 'CA', 'CT', 'CC', 'CG', 'GA', 'GT', 'GC', 'GG'} );
end
set(gca,'TickLength',[0 0]);
print('~/mutSSILtype.png', '-r300')


close all;
pause(0.01);
imagesc(crosstalkPos);
set(gca,'Ydir','normal');
axis square;
colormap(flipud(gray));
caxis([0 1]);
xlabel('Detected position');
ylabel('Expected position');
print('~/mutSSILposition.png', '-r300')



