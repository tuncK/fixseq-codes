%%%
% Analyses the outliers that are difficult to repair for double barcoded libraries.

addpath ./common/
more off;

allTriplets = {};
allPentamers = {};
allHeptamers = {};
tripletEfficiency = [];
pentamerEfficiency = [];
heptamerEfficiency = [];

nonamerSought = "CAAAAAT";
allNonamers = {};
nonamerEfficiency = [];


for fileID=[1:3 5:7 9:11 13:15]
	libID = ceil(fileID / 4);
	
	% Set the parameters depending on the chosen library
	libname = sprintf("SSL%d", libID);
	repairInit;
	fixedStrand = [fixedStrand "GAGCTC"]; % Add the information about the fixed XhoI cut site.
	
	filename = sprintf("~/SSLmatfiles/%d.mat", fileID);
	load(filename);
	backgroundEfficiency = 0.37398; % minumum repair efficiency of combined3-4.mat for 3ML1 mutS deficient.
	reprate = (reprate - backgroundEfficiency) / (1-backgroundEfficiency);
	
	for pos=2:1:len
		for base = 1:1:4
			if isnan(reprate(base,pos))
				continue
			end
			
			outlierTriplet = [ fixedStrand( (pos+1):-1:(pos-1) ) '-' num2dna(base) ];
			allTriplets{end+1} = outlierTriplet;
			tripletEfficiency = [tripletEfficiency; reprate(base,pos) ];
		end
	end
	
	for pos=3:1:len
		for base = 1:1:4
			if isnan(reprate(base,pos))
				continue
			end
			
			outlierPentamer = [ fixedStrand( (pos+2):-1:(pos-2) ) '-' num2dna(base) ];
			allPentamers{end+1} = outlierPentamer;
			pentamerEfficiency = [pentamerEfficiency; reprate(base,pos) ];
		end
	end
	
	for pos=4:1:len
		for base = 1:1:4
			if isnan(reprate(base,pos))
				continue
			end
			
			allHeptamers{end+1} = [ fixedStrand( (pos+3):-1:(pos-3) ) '-' num2dna(base) ];
			heptamerEfficiency = [ heptamerEfficiency; reprate(base,pos) ];
		end
	end
	
	% Statistics for the motif used for cosine similarity arguments
	nonamerPos = strfind(fixedStrand, nonamerSought);
	for i=1:length(nonamerPos)
		nonamer = substr(fixedStrand, nonamerPos(i)-1, 9);
		pos = nonamerPos(i)+4;
		for base = 1:1:4
			if isnan(reprate(base,pos))
				continue
			end
			allNonamers{end+1} = [ nonamer '-' num2dna(base) ];
			nonamerEfficiency = [nonamerEfficiency; reprate(base,pos) ];
		end
	end
end


file3 = fopen("~/trimerSSL.csv", "wt");
fprintf(file3, "MMType,Strand1(5>3),Strand2(3>5),Mean,STDEV,Number,\n");
			
file5 = fopen("~/pentamerSSL.csv", "wt");
fprintf(file5, "MMType,Strand1(5>3),Strand2(3>5),Mean,STDEV,Number,\n");

file7 = fopen("~/heptamerSSL.csv", "wt");
fprintf(file7, "MMType,Strand1(5>3),Strand2(3>5),Mean,STDEV,Number,\n");

for MMtype = { "AA", "AC", "AG", "CA", "CC", "CT", "GA", "GG", "GT", "TC", "TG", "TT" }
	for base1="ACGT"
		for base2="ACGT"
			hits1 = strcmp(allTriplets, sprintf("%s%s%s-%s", base1, MMtype{}(1), base2, MMtype{}(2)) );
			hits2 = strcmp(allTriplets, sprintf("%s%s%s-%s", complementary(base2), MMtype{}(2), complementary(base1), MMtype{}(1)) );
			selection = ( hits1 | hits2 )';
			
			selectedRates = tripletEfficiency(selection);
			numSelected = length(selectedRates);
			if numSelected > 0
				avg = mean(selectedRates);
				stdev = std(selectedRates,1);
				strand1 = sprintf("%s%s%s",base1, MMtype{}(1), base2);
				strand2 = sprintf("%s%s%s", complementary(base1), MMtype{}(2), complementary(base2) );
				fprintf(file3, "%s, %s, %s, %.3f, %.3f, %d,\n", MMtype{}, strand1, strand2, avg, stdev, numSelected);
			end
				
			for base3="ACGT"
				for base4="ACGT"
					hits1 = strcmp( allPentamers, sprintf("%s%s%s%s%s-%s", base1, base2, MMtype{}(1), base3, base4, MMtype{}(2)) );
					hits2 = strcmp( allPentamers, sprintf("%s%s%s%s%s-%s", complementary(base4), complementary(base3), MMtype{}(2), complementary(base2), complementary(base1), MMtype{}(1)) );
					selection = ( hits1 | hits2 )';
					
					selectedRates = pentamerEfficiency(selection);
					numSelected = length(selectedRates);
					if numSelected > 0
						avg = mean(selectedRates);
						stdev = std(selectedRates,1);
						strand1 = sprintf("%s%s%s",base1, base2, MMtype{}(1), base3, base4);
						strand2 = sprintf("%s%s%s", complementary(base1), complementary(base2), MMtype{}(2), complementary(base3), complementary(base4) );	
						fprintf(file5, "%s, %s, %s, %.3f, %.3f, %d,\n", MMtype{}, strand1, strand2, avg, stdev, numSelected);
					end
					
					for base5="ACGT"
						for base6="ACGT"
							hits1 = strcmp( allHeptamers, sprintf("%s%s%s%s%s%s%s-%s", base1, base2, base3, MMtype{}(1), base4, base5, base6, MMtype{}(2)) );
							hits2 = strcmp( allHeptamers, sprintf("%s%s%s%s%s%s%s-%s", complementary(base6), complementary(base5), complementary(base4), MMtype{}(2), complementary(base3), complementary(base2), complementary(base1), MMtype{}(1)) );
							selection = ( hits1 | hits2 )';
							
							selectedRates = heptamerEfficiency(selection);
							numSelected = length(selectedRates);
							if numSelected == 0
								continue;
							end
							avg = mean(selectedRates);
							stdev = std(selectedRates,1);
							strand1 = sprintf("%s%s%s%s%s%s%s",base1, base2, base3, MMtype{}(1), base4, base5, base6);
							strand2 = sprintf("%s%s%s%s%s%s%s", complementary(base1), complementary(base2), complementary(base3), MMtype{}(2), complementary(base4), complementary(base5), complementary(base6) );
							fprintf(file7, "%s, %s, %s, %.3f, %.3f, %d,\n", MMtype{}, strand1, strand2, avg, stdev, numSelected);
						end
					end
				end
			end
		end	
	end
end

fclose(file3);
fclose(file5);
fclose(file7);



% Statistics for nonamer motifs
for MMtype = { "AA", "AC", "AG" }
	for base1="ACGT"
		for base2="ACGT"
			searchKey = sprintf("%s%s%s-%s", base1, nonamerSought, base2, MMtype{}(2));
			selection = strcmp(allNonamers, searchKey)';
			selectedRates = tripletEfficiency(selection);
			numSelected = length(selectedRates);
			if numSelected > 0
				avg = mean(selectedRates);
				stdev = std(selectedRates,1);
				fprintf("%s, %.3f, %.3f, %d,\n", searchKey,  avg, stdev/sqrt(numSelected), numSelected);
			end
		end
	end
end



