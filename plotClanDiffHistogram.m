%%%

addpath ./common

filename = "~/DEB3L/sample1.hist";
load(filename);
libname = 'DEB3L';
repairInit;

mkdir('~/clans/');

for i=1:1:50
	block = hists(:,:,i);
	numreads = sum(block(:,1));
	if numreads < 10
		continue;
	end

	block /= numreads;
	difference = block - original;
	
	outfilename = sprintf('~/clans/%d.png', i);
	drawBoard(difference, outfilename, -1, 1, libname, 'redBlue', false);
end



