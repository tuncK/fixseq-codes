%%%
% Analyses the outliers that are difficult to repair for double barcoded libraries.

addpath ./common/
more off;

allTriplets = {};
tripletEfficiency = [];

for libID=1:1:13
	% Set the parameters depending on the chosen library
	libname = sprintf("5ML%d", libID);
	repairInit;
	fixedStrand = [fixedStrand "GAGCTC"]; % Add the information about the fixed XhoI cut site.
	
	filename = sprintf("~/5MLx matfiles/combined%d-%d.mat", 2*libID+3, 2*libID+4);
	load(filename);
	backgroundEfficiency = 0.37398; % minumum repair efficiency of combined3-4.mat for 3ML1 mutS deficient.
	reprate = (reprate - backgroundEfficiency) / (1-backgroundEfficiency);

	for pos=3:1:len
		for base = 1:1:4
			if isnan(reprate(base,pos))
				continue
			end
			
			outlierTriplet = [ fixedStrand( (pos+2):-1:(pos-2) ) '-' num2dna(base) ];
			allTriplets{end+1} = outlierTriplet;
			tripletEfficiency = [tripletEfficiency; reprate(base,pos) ];
		end
	end
end


% Generate a grid scheme for markers to be located at.
xshifts = (rem(0:15,4)-1.5) / 5;
yshifts = (floor((0:15)/4)-1.5) / 5;

% Make separate plots for AG, CT, TT mismatches
% Label the mismatches w.r.t. their MM type or context and plot
types2check = {"AG", "GA", "CT", "TC", "TT"};
contexts = {"AA", "AC", "CA", "AG", "GA", "AT", "TA", "CC", "CG", "GC", "CT", "TC", "GG", "GT", "TG", "TT"};

set(0, 'defaultaxesfontsize', 18);
for j=1:length(types2check)
	MM1 = types2check{j}(1);
	MM2 = types2check{j}(2);
	
	NNLabels = {};
	xCoordinates = [];
	yCoordinates = [];
	cCoordinates = [];
	yindex = 0;
	for base1 = "RY"
		for base4 = "RY"
			NNLabels{end+1} = sprintf("%s%s",base1,base4);
			yindex++;
			for xindex=1:length(contexts)
				base2 = contexts{xindex}(1);
				base3 = contexts{xindex}(2);
				
				query = sprintf("%s%s%s%s%s-%s", base1, base2, MM1, base3, base4, MM2);
				query2 = sprintf("%s%s%s%s%s-%s", complementary(base4), complementary(base3), MM2, complementary(base2), complementary(base1), MM1 );	
				query = strrep(query,"R","[AG]");
				query = strrep(query,"Y","[CT]");
				query = strrep(query,"N","[ACGT]");
				query2 = strrep(query2,"R","[AG]");
				query2 = strrep(query2,"Y","[CT]");
				query2 = strrep(query2,"N","[ACGT]");
				
				selection = (~cellfun( @isempty, regexp(allTriplets,query) ) | ~cellfun( @isempty, regexp(allTriplets,query2) ) );
				selectedRates = tripletEfficiency(selection);
				numSelected = length(selectedRates);
				xCoordinates = [ xCoordinates, xindex+xshifts(1:numSelected) ];
				yCoordinates = [ yCoordinates, yindex+yshifts(1:numSelected) ];
				cCoordinates = [ cCoordinates; selectedRates ];
			end
		end
	end
	
	close all;
	h = scatter(xCoordinates, yCoordinates, 20, cCoordinates, "o");
	hold on
	% Make custom grid lines
	for x = 1.5:(1+length(contexts))
		plot( x*ones(1,length(NNLabels)+2), 0:length(NNLabels)+1, "k:");
	end
	for y = 1.5:length(NNLabels)+1
		plot( 0:(1+length(contexts)), y*ones(1,2+length(contexts)), "k:");
	end
	hold off
	
	set(h, "markerfacecolor", "auto");
	set(h, "markeredgecolor", "k");
	set(h, "linewidth", 1);
	
	% Rotated X-axis labels
	set(gca,'XTick', [] )
	text(1:16, 4.6*ones(16,1), contexts, "rotation", 90, 'fontsize', 18);
	set(gca,'YTick', [] )
	text(16.6*ones(4,1), 1:4, NNLabels, "fontsize", 18);
	axis equal
	
	xlim([0.5 (0.5+length(contexts)) ]);
	ylim([0.5 length(NNLabels)+0.5 ]);
	colormap("hot");
	caxis([0 1]);
	xlabel('Nearest bases (PQ)');
	ylabel("Next-nearest\n bases (SR)");
	set(gca,'position', [0.13 0.1 0.8 1.3])
	 
	pause(0.1)
	print(sprintf('~/%s.png', types2check{j}),'-r300');
end


