%%%
% Analyses the results of the control experiment, where we formed mismatch 'libraries' that contain only one mismatch each.

addpath ./common


sbins = -0.2:0.05:1.2;
posBins = 1:65;
histResults = zeros(6,length(sbins));
maxSubsPosHist = zeros(6,length(posBins));
maxSubsBaseHist = zeros(6,4);

fileID = 1;
for lib = {'LP', 'LM', 'LD', 'HP', 'HM', 'HD' }
	libname = lib{};
	repairInit;
	
	for files = dir(sprintf('~/07102019/%s/*.hist', libname))'
		filename = [files.folder '/' files.name];
		load(filename);
		size(hists)
		
		numReadsList = sum(hists(:,1,:),1);
		seqDeepEnough = squeeze(numReadsList >= 10)';
		hists = hists(:,:,seqDeepEnough);
		numReadsList = sum(hists(:,1,:),1);
		
		frequencies = ( hists ./ numReadsList );
		differences = frequencies - original;
		maxChanges = squeeze(max(differences, [], 1));
		[maxSubstitutionFreq, maxSubstitutionPos] = max(maxChanges, [], 1);
		
		% Find MaxSubstituted base type
		maxSubstitutionBase = zeros(size(differences,3),1);
		for i=1:1:size(differences,3)
			[~,maxSubstitutionBase(i)] = max(differences(:,maxSubstitutionPos(i),i));
		end
				
		histResults(fileID,:) += histc(maxSubstitutionFreq, sbins);
		maxSubsPosHist(fileID, :) += histc(maxSubstitutionPos, posBins);
		maxSubsBaseHist(fileID, :) += histc(maxSubstitutionBase, 1:4)';
	end
	
	fileID++;
end

histResultsNorm = histResults ./ sum(histResults,2);
maxSubsPosHist = maxSubsPosHist ./ sum(maxSubsPosHist,2);
maxSubsBaseHist = maxSubsBaseHist ./ sum(maxSubsBaseHist,2);


set(0,"defaultaxesfontsize",25);

close all;
h(1) = plot(sbins, histResultsNorm(1,:), "r--", "linewidth",1)(1);
hold on;
h(2) = plot(sbins, histResultsNorm(2,:), "m-", "linewidth",1.5)(1);
h(3) = plot(sbins, histResultsNorm(3,:), "k:", "linewidth",1.5)(1);
hold off;

%ylim([0 0.31]);
xlabel("Substitution prevalence")
ylabel("Frequency");
xlim([-0.05 1.05]);

HLEG = legend(h, 'LP', 'LM', 'LD' );
legend("location", "north", 'orientation', 'horizontal')
legend boxoff;
lchildren = get(HLEG, 'children');
for i=1:3
	set(lchildren(i+3), "color", get(lchildren(i),'color') );
end
print("~/shistL.png", "-r300")


close all;
h(1) = plot(sbins, histResultsNorm(4,:), "g--", "linewidth",1)(1);
hold on;
h(2) = plot(sbins, histResultsNorm(5,:), "b-", "linewidth",1.5)(1);
h(3) = plot(sbins, histResultsNorm(6,:), "c:", "linewidth",1.5)(1);
hold off;

%ylim([0 0.31]);
xlabel("Substitution prevalence")
ylabel("Frequency");
xlim([-0.05 1.05]);

HLEG = legend(h, 'HP', 'HM', 'HD' );
legend("location", "north", 'orientation', 'horizontal')
legend boxoff;
lchildren = get(HLEG, 'children');
for i=1:3
	set(lchildren(i+3), "color", get(lchildren(i),'color') );
end
print("~/shistH.png", "-r300")



close all;
h(1) = plot(posBins, maxSubsPosHist(1,:), "r--", "linewidth",1)(1);
hold on;
h(2) = plot(posBins, maxSubsPosHist(2,:), "m-", "linewidth",1.5)(1);
h(3) = plot(posBins, maxSubsPosHist(3,:), "k:", "linewidth",1.5)(1);
h(4) = plot(posBins, maxSubsPosHist(4,:), "g-.", "linewidth",1.5)(1);
h(5) = plot(posBins, maxSubsPosHist(5,:), "bo", "linewidth",1.5)(1);
plot(posBins, maxSubsPosHist(5,:), "b-.", "linewidth",1);
h(6) = plot(posBins, maxSubsPosHist(6,:), "cv", "linewidth",1.5)(1);
plot(posBins, maxSubsPosHist(6,:), "c-.", "linewidth",1);
hold off;
set(get(gca, 'children'), 'markerfacecolor', 'auto')

xlabel("Base position")
ylabel("Frequency");
xlim([-1 len+1]);
ylim([0 1]);

HLEG = legend(h, 'LP', 'LM', 'LD', 'HP', 'HM', 'HD' );
legend("location", "northoutside", 'orientation', 'horizontal')
legend boxoff;
lchildren = get(HLEG, 'children');
for i=1:6
	set(lchildren(i+6), "color", get(lchildren(i),'color') );
end
print("~/poshist.png", "-r300")


close all;
h = bar(1:4, maxSubsBaseHist', 'edgecolor', 'none');

set(h(1), 'facecolor', 'r');
set(h(2), 'facecolor', 'm');
set(h(3), 'facecolor', 'k');
set(h(4), 'facecolor', 'g');
set(h(5), 'facecolor', 'b');
set(h(6), 'facecolor', 'c');

xlabel("Substituted base type")
ylabel("Frequency");
xlim([0 5]);
ylim([0 1]);
set(gca, 'xticklabel', {'A', 'T', 'C', 'G'});

HLEG = legend(h, 'LP', 'LM', 'LD', 'HP', 'HM', 'HD' );
legend("location", "northoutside", 'orientation', 'horizontal')
legend boxoff;
lchildren = get(HLEG, 'children');
for i=1:6
	set(lchildren(i+6), "color", get(lchildren(i),'facecolor') );
end
print("~/basehist.png", "-r300")


