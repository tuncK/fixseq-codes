%% Accortding to the formula :
% \sigma_{\eta_{ij}} \approx \frac{ \sqrt{ F_{ij} + V_{ij} + \eta^2_{ij} \sigma^2_c }}{ F_{ij} + U_{ij} + V_{ij} }
% Calculates the expected uncertainty per each measurement

set(0, 'defaultaxesfontsize', 20);
close all
hold on;

for i=5:2:30


filename1 = sprintf("/home/tk/5MLx matfiles/sample%d.mat",i);
load(filename1);
totalrepairedF = repairedF;
totalrepairedV = repairedV;
totalunrepaired = unrepaired;
reprate1 = (repairedF + repairedV) ./ (repairedF + repairedV + unrepaired);

filename2 = sprintf("/home/tk/5MLx matfiles/sample%d.mat",i+1);
load(filename2);
totalrepairedF += repairedF;
totalrepairedV += repairedV;
totalunrepaired += unrepaired;
reprate2 = (repairedF + repairedV) ./ (repairedF + repairedV + unrepaired);

% Recalculate the repair rate/bias based on the cumulated counts
reprate = (totalrepairedF + totalrepairedV) ./ (totalrepairedF + totalrepairedV + totalunrepaired);
deta_exp = sqrt( totalrepairedF + totalrepairedV + reprate.^2*0 ) ./ (totalrepairedF + totalrepairedV + totalunrepaired);
deta_obs = abs(reprate1-reprate2);

deta_exp = reshape(deta_exp,1,[]);
deta_obs = reshape(deta_obs,1,[]);

expAvg = nanmean(deta_exp);
expStd = nanstd(deta_exp,1);
obsAvg = nanmean(deta_obs);
obsStd = nanstd(deta_obs,1);

plot(deta_exp, deta_obs, 'r.', 'linewidth', 2);
errorbar(expAvg, obsAvg, expStd, obsStd, "#~>.k");


end
hold off

xlabel('Theoretical uncertainty');
ylabel('Observed variability');

print('predictedErrors.png', '-r300');


