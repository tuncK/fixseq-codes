%%
% For 3ML1 library comparison
len = 66;

legendList = {'wt', '\DeltamutS', '\DeltamutH', '\Deltadam'};


entropies = zeros(len,16);
counter = 1;
% wt wthm muts mutShm mutH mutHhm dam damhm
for i= [ 1:2 5:6 3:4 11:12 29:30 35:36 33:34 25:26 ]
	filename = sprintf("/home/tk/3ML1/sample%d.hist",i);
	load(filename); % load mat file with clan histograms

	% Entropy calculations
	cumHist = sum(hists,3);
	numReads = sum(cumHist,1)(1);
	probability = cumHist(:,8:end,:) / numReads;
	logprob = log2(probability);
	logprob(probability==0) = 0;
	entropies(:,counter) = -sum(probability .* logprob, 1);
	counter++;
end

EntropyMean = [ mean(entropies(:,1:2),2) mean(entropies(:,3:4),2) mean(entropies(:,5:6),2) mean(entropies(:,7:8),2) mean(entropies(:,9:10),2) mean(entropies(:,11:12),2) mean(entropies(:,13:14),2) mean(entropies(:,15:16),2) ];
EntropyStd = [ std(entropies(:,1:2),0,2) std(entropies(:,3:4),0,2) std(entropies(:,5:6),0,2) std(entropies(:,7:8),0,2) std(entropies(:,9:10),0,2) std(entropies(:,11:12),0,2) std(entropies(:,13:14),0,2) std(entropies(:,15:16),0,2) ] /sqrt(2) ;


entropyt19 = EntropyMean(:,1:2:end);
entropyhm19 = EntropyMean(:,2:2:end);
entropyt19std = EntropyStd(:,1:2:end);
entropyhm19std = EntropyStd(:,2:2:end);
difference = entropyt19-entropyhm19;
std2difference = sqrt( entropyt19std.^2 + entropyhm19std.^2 );


set(0, 'defaultaxesfontsize', 22);
h(1)=errorbar(difference(:,1), std2difference(:,1), 'ro');
hold on;
h(2)=errorbar(difference(:,2), std2difference(:,2), 'g^');
h(3)=errorbar(difference(:,3), std2difference(:,3), 'mv');
h(4)=errorbar(difference(:,4), std2difference(:,4), 'kd');
plot(0:len, zeros(len+1,1), 'k-');
hold off;
set(h, 'markersize', 5, 'markerfacecolor', 'auto');

xlabel('Position');
ylabel('S(tUNC19) - S(hm19)');
HLEG=legend(h, legendList );
legend boxoff;
legend('location', 'north', 'orientation', 'horizontal');
set(HLEG,'color','none');
lchildren = get(HLEG, 'children');
for i=1:4
	set(lchildren(i+4), "color", get(lchildren(i),'color') );
end
set(lchildren(1:4), "markersize", 12);
print('~/3ML1-entropyHM19.png', '-r300');



% Evaluation of the order of samples per position
hist(difference, -0.04:0.02:0.04, 'edgecolor', 'none');
h = get(gca, 'children');
set(h(4), 'facecolor', 'r');
set(h(3), 'facecolor', 'g');
set(h(2), 'facecolor', 'm');
set(h(1), 'facecolor', 'k');

xlabel('S(tUNC19) - S(hm19)');
ylabel('Number of positions');
HLEG = legend(legendList);
legend('location', 'northoutside', 'orientation', 'horizontal')
set(HLEG, 'color', 'none');
legend boxoff;
lchildren = get(HLEG, 'children');
for i=1:4
	set(lchildren(i+4), "color", get(lchildren(i),'facecolor') );
end
print('~/3ML1-hm19-entropyDiffHist.png', '-r300');


