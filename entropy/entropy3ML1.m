%%
% For 3ML1 library comparison
len = 66;

entropies = zeros(len,7);
counter = 1;
for i= [1:2, 3:4, 22:24 ]
	filename = sprintf("/home/tk/3ML1/sample%d.hist",i);
	load(filename); % load mat file with clan histograms

	% Entropy calculations
	cumHist = sum(hists,3);
	numReads = sum(cumHist,1)(1);
	probability = cumHist(:,8:end,:) / numReads;z
	logprob = log2(probability);
	logprob(probability==0) = 0;
	entropies(:,counter) = -sum(probability .* logprob, 1);
	counter++;
end
	
EntropyMean = [mean(entropies(:,1:2),2) mean(entropies(:,3:4),2) mean(entropies(:,5:7),2) ];
EntropyStd = [std(entropies(:,1:2),0,2)/sqrt(2) std(entropies(:,3:4),0,2)/sqrt(2) std(entropies(:,5:7),0,2)/sqrt(3) ];


% Expected entropy of a uniform MM distribution
b = 0.5;
uniformp = b/(len*3);
uniformq = 1-3*uniformp;

% Account for MiSeq error
errorRate = 0.00; % 0.5%
p = uniformp*(1-errorRate) + 2*errorRate*1/3*uniformp + errorRate*1/3*uniformq;
q = 1-3*p;
uniforme = -3*p*log2(p)-q*log2(q);
uniforme = [uniforme*ones(len,1)];

set(0, 'defaultaxesfontsize', 20);
h(1) = plot(1:len, uniforme, 'b-', 'linewidth', 2);
hold on;
h(2)=errorbar(EntropyMean(:,1), EntropyStd(:,1), 'ro');
h(3)=errorbar(EntropyMean(:,2), EntropyStd(:,2), 'g^');
h(4)=errorbar(EntropyMean(:,3), EntropyStd(:,3), 'mv');
set(h, 'markersize', 5, 'markerfacecolor', 'auto');
hold off;

xlabel('Position');
ylabel('Entropy');

HLEG=legend(h, 'Ideal library', '3ML1-wt', '3ML1-\DeltamutS', '3CL1-wt');
legend boxoff;
set(HLEG, 'location', 'northoutside', 'orientation', 'horizontal', 'color','none');
lchildren = get(HLEG, 'children');
for i=1:4
	set(lchildren(i+4), "color", get(lchildren(i),'color') );
end
set(lchildren(1:3), "markersize", 10);
print('~/3ML1entropy.png', '-r300');


% Evaluation of the order of samples per position
[~, idx] = sort(EntropyMean,2,'descend');
hist(idx, 1:3, 'edgecolor', 'none');
h = get(gca, 'children');
set(h(3), 'facecolor', 'r');
set(h(2), 'facecolor', 'g');
set(h(1), 'facecolor', 'm');

lims = get(gca, 'xlim');
hold on
plot(lims(1):lims(2), len/3*ones(size(lims(1):lims(2))), 'k-')
hold off;

xlabel('Entropy rank');
ylabel('Number of positions');
HLEG = legend('3ML1-wt', '3ML1-\DeltamutS', '3CL1-wt');
legend('location', 'north', 'orientation', 'horizontal')
set(HLEG, 'color', 'none');
legend boxoff;

for i=1:3
	set(lchildren(i+3), "color", get(lchildren(i),'facecolor') );
end
set(gca, 'xticklabel', {'Highest', 'Medium', 'Lowest'} );

%p-value statistics
counts = hist(idx, 1:3);
x = len/3 + abs(counts-len/3);
p = 1- binocdf(x,len,1/3);

for i = 1:3
	text(0.6+i*0.2, 25, sprintf('p=%.3f', p(i,1)), 'fontsize', 20, 'rotation', 90);
	text(1.6+i*0.2, 25, sprintf('p=%.3f', p(i,2)), 'fontsize', 20, 'rotation', 90);
	text(2.6+i*0.2, 25, sprintf('p=%.3f', p(i,3)), 'fontsize', 20, 'rotation', 90);
end

print('~/3ML1entropyRanks.png', '-r300');


