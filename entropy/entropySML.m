%%
% for SML vs NPL comparison
len = 53;

entropies = zeros(len,12);
counter = 1;
% NPL wt, NPL dmutS, SML wt, SML dmutS
for i= [24:26, 27:29, 74:77, 90:91]
	filename = sprintf("/home/tk/SML/sample%d.hist",i);
	load(filename); % load mat file with clan histograms

	% Entropy calculations
	cumHist = sum(hists,3);
	numReads = sum(cumHist,1)(1);
	probability = cumHist / numReads;
	logprob = log2(probability);
	logprob(probability==0) = 0;
	entropies(:,counter) = -sum(probability .* logprob, 1);
	counter++;
end
	
EntropyMean = [mean(entropies(:,1:3),2) mean(entropies(:,4:6),2) mean(entropies(:,7:10),2) mean(entropies(:,11:12),2) ];
EntropyStd = [ std(entropies(:,1:3),0,2)/sqrt(3) std(entropies(:,4:6),0,2)/sqrt(3) std(entropies(:,7:10),0,2)/sqrt(4) std(entropies(:,11:12),0,2)/sqrt(2) ];


% Expected entropy of a uniform MM distribution
b = 0.5;
uniformp = b/(50*3);
uniformq = 1-3*uniformp;

% Account for MiSeq error
errorRate = 0; % 0.5%
p = uniformp*(1-errorRate) + 2*errorRate*1/3*uniformp + errorRate*1/3*uniformq;
q = 1-3*p;
uniforme = -3*p*log2(p)-q*log2(q);
uniforme = [uniforme*ones(len-3,1); 0; 0; 0];

set(0, 'defaultaxesfontsize', 20);
h(1)=plot(1:len, uniforme, 'b-', 'linewidth', 2);
hold on;
h(2)=errorbar(EntropyMean(:,1), EntropyStd(:,1), 'mv');
h(3)=errorbar(EntropyMean(:,2), EntropyStd(:,2), 'co');
h(4)=errorbar(EntropyMean(:,3), EntropyStd(:,3), 'ro');
h(5)=errorbar(EntropyMean(:,4), EntropyStd(:,4), 'g^');
set(h, 'markersize', 5, 'markerfacecolor', 'auto');
hold off;

xlim([ 0 len+1]);
xlabel('Base position');
ylabel('Entropy');

HLEG=legend(h, 'Ideal library', 'NPL-wt', 'NPL-\DeltamutS', 'SML-wt', 'SML-\DeltamutS');
legend boxoff;
legend('location', 'north', 'orientation', 'horizontal');
set(HLEG, 'color', 'none');
lchildren = get(HLEG, 'children');
for i=1:5
	set(lchildren(i+5), "color", get(lchildren(i),'color') );
end
set(lchildren(1:5), "markersize", 12);

print('/home/tk/SMLentropy.png', '-r300');


