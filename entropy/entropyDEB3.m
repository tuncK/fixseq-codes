%% Comparison of hemi-methylation effect on DEB3 libraries.

if false
	% Plot entropy versus bias expected graph
	set(0, 'defaultaxesfontsize', 25)
	b = 0:0.01:1;
	S = zeros(4,length(b));
	for i=1:1:4
		L = 25*i;
		p = b/(3*L);
		q = 1-3*p;
		S(i,:) = -3*p.*log2(p)-q.*log2(q);
	end

	plot(b,S,'-', 'linewidth', 3);
	HLEG=legend('L=25', 'L=50', 'L=75', 'L=100');
	set(HLEG, 'position', [0.2, 0.6, 0.2, 0.3] );
	legend boxoff;
	lchildren = get(HLEG, 'children');
	numData = length(lchildren)/2;
	for i=1:1:numData
		set(lchildren(numData+i), "color", get(lchildren(i),'color') );
	end

	xlabel('Strand bias (b)');
	ylabel('Entropy (S)');
	print('EntropyvsBias.png', '-r300');
end


% for DEB3L/R comparison
len = 60; % library length

entropies = zeros(len,8);
counter = 1;
for i= [ 5:6 9:10 ]
	filename = sprintf("~/DEB3L/sample%d.hist",i);
	load(filename); % load mat file with clan histograms

	% Entropy calculations
	cumHist = sum(hists,3);
	numReads = sum(cumHist,1)(1);
	probability = cumHist / numReads;
	logprob = log2(probability);
	logprob(probability==0) = 0;
	entropies(:,counter) = -sum(probability .* logprob, 1);
	counter++;
end

for i= [ 5:6 9:10 ]
	filename = sprintf("~/DEB3R/sample%d.hist",i);
	load(filename); % load mat file with clan histograms

	% Entropy calculations
	cumHist = sum(hists,3);
	numReads = sum(cumHist,1)(1);
	probability = cumHist / numReads;
	logprob = log2(probability);
	logprob(probability==0) = 0;
	entropies(:,counter) = -sum(probability .* logprob, 1);
	counter++;
end


differenceL = mean(entropies(:,1:2),2)-mean(entropies(:,3:4),2);
std2differenceL = sqrt( std(entropies(:,1:2),0,2).^2 + std(entropies(:,3:4),0,2).^2 ) / sqrt(2);
differenceR = mean(entropies(:,5:6),2)-mean(entropies(:,7:8),2);
std2differenceR = sqrt( std(entropies(:,5:6),0,2).^2 + std(entropies(:,7:8),0,2).^2 ) / sqrt(2);

close all;
set(0, 'defaultaxesfontsize', 20);
h(1)=errorbar(differenceL, std2differenceL, 'mo');
hold on;
h(2)=errorbar(differenceR, std2differenceR, 'k^');
plot(0:len, zeros(len+1,1), 'k-');
set(h, 'markersize', 5, 'markerfacecolor', 'auto');
hold off;

xlim([0 len+1]);
xlabel('Base position');
ylabel('S(tUNC19) - S(hm19)');

HLEG=legend(h, 'DEB3L', 'DEB3R');
legend boxoff;
legend('location', 'northeast');
set(HLEG,'color','none');
lchildren = get(HLEG, 'children');
set(lchildren(3), "color", get(lchildren(1),'color') );
set(lchildren(4), "color", get(lchildren(2),'color') );
set(lchildren(1:2), "markersize", 12);

print('~/DEB3entropy.png', '-r300');


