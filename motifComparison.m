%%%
% Analyses the outliers that are difficult to repair for double barcoded libraries.

more off;
% Load repair efficiency data
allTriplets = {};
tripletEfficiency = zeros(3072,3); % mean, stdev, num of measurements
file = fopen("./pentamer.csv");
fgetl(file);
for i=1:size(tripletEfficiency,1)
	pattern = fscanf(file, "%2*c,%5c,%5c");
	allTriplets{end+1} = [pattern(2:4) "-" pattern(8)];
	tripletEfficiency(i,:) = fscanf(file, "%f,%f,%f,\n");
end
fclose(file);


% Label the mismatches w.r.t. their MM type or context and plot each in a different colour based on the sequence context
set(0, 'defaultaxesfontsize', 22);
MMtypes = {"AA", "AC", "AG", "CC", "CT", "GG", "GT", "TT" };
bins = -0.1:0.05:1.1;

close all;
hold on;
for i=1:8
	hits1 = regexp(allTriplets, sprintf("[ACGT]%s[ACGT]-%s", MMtypes{i}(1), MMtypes{i}(2)) );
	hits2 = regexp(allTriplets, sprintf("[ACGT]%s[ACGT]-%s", MMtypes{i}(2), MMtypes{i}(1)) );
	selection = ( (~cellfun(@isempty,hits1)) | (~cellfun(@isempty,hits2)) )';

	selectedRates = tripletEfficiency(selection & tripletEfficiency(:,2)<=0.1 & tripletEfficiency(:,3)>1,1);
	numSelected = length(selectedRates);
	avg = mean(selectedRates);
	dev = std(selectedRates,1);
	selectedRates(selectedRates<0) = 0;
	
	switch i
		case 1
			h(i) = plot( i*ones(numSelected,1)+0.4*rand(numSelected,1)-0.2 , selectedRates, "ro", "linewidth", 1.5);
		case 2
			h(i) = plot( i*ones(numSelected,1)+0.4*rand(numSelected,1)-0.2 , selectedRates, "b*", "linewidth", 1.5);
		case 3
			h(i) = plot( i*ones(numSelected,1)+0.4*rand(numSelected,1)-0.2 , selectedRates, "gd", "linewidth", 1.5);
		case 4
			h(i) = plot( i*ones(numSelected,1)+0.4*rand(numSelected,1)-0.2 , selectedRates, "mp", "linewidth", 1.5);
		case 5
			h(i) = plot( i*ones(numSelected,1)+0.4*rand(numSelected,1)-0.2 , selectedRates, "^", "Color", [0.9 0.5 0], "linewidth", 1.5);
		case 6
			h(i) = plot( i*ones(numSelected,1)+0.4*rand(numSelected,1)-0.2 , selectedRates, "k>", "linewidth", 1.5);
		case 7
			h(i) = plot( i*ones(numSelected,1)+0.4*rand(numSelected,1)-0.2 , selectedRates, "cv", "linewidth", 1.5);
		case 8
			h(i) = plot( i*ones(numSelected,1)+0.4*rand(numSelected,1)-0.2 , selectedRates, "<", "Color", [0.5 0.5 0.5], "linewidth", 1.5);
	end
	h2(i)=errorbar(i, avg, dev, 'k^');
	
	% Contours for a violin plot
	%counts = hist(selectedRates,bins);
	%counts = 0.45*counts / max(counts);
	%plot(i+counts,bins,"k","linewidth",3);
	%plot(i-counts,bins,"k","linewidth",3);
end

set(h2(6), "color", [0.9 1 0])
set(h2, 'linewidth', 2, "markerfacecolor", "auto");
set(h,"markerfacecolor","auto")	

hold off;
xlabel('Mismatch Type (NM)');
ylabel('Repair efficiency (\eta_s)');
set(gca,'XTick', 1:8)
set(gca,'XTickLabel', MMtypes)
xlim([0 9]);
ylim([0 1.1]);
pause(0.1)
print('~/typesEfficiency.png','-r300');



contexts = {"AA","AC","AG","AT","CA","CC","CG","GA","GC","TA"};
close all;
hold on;
for i=1:10
	base1 = contexts{i}(1);
	base2 = contexts{i}(2);
	hits1 = regexp( allTriplets, sprintf("%s[ACGT]%s-[ACGT]", base1, base2) );
	hits2 = regexp( allTriplets, sprintf("%s[ACGT]%s-[ACGT]", complementary(base2), complementary(base1)) );
	selection = ( (~cellfun(@isempty,hits1)) | (~cellfun(@isempty,hits2)) )';
	
	selectedRates = tripletEfficiency(selection & tripletEfficiency(:,2)<=0.1 & tripletEfficiency(:,3)>1,1);
	numSelected = length(selectedRates);
	avg = mean(selectedRates);
	dev = std(selectedRates,1);
	selectedRates(selectedRates<0) = 0;
	
	plot(i*ones(numSelected,1)+0.4*rand(numSelected,1)-0.2, selectedRates, "o", "linewidth", 1.5, 'color', [0.8 0.4 0.3]);
	h=errorbar(i, avg, dev, 'k^');
	set(h, 'linewidth', 2, "markerfacecolor", "auto");
end
hold off;
xlabel('Adjacent bases (PQ)');
ylabel('Repair efficiency (\eta_s)');
set(gca,'XTick', 1:10)
set(gca,'XTickLabel', contexts)
xlim([0 11]);
ylim([0 1.1]);
pause(0.1)
print('~/contextsEfficiency.png','-r300');



