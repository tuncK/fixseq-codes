// Analysis of the IL libraries after clustering to deduce substitution prevalence histogram
// The groups of reads that belong to the same clan are compared to the fixed/variable strands and counted.
// Tunc Kayikcioglu 2020


#include <iostream>
#include <vector>
#include <fstream>
#include "files.h"
#include "align.h"

using namespace std;


vector <string> readAclan (fstream& inputFile) {
	if (inputFile.eof()==true) {
		vector<string> out;
		return out;
	}
	else {
		vector <string> out;
		string observedSequence;
		getline(inputFile, observedSequence);
		while (observedSequence != "") {
			out.push_back(observedSequence);
			getline(inputFile, observedSequence);
		}
		return out;
	}
}


// CATATGCGGTGTGAAATACCGCA NNNNNANNNNNANNNNNANNNNNANNNNN GAGCTC NNNNNNN TTTATTCTTGTAATACTAGTCATCCTCGTGATGCTGGAAACAAGACCACGAGCAGGCCCGCGGGTT CTCGAGGCAAGCTTGGCGT
void analyseReadsFile (const string filename, const int barcodeLength) {
	// Import the reads file.
	fstream inputFile;
	inputFile.open(filename.c_str());
	if (inputFile.is_open() == false) {
		cout << "Error: File " + filename + " could not be read." << endl;
		throw(0);
	}
	else
		cout << "Importing " << filename << endl;
	
	vector <vector <string> > allClans;
	while (true) {
		vector <string> clan = readAclan (inputFile);
		if (clan.size() == 0) {
			// End of input file was reached.
			break;
		}
		allClans.push_back(clan);
	}
	inputFile.close();
	
	cout << "Starting fixed/variable strand assignments..." << endl;
	string fixedStrand = "TTTATTCTTGTAATACTAGTCATCCTCGTGATGCTGGAAACAAGACCACGAGCAGGCCCGCGGGTT";	
	
	// List all possible insertion scenarios.
	vector <string> insertions;
	for (int i=0; i<fixedStrand.size()-3; i++) {
		string temp = fixedStrand.substr(0,i) + "NN" + fixedStrand.substr(i);
		insertions.push_back( temp.substr(0,fixedStrand.size()) );
	}	
	
	vector <double> sPrevalence (allClans.size());
	
	# pragma omp parallel for schedule (dynamic, 100)
	for (int clanID=0; clanID<allClans.size(); clanID++) {
		vector <string> clan = allClans.at(clanID);
		
		// Check each member of the clan to check if it is shifted.
		int Fcount = 0;
		int Vcount = 0;
		vector <int> vIDlist;
		for (int i=0; i<clan.size(); i++) {
			string read = clan[i].substr(barcodeLength, fixedStrand.size()); // Ignore mapping barcode
			int Fdist = HammingDistance(read, fixedStrand); // fixed strand prototype
			
			// Find insertion position by brute force, 
			// since no napping barcode info available.
			int Vdist = 10000;
			int vindex = -1;
			for (int j=0; j<insertions.size(); j++) {
				int tempdist = HammingDistance( read, insertions.at(j) );
				if (tempdist < Vdist) {
					Vdist = tempdist;
					vindex = j;
				}
			}
				
			if (Fdist>Vdist) {
				Vcount++;
				vIDlist.push_back(vindex);
			}
			else
				Fcount++;
		}
		
		sPrevalence.at(clanID) = Vcount / (double) (Fcount + Vcount);
	}
	
	// Output the s's of all clans
	int parpos = filename.find_last_of(".")+1;
	string path = filename.substr(0,parpos);
	string outfilename =  path.substr(0,path.size()-1) + ".csv";
	ofstream file;
	file.open(outfilename.c_str());
	for (int i=0; i<sPrevalence.size(); i++)
		file << sPrevalence.at(i) << endl;
	file.close();	
	
	return;
}



int main (int argc, const char** argv) {

	for (int fileID=5; fileID<=12; fileID++) {
		string filename = "~/dil/" + to_string(fileID) + ".reads";
		analyseReadsFile (filename, 8);
	}
	
	for (int fileID=11; fileID<=18; fileID+=7) {
		string filename = "~/dil/3cl1/" + to_string(fileID) + ".reads";
		analyseReadsFile (filename, 7);
	}
	
	return 0;
}


