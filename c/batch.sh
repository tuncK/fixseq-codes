#!/bin/bash


echo "Started submitting  jobs..."
for fileID in 5 6 7 8 11 12 13 14 15 16 17 18
do
	filename=~/scratch/data/sample$fileID.csf
	echo $filename
	sbatch ~/submit.sh
done

echo "Finished job submissions."


