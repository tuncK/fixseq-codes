// Analysis of the SIL/DIL libraries after clustering.
// The groups of reads that belong to the same clan are compared to the fixed/variable strands and counted.
// Tunc Kayikcioglu 2020


#include <iostream>
#include <vector>
#include <fstream>
#include "files.h"
#include "align.h"

using namespace std;


// CGCTCCTTGTTGTACTCGCAGAGCTC TACTAGT TTTATTCTTGTAATACTAGTCATCCTCGTGATGCTGGAAACAAGACCACGAGCAGGCCCGCGGGTT CTCGAG ATACCGGCGGTTGTTCAACC
// TGACGGAGGATAGAAGGCCAGAGCTC ATTTCCCG TTTATTCTTGTAATACTAGTCATCCTCGTGATGCTGGAAACAAGACCACGAGCAGGCCCGCGGGTT CTCGAG ATACCGGCGGTTGTTCAACC
vector <vector <string> > importBarcodeList (const string filename) {
	fstream inputFile;
	inputFile.open(filename.c_str());
	if (inputFile.is_open() == false) {
		cout << "Error: File " + filename + " could not be read." << endl;
		throw(0);
	}
	
	map<string,string> SILmap;
	map<string,string> DILmap;
	
	vector < vector <string> > out;
	vector <string> barcodes;
	for (int linecounter=0; linecounter<3300; linecounter++) {
		string line;
		getline(inputFile, line);
		string barcode = line.substr(26,7);
		barcodes.push_back(barcode);
	}
	out.push_back(barcodes);
	barcodes.clear();
	
	for (int linecounter=3300; linecounter<11220; linecounter++) {
		string line;
		getline(inputFile, line);
		string barcode = line.substr(26,8);
		barcodes.push_back(barcode);
	}
	out.push_back(barcodes);
	inputFile.close();
	return out;
}


void analyseReadsFile (const string filename, const bool is_SIL, const vector <string>& barcodeList) {
	const int maxDistLimit = 2;

	// Import the reads file.
	fstream inputFile;
	inputFile.open(filename.c_str());
	if (inputFile.is_open() == false) {
		cout << "Error: File " + filename + " could not be read." << endl;
		throw(0);
	}
	else
		cout << "Importing " << filename << endl;
	
	vector <vector <string> > allClans;
	while (true) {
		vector <string> clan = readAclan (inputFile);
		if (clan.size() == 0) {
			// End of input file was reached.
			break;
		}
		allClans.push_back(clan);
	}
	inputFile.close();
	
	int mappingBarcodeLength;
	if (is_SIL) {
		mappingBarcodeLength = 7;
	}
	else {
		mappingBarcodeLength = 8;
	}
	
	cout << "Started counting mapping barcodes..." << endl;
	vector <int> barcodeHitCounts (barcodeList.size()+1);
	
	# pragma omp parallel for schedule (dynamic, 100)
	for (int clanID=0; clanID<allClans.size(); clanID++) {
		vector <string> clan = allClans.at(clanID);
		
		// Calculate the average mapping barcode
		vector <vector <int> > mappingBarcode (mappingBarcodeLength);
		for (int i=0; i<mappingBarcodeLength; i++)
			mappingBarcode[i].resize(4);
		
		for (int i=0; i<clan.size(); i++) {
			string barcode = clan[i].substr(0,mappingBarcodeLength);
			for (int j=0; j<mappingBarcodeLength; j++) {
				mappingBarcode[j][ base2intMap[barcode[j]] ]++;
			}
		}
		
		// Look-up the barcode maps
		string ensembleBarcode = histogram2DNA(mappingBarcode);
		vector<string>::const_iterator it = find(barcodeList.begin(), barcodeList.end(), ensembleBarcode);
		if (it != barcodeList.end()) {
			int pos = it-barcodeList.begin();
			#pragma omp critical
			barcodeHitCounts.at(pos)++;
		}
		else {
			// Mapping barcode not found in the lookup table, skip this clan.
			#pragma omp critical
			barcodeHitCounts.back()++;
		}
	}
	
	// Output the number of events per mapping barcode
	int parpos = filename.find_last_of(".")+1;
	string path = filename.substr(0,parpos);
	exportList(barcodeHitCounts, path.substr(0,path.size()-1) + ".csv");
	return;
}



int main (int argc, const char** argv) {
	base2intMap['A'] = 0;
	base2intMap['a'] = 0;
	base2intMap['T'] = 1;
	base2intMap['t'] = 1;
	base2intMap['C'] = 2;
	base2intMap['c'] = 2;
	base2intMap['G'] = 3;
	base2intMap['g'] = 3;

	int2baseMap[0] = 'A';
	int2baseMap[1] = 'T';
	int2baseMap[2] = 'C';
	int2baseMap[3] = 'G';
	
	
	// Import the 2nd barcode mapping from file
	cout << "Importing maps..." << endl;
	vector < vector<string> > barcodeLists = importBarcodeList ("~/fixseq-codes/processHists/SIL-DIL-sequencesOrdered.txt");
	
	for (int fileID=1; fileID<3; fileID++) {
		string filename = "~/" + to_string(fileID) + ".reads";
		analyseReadsFile (filename, true, barcodeLists[0]);
	}
	
	/*
	for (int fileID=5; fileID<13; fileID++) {
		string filename = "~/dil/" + to_string(fileID) + ".reads";
		analyseReadsFile (filename, false, barcodeLists[1]);
	}
	*/
	
	return 0;
}



