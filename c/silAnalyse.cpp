// Analysis of the SIL/DIL libraries after clustering.
// The groups of reads that belong to the same clan are compared to the fixed/variable strands and counted.
// Tunc Kayikcioglu 2020


#include <iostream>
#include <vector>
#include <fstream>
#include "files.h"
#include "align.h"

using namespace std;


const string fixedStrand = "TTTATTCTTGTAATACTAGTCATCCTCGTGATGCTGGAAACAAGACCACGAGCAGGCCCGCGGGTTCTCGAG";

// CGCTCCTTGTTGTACTCGCAGAGCTC TACTAGT TTTATTCTTGTAATACTAGTCATCCTCGTGATGCTGGAAACAAGACCACGAGCAGGCCCGCGGGTT CTCGAG ATACCGGCGGTTGTTCAACC
// TGACGGAGGATAGAAGGCCAGAGCTC ATTTCCCG TTTATTCTTGTAATACTAGTCATCCTCGTGATGCTGGAAACAAGACCACGAGCAGGCCCGCGGGTT CTCGAG ATACCGGCGGTTGTTCAACC
vector < map<string,string> > importLibrary (const string filename) {
	fstream inputFile;
	inputFile.open(filename.c_str());
	if (inputFile.is_open() == false) {
		cout << "Error: File " + filename + " could not be read." << endl;
		throw(0);
	}
	
	map<string,string> SILmap;
	map<string,string> DILmap;
	
	for (int linecounter=0; linecounter<3300; linecounter++) {
		string line;
		getline(inputFile, line);
		string barcode = line.substr(26,7);
		string deb3 = line.substr(33,fixedStrand.size());
		SILmap[barcode] = deb3;
	}
	
	for (int linecounter=3300; linecounter<11220; linecounter++) {
		string line;
		getline(inputFile, line);
		string barcode = line.substr(26,8);
		string deb3 = line.substr(34,fixedStrand.size());
		DILmap[barcode] = deb3;
	}
	inputFile.close();
	
	vector < map<string,string> > out (2);
	out[0] = SILmap;
	out[1] = DILmap;
	return out;
}


// Generate a list of all expected sequences w.r.t repair matrix element
map <string,int> assignMMids (bool is_SIL) {
	int numBaseIDs,mappingBarcodeLength;
	string outfilename;
	if (is_SIL) {
		numBaseIDs = 4;
		mappingBarcodeLength = 7;
		outfilename = "/home/tk/silMMIDmatrix.csv";
	}
	else {
		numBaseIDs = 16;
		mappingBarcodeLength = 8;
		outfilename = "/home/tk/dilMMIDmatrix.csv";
	}
	
	map <string,int> variableStrandMap;
	ofstream matrixOut;
	matrixOut.open(outfilename);

	int mmID = 1;
	int len = fixedStrand.size();
	for (int i=1; i<len; i++) {
		for (int baseID=0; baseID<numBaseIDs; baseID++) {
			string variableStrand;
			if (is_SIL) {
				char base = int2baseMap[baseID];
				variableStrand = fixedStrand.substr(0,i) + base + fixedStrand.substr(i);
			}	
			else {
				char base1 = int2baseMap[baseID/4];
				char base2 = int2baseMap[baseID%4];
				variableStrand = fixedStrand.substr(0,i) + base1 + base2 + fixedStrand.substr(i);
			}
				
			variableStrand = variableStrand.substr(0,fixedStrand.size());
			if ( variableStrandMap.find(variableStrand)==variableStrandMap.end() ) {
				// Element encountered for the first time, insert it.
				//if (i>len-7)
				//	mmID = 0;
				
				variableStrandMap[variableStrand] = mmID;
				mmID++;
			}
			
			matrixOut << i << "\t" << baseID << "\t" << variableStrandMap.find(variableStrand)->second << endl;
		}
	}
	
	matrixOut.close();
	return variableStrandMap;
}


void analyseReadsFile (const string filename, bool is_SIL, const map <string,string>& barcodeMap, const map <string,int>& strand2matrixMap) {
	const int maxDistLimit = 2;

	// Import the reads file.
	fstream inputFile;
	inputFile.open(filename.c_str());
	if (inputFile.is_open() == false) {
		cout << "Error: File " + filename + " could not be read." << endl;
		throw(0);
	}
	else
		cout << "Importing " << filename << endl;
	
	vector <vector <string> > allClans;
	while (true) {
		vector <string> clan = readAclan (inputFile);
		if (clan.size() == 0) {
			// End of input file was reached.
			break;
		}
		allClans.push_back(clan);
	}
	inputFile.close();
	
	int mappingBarcodeLength;
	if (is_SIL) {
		mappingBarcodeLength = 7;
	}
	else {
		mappingBarcodeLength = 8;
	}
	
	cout << "Starting fixed/variable assignments..." << endl;
	vector <vector <int> > FVcounts (allClans.size()); // 0: #F, 1: #V, 2:MM_ID
	
	# pragma omp parallel for schedule (dynamic, 100)
	for (int clanID=0; clanID<allClans.size(); clanID++) {
		vector <string> clan = allClans.at(clanID);
		
		// Calculate the average mapping barcode
		vector <vector <int> > mappingBarcode (mappingBarcodeLength);
		for (int i=0; i<mappingBarcodeLength; i++)
			mappingBarcode[i].resize(4);
		
		for (int i=0; i<clan.size(); i++) {
			string barcode = clan[i].substr(0,mappingBarcodeLength);
			for (int j=0; j<mappingBarcodeLength; j++) {
				mappingBarcode[j][ base2intMap[barcode[j]] ]++;
			}
		}
		
		// Look-up the barcode maps
		string ensembleBarcode = histogram2DNA(mappingBarcode);
		if (barcodeMap.find(ensembleBarcode) == barcodeMap.end()) {
			// Mapping barcode not found in the lookup table, skip this clan.
			continue;
		}
		
		string variableStrand = barcodeMap.find(ensembleBarcode)->second.substr(0,fixedStrand.size());
		int mmID = strand2matrixMap.find(variableStrand)->second;
		
		// Check each member of the clan to decide if it is shifted.
		int Fcount = 0;
		int Vcount = 0;
		for (int i=0; i<clan.size(); i++) {
			string read = clan[i].substr(mappingBarcodeLength, fixedStrand.size());
			
			int Fdist = HammingDistance(read, fixedStrand); // fixed strand prototype
			int Vdist = HammingDistance(read, variableStrand); // expected variable strand based on barcode
			
			if (Vdist<Fdist && Vdist<maxDistLimit )
				Vcount++;
			
			if (Fdist<Vdist && Fdist<maxDistLimit )
				Fcount++;
		}
		
	//	if (Fcount + Vcount >= 10) {
			vector <int> temp (3);
			temp[0] = Fcount;
			temp[1] = Vcount;
			temp[2] = mmID;
			FVcounts[clanID] = temp;
	//	}
	}
	
	// Eliminate skipped clans from the list
	for (int i=0; i<FVcounts.size(); i++) {
		if (FVcounts[i].size() == 0) {
			FVcounts[i] = FVcounts.back();
			FVcounts.pop_back();
			i--;
		}
	}
	
	// Output the F V counts of all clans
	int parpos = filename.find_last_of(".")+1;
	string path = filename.substr(0,parpos);
	exportCSV(FVcounts, path.substr(0,path.size()-1) + ".csv");
	return;
}



int main (int argc, const char** argv) {
	base2intMap['A'] = 0;
	base2intMap['a'] = 0;
	base2intMap['T'] = 1;
	base2intMap['t'] = 1;
	base2intMap['C'] = 2;
	base2intMap['c'] = 2;
	base2intMap['G'] = 3;
	base2intMap['g'] = 3;

	int2baseMap[0] = 'A';
	int2baseMap[1] = 'T';
	int2baseMap[2] = 'C';
	int2baseMap[3] = 'G';
	
	
	// Import the 2nd barcode mapping from file
	cout << "Importing maps..." << endl;
	vector < map<string,string> > maps = importLibrary ("~/fixseq-codes/processHists/SIL-DIL-sequencesOrdered.txt");
	map <string,int> silVarStrandMap = assignMMids (true);
	map <string,int> dilVarStrandMap = assignMMids (false);
	
	for (int fileID=1; fileID<5; fileID++) {
		string filename = "/home/tk/sil/" + to_string(fileID) + ".reads";
		analyseReadsFile (filename, true, maps[0], silVarStrandMap);
	}
		
	for (int fileID=5; fileID<13; fileID++) {
		string filename = "/home/tk/dil/" + to_string(fileID) + ".reads";
		analyseReadsFile (filename, false, maps[1], dilVarStrandMap);
	}
	
	return 0;
}


