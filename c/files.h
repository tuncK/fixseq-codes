#include <iostream>
#include <vector>
#include <fstream>
#include <map>
#include <algorithm>

using namespace std;
map<char,int> base2intMap;
map<int,char> int2baseMap;


vector <string> readAclan (fstream& inputFile) {
	if (inputFile.eof()==true) {
		vector<string> out;
		return out;
	}
	else {
		vector <string> out;
		string observedSequence;
		getline(inputFile, observedSequence);
		while (observedSequence != "") {
			out.push_back(observedSequence);
			getline(inputFile, observedSequence);
		}
		return out;
	}
}


vector <vector <string> > importClusters (string filename) {
	vector <vector <string> > out(1);
	fstream inputFile;
	inputFile.open(filename.c_str());
	
	if (inputFile.is_open() == false) {
		cout << "File " << filename << " could not be read." << endl;
		throw(1);
	}
	
	while (true) {
		string sequence;
		getline(inputFile, sequence);
		
		if (inputFile.eof()==true)
			break;
		
		if (sequence.size() == 0) {
			vector <string> temp;
			out.push_back(temp);
		}
		else {
			out.back().push_back(sequence);
		}
	}
	
	inputFile.close();
	out.pop_back();
	return out;
}


// Generates a matrix of base abundance histogram
vector <vector <int> > baseHistogram (const vector <string>& in, int readlen = -1) {	
	if (readlen==-1)
		readlen = in.at(0).size();
	
	vector <vector <int> > freq (readlen);
	for (int i=0; i<readlen; i++)
		freq.at(i).resize(4);
	
	for (int j=0; j<in.size(); j++) {
		int lengthLim = min(readlen,(int)in.at(j).size());
		for (int i=0; i<lengthLim; i++) {
			char base = in[j][i];
			freq[i][ base2intMap.find(base)->second ]++;
		}
	}
	
	return freq;
}


// Find the dominant DNA that would produce the given base frequency histogram.
template <typename T>
string histogram2DNA (const vector <vector <T> >& histogram) {
	int readlen = histogram.size();
	string dna;
	
	for (int i=0; i<readlen; i++) {
		char base = 'A';
		T maxfreq = histogram[i][0];
		
		if (histogram.at(i).at(1) > maxfreq ) {
			base = 'T';
			maxfreq = histogram[i][1];
		}

		if (histogram.at(i).at(2) > maxfreq ) {
			base = 'C';
			maxfreq = histogram[i][2];
		}
				
		if (histogram.at(i).at(3) > maxfreq ) {
			base = 'G';
			// maxfreq = histogram[i][3];
		}

		dna.push_back( base );
	}
	
	return dna;
}


template <typename T>
void exportHist_as_MatFile (const vector <vector <vector <T> > >& matrices, const string filename) {
	int numClans = matrices.size();
	if (numClans == 0)
		return;
	
	int numRows = matrices.at(0).size();
	int numColumns = matrices.at(0).at(0).size();

	ofstream out;
	out.open( filename.c_str() );
	out << "# Created by FixSeq Analysis" << endl;
	out << "# name: hists" << endl;
	out << "# type: float matrix" << endl;
	out << "# ndims: 3" << endl;
	out << " " << numColumns << " " << numRows << " " << numClans << endl;	
	
	for (int i=0; i<numClans; i++) {
		for (int k=0; k<numRows; k++) {
			for (int j=0; j<numColumns; j++) {
				out << matrices.at(i).at(k).at(j) << endl;
			}
		}
	}
	
	out << endl;
	out << endl;
	out.close();
	return;
}



template <typename T>
void exportMatrix (const vector <vector <vector <T> > >& matrices, const string filename) {
	int numMatrices = matrices.size();
	if (numMatrices == 0)
		return;
	
	
	int n = matrices.at(0).size();
	int m = matrices.at(0).at(0).size();

	ofstream out;
	out.open( filename.c_str() );
	for (int i=0; i<numMatrices; i++) {
		for (int j=0; j<m; j++) {
			for (int k=0; k<n; k++) {
				out << matrices.at(i).at(k).at(j) << "\t";
			}
			out << endl;
		}
		out << endl;
	}
	
	out.close();
	return;
}


template <typename T>
void exportMatrix (const vector <vector <T> >& matrix, const string filename) {
	vector <vector <vector <T> > > temp;
	temp.push_back(matrix);
	return exportMatrix(temp,filename);	
}


template <typename T>
void exportCSV (const vector <vector <T> >& matrices, const string filename) {
	if (matrices.size() == 0) {
		cout << "File " << filename << " was not generated as the input matrix is empty." << endl;
		return;
	}
	
	int n = matrices.size();
	int m = matrices.at(0).size();

	ofstream out;
	out.open( filename.c_str() );
	for (int j=0; j<n; j++) {
		for (int k=0; k<m; k++) {
			out << matrices.at(j).at(k) << "\t";
			}
		out << endl;
	}
	out.close();
	return;
}


template <typename T>
void exportList (const vector <T>& list, const string filename) {
	ofstream out;
	out.open( filename.c_str() );
	for (int i=0; i<list.size(); i++) {
		out << list.at(i) << endl;
	}
	out.close();
	return;
}


template <typename T1, typename T2>
void exportPairedList (const vector <pair <T1,T2> >& list, const string filename) {
	int len = list.size();
	ofstream out;
	out.open( filename.c_str() );
	
	for (int i=0; i<len; i++) {
		out << list.at(i).first << "\t" << list.at(i).second << endl;
	}
	
	out.close();
	return;
}


vector <string> importConsensusSequence (string filename) {
	vector <vector <string> > allClans = importClusters(filename);
	vector <string> consensusSequences (allClans.size());

	cout << filename << endl;

	# pragma omp parallel for schedule (dynamic, 1000)
	for (int i=0; i<allClans.size(); i++) {
		vector <string> clan = allClans.at(i);
		vector <vector <int> > histogram = baseHistogram(clan);
		consensusSequences.at(i) = histogram2DNA(histogram);
	}
	
	return consensusSequences;
}


vector <string> importBarcodes (string filename) {
	vector <string> out;
	
	fstream inputFile;
	inputFile.open(filename.c_str());
	while (true) {
		string trash;
		getline(inputFile, trash); // Discard raw data
		if (inputFile.eof()==true)
			break;
		
		string sequence;
		getline(inputFile, trash); // Discard data row
		getline(inputFile, sequence);
		getline(inputFile, trash);
		getline(inputFile, trash);
		
		out.push_back(sequence);
	}
	
	inputFile.close();
	return out;
}



void exportClusters (vector <vector <string> > list, string filename) {
	ofstream outFile;
	outFile.open(filename.c_str());
	
	for (int i=0; i<list.size(); i++) {
		for (int j=0; j<list.at(i).size(); j++)
			outFile << list.at(i).at(j) << endl;
		outFile << endl;
	}
	
	outFile.close();
	return;
}



