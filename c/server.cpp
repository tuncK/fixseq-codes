// Analysis of very large data sets that require high memory.
// Tunc Kayikcioglu 2020


#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include "dbscan.h"
#include "fastq.h"
#include "design.h"
#include "files.h"
#include "os.h"
#include "align.h"

using namespace std;


int analyseGroup (const vector <pair <string,string> >& input, ofstream& ofile, const int indexOffset=0) {
	const int numReads = input.size();
	vector < vector<string> > out; // first: barcodeFWD, second: variable segment FWD, third: barcode REV, fourth: variable segment REV
	out.reserve(numReads);

	const int adaptorMatchThreshold = 0.6*leftAdaptorLength; // At least this much of the adaptor should match.
	const int minReadLength = barcodeLength + restrictionOffset + variableSegLength + leftAdaptorLength; // All reads should at least have a length enough for the read and half an adaptor on both sides.
	const int shiftThreshold = 5; // Discrepancy exceeding this will trigger an attempt to detect possible shifts of the sequence of interest
	const string variableSegmentComplementary = complementary(variableSegment);
	const string rightAdaptorComplementary = complementary(rightAdaptor);
	
	int ignoredReadCount = 0;
	
	# pragma omp parallel for schedule (dynamic, 1000)
	for (int i=0; i<numReads; i++) {
		string sequenceFwd = trimUnknownBases( input.at(i).first );
		string sequenceRev = trimUnknownBases( input.at(i).second );
		
		if (sequenceFwd.size()<minReadLength || sequenceRev.size()<minReadLength) {
			// Either the forward or the reverse read is too short to be meaningful.
			ignoredReadCount++;
			continue;
		}
		
		pair <int,int> Wscore = semiglobalAlignment(sequenceFwd, leftAdaptor);
		int adaptorEndPosFwd = Wscore.first;
		int scoreFwd = Wscore.second;
		
		pair <int,int> Cscore = semiglobalAlignment(sequenceRev, rightAdaptorComplementary);
		int adaptorEndPosRev = Cscore.first;
		int scoreRev = Cscore.second;
		
		if (scoreFwd < adaptorMatchThreshold || adaptorEndPosFwd > 30 || adaptorEndPosFwd < 15 ||
				 scoreRev < adaptorMatchThreshold || adaptorEndPosRev > 30 || adaptorEndPosRev < 15) { 
				 // Adaptor not read accurately enough or misaligned too much
			ignoredReadCount++;
			continue;
		}
		
		// Extract the barcode and the library segments out of the whole read.
		int variableSegmentStartPosFwd = adaptorEndPosFwd + barcodeLength + restrictionOffset;
		string barcodeFwd = sequenceFwd.substr(adaptorEndPosFwd, barcodeLength);
		string detectedVariableSegmentFwd = sequenceFwd.substr(variableSegmentStartPosFwd, variableSegLength);
		
		int variableSegmentStartPosRev = adaptorEndPosRev;
		string detectedVariableSegmentRev = sequenceRev.substr(variableSegmentStartPosRev, variableSegLength);
		int barcodeStartPosRev = adaptorEndPosRev + variableSegLength + restrictionOffset;
		string barcodeRev = sequenceRev.substr(barcodeStartPosRev, barcodeLength);

		// Optional scan for shifts of variable segment
		// If a significant deviation is detected, +/- 1 base shifts are tried and saved if a good match.
		// SHOULD BE DISABLED FOR INSERTION/DELETION EXPERIMENTS!
		if (true) {
			if (HammingDistance(detectedVariableSegmentFwd,variableSegment) > shiftThreshold) {			
				pair<int,int> variableHitParams = biasedAlignment(sequenceFwd,variableSegment,variableSegmentStartPosFwd);
		
				if (variableHitParams.second>=0)
					detectedVariableSegmentFwd = sequenceFwd.substr(variableHitParams.second,variableSegLength);
			}
		
			if (HammingDistance(detectedVariableSegmentRev,variableSegmentComplementary)>shiftThreshold) {		
				pair<int,int> variableHitParams = biasedAlignment(sequenceRev,variableSegmentComplementary,variableSegmentStartPosRev);
				
				if (variableHitParams.second>=0)
					detectedVariableSegmentRev = sequenceRev.substr(variableHitParams.second,variableSegLength);
			}
			
			// Observed variable sequence significantly deviates from expectations
			if (HammingDistance(detectedVariableSegmentFwd,variableSegment)>shiftThreshold) {
				barcodeFwd = "";
				detectedVariableSegmentFwd = "";
				ignoredReadCount++;
			}
			if ( HammingDistance(detectedVariableSegmentRev,variableSegmentComplementary)>shiftThreshold) {
				barcodeRev = "";
				detectedVariableSegmentRev = "";
				ignoredReadCount++;							
			}
		}
		
		vector <string> temp (4);
		temp.at(0) = barcodeFwd;
		temp.at(1) = detectedVariableSegmentFwd;
		temp.at(2) = complementary(barcodeRev);
		temp.at(3) = complementary(detectedVariableSegmentRev);
		
		#pragma omp critical
		out.push_back(temp);
	}
	
	// Output to the provided file
	for (int i=0; i<out.size(); i++) {
		if (out.at(i).at(0) != "") {
			ofile << "#F " << i+1+indexOffset << endl;
			ofile << out.at(i).at(0) << endl;
			ofile << out.at(i).at(1) << endl;
		}
		if (out.at(i).at(2) != "") {
			ofile << "#R " << i+1+indexOffset << endl;
			ofile << out.at(i).at(2) << endl;
			ofile << out.at(i).at(3) << endl;
		}
	}
	
	return ignoredReadCount;
}



int analyseGroup_SIL_DIL (const vector <pair <string,string> >& input, ofstream& ofile, const int indexOffset=0) {
	const int numReads = input.size();
	vector < vector<string> > out; // first: barcodeFWD, second: variable segment FWD, third: barcode REV, fourth: variable segment REV
	out.reserve(numReads);

	const int adaptorMatchThreshold = 0.6*leftAdaptorLength; // At least this much of the adaptor should match.
	const int minReadLength = barcodeLength + restrictionOffset + variableSegLength + leftAdaptorLength; // All reads should at least have a length enough for the read and half an adaptor on both sides.
	const int shiftThreshold = 5; // Discrepancy exceeding this will trigger an attempt to detect possible shifts of the sequence of interest
	
	int ignoredReadCount = 0;
	
	# pragma omp parallel for schedule (dynamic, 1000)
	for (int i=0; i<numReads; i++) {		
		string sequenceFwd = input.at(i).first;
		string sequenceRev = input.at(i).second;
		
		if (sequenceFwd.size()<minReadLength || sequenceRev.size()<minReadLength) {
			// Either the forward or the reverse read is too short to be meaningful.
			ignoredReadCount++;
			continue;
		}
		
		pair <int,int> Wscore = semiglobalAlignment(sequenceFwd, leftAdaptor);
		int adaptorEndPosFwd = Wscore.first;
		int scoreFwd = Wscore.second;
		
		sequenceRev = complementary(sequenceRev);
		pair <int,int> Cscore = semiglobalAlignment(sequenceRev, leftAdaptor);
		int adaptorEndPosRev = Cscore.first;
		int scoreRev = Cscore.second;
		
		if (scoreFwd < adaptorMatchThreshold || adaptorEndPosFwd > 30 || adaptorEndPosFwd < 15 ||
				 scoreRev < adaptorMatchThreshold || adaptorEndPosRev > 30 || adaptorEndPosRev < 15) { 
				 // Adaptor not read accurately enough or misaligned too much
			ignoredReadCount++;
			continue;
		}
		
		// Extract the barcode and the library segments out of the whole read.
		pair<int,int> variableHitParamsFwd = biasedAlignment(sequenceFwd,"GAGCTC",adaptorEndPosFwd + barcodeLength);
		string detectedVariableSegmentFwd = sequenceFwd.substr(variableHitParamsFwd.second+restrictionOffset,variableSegLength);
		string barcodeFwd = sequenceFwd.substr(variableHitParamsFwd.second-barcodeLength, barcodeLength);
		
		pair<int,int> variableHitParamsRev = biasedAlignment(sequenceRev,"GAGCTC",adaptorEndPosRev + barcodeLength);
		string detectedVariableSegmentRev = sequenceRev.substr(variableHitParamsRev.second+restrictionOffset,variableSegLength);
		string barcodeRev = sequenceRev.substr(variableHitParamsRev.second-barcodeLength, barcodeLength);
		
		// Observed variable sequence significantly deviates from expectations
		if (editDistance(detectedVariableSegmentFwd,variableSegment)>shiftThreshold) {
			barcodeFwd = "";
			detectedVariableSegmentFwd = "";
			ignoredReadCount++;
		}
		if (editDistance(detectedVariableSegmentRev,variableSegment)>shiftThreshold) {
			barcodeRev = "";
			detectedVariableSegmentRev = "";
			ignoredReadCount++;							
		}
		
		vector <string> temp (4);
		temp.at(0) = barcodeFwd;
		temp.at(1) = detectedVariableSegmentFwd;
		temp.at(2) = barcodeRev;
		temp.at(3) = detectedVariableSegmentRev;
		
		#pragma omp critical
		out.push_back(temp);
	}
	
	// Output to the provided file
	for (int i=0; i<out.size(); i++) {
		if (out.at(i).at(0) != "") {
			ofile << "#F " << i+1+indexOffset << endl;
			ofile << out.at(i).at(0) << endl;
			ofile << out.at(i).at(1) << endl;
		}
		if (out.at(i).at(2) != "") {
			ofile << "#R " << i+1+indexOffset << endl;
			ofile << out.at(i).at(2) << endl;
			ofile << out.at(i).at(3) << endl;
		}
	}
	
	return ignoredReadCount;
}



pair<string, string> getAreadFromFASTQ (fstream& inputFile) {
	string trash;
	getline(inputFile, trash); // Discard info row starting with @
	if (inputFile.eof()==true) {
		pair<string, string> out ("", "");
		return out;
	}
	else {
		string sequence, qscore;
		getline(inputFile, sequence);
		getline(inputFile, trash); // discard '+' row
		getline(inputFile, qscore);
		
		if (qscore.size() != sequence.size()) {
			cout << sequence << endl;
			cout << qscore << endl;
			cout << "ERROR: Invalid FASTQ file format." << endl;
			throw(1);
		}
		pair<string, string> out (sequence, qscore);
		return out;
	}
}


pair<string, string> getAreadFromCSF (fstream& inputFile) {
	string trash;
	getline(inputFile, trash); // Discard info row starting with #
	if (inputFile.eof()==true) {
		pair<string, string> out ("", "");
		return out;
	}
	else {
		string barcode, variableSequence;
		getline(inputFile, barcode);
		getline(inputFile, variableSequence);
		
		if (barcode.size()!=barcodeLength || variableSequence.size()!=variableSegLength ) {
			cout << "ERROR: Invalid CSF file format." << endl;
			throw(1);
		}
		pair<string, string> out (barcode, variableSequence);
		return out;
	}
}



void compressFile (const string filenameFwd, const string filenameRev) {
	// Import the file.
	// Input fastq file is assumed to be too big for the available RAM, so
	// So it will be processed in chunks to avoid running an out of memory.
	fstream inputFileFwd, inputFileRev;
	inputFileFwd.open(filenameFwd.c_str());
	if (inputFileFwd.is_open() == false )
		throw("Error: File " + filenameFwd + " could not be read.\n");
	
	inputFileRev.open(filenameRev.c_str());
	if (inputFileRev.is_open() == false )
		throw("Error: File " + filenameRev + " could not be read.\n");

	int parpos = filenameFwd.find_last_of(".")+1;
	string path = filenameFwd.substr(0,parpos);	
	ofstream outputFile;
	outputFile.open( (path.substr(0,path.size()-4) + ".csf").c_str());
	
	// Import reads 4-line blocks at a time
	int ignoredCounter = 0;
	int totalCounter = 0;
	
	int MAX_CHUNK_SIZE = 2e6;
	vector <pair <string,string> > tempReads;
	tempReads.reserve (MAX_CHUNK_SIZE);
	
	bool eof = false;
	while (!eof) {
		// Import reads 4-line blocks at a time
		pair<string,string> fwdRead = getAreadFromFASTQ(inputFileFwd);
		pair<string,string> revRead = getAreadFromFASTQ(inputFileRev);
		
		if (fwdRead.first.size()==0 || revRead.first.size()==0)
			eof = true;
		else {
			pair<string,string> temp (fwdRead.first,revRead.first);
			tempReads.push_back(temp);
		}
				
		// If enough number of reads available in the ubuffer, analyse them.
		if (tempReads.size() == MAX_CHUNK_SIZE || eof ) {
			ignoredCounter += analyseGroup(tempReads, outputFile, totalCounter+1);
			//ignoredCounter += analyseGroup_SIL_DIL(tempReads, outputFile, totalCounter+1);
			totalCounter += tempReads.size();
			tempReads.clear();
		}
	}
	
	inputFileFwd.close();
	inputFileRev.close();
	outputFile.close();
	
	cout << ignoredCounter << " out of " << 2*totalCounter << " reads were discarded due to low quality scores." << endl;
	return;
}


void analyseCompressedFile (const string filename) {
	// Import the file.
	// Only barcodes are saved at the first pass to conserve memory.
	fstream inputFile;
	inputFile.open(filename.c_str());
	if (inputFile.is_open() == false)
		throw("Error: File " + filename + " could not be read.\n");
	
	vector <string> barcodes;
	while (true) {
		pair<string, string> read = getAreadFromCSF (inputFile);
		if (read.first.size() == 0) {
			break;
		}
		else {
			barcodes.push_back(read.first);
		}
	}
	inputFile.close();
	
	int numBarcodestoProcess = barcodes.size();
	cout << numBarcodestoProcess << " barcodes are of correct length." << endl;
	
	// Find clusters using DBscan
	cout << "Starting clustering with DBSCAN..." << endl;

	// pair<vector <int>,int> findClusters (inputlist, minPts, searchRadius, distanceMetric)
	// Choice of metric is of importance, dynamic programing accounting for the shifts might be necessary.
	//vector <int> classLabels = findClusters (barcodes, 10, 3, NeedlemanWunsch);
	vector <int> classLabels = findClusters (barcodes, 10, 3, HammingDistance);
	int numClasses = classLabels.back();
	classLabels.pop_back();
	cout << "Number of barcodes: " << barcodes.size() << endl;
	cout << "Number of clusters: " << numClasses << endl;	
	
	map<string,int> barcode2intMap;
	for (int i=0; i<classLabels.size(); i++) {
		int label = classLabels.at(i);
		string barcode = barcodes.at(i);
		barcode2intMap [barcode] = label;
	}
	barcodes.clear();
	barcodes.reserve(0);
	
	// Calculate the histogram within each cluster and export to file
	// Re-iterate through the input file.
	
	// Initialise the tensor to store the data
	vector< vector< vector <int> > > allHistograms (numClasses);
	for (int i=0; i<numClasses; i++) {
		allHistograms.at(i).resize(variableSegLength);
		for (int j=0; j<variableSegLength; j++)
			allHistograms.at(i).at(j).resize(4);
	}
	
	inputFile.open(filename.c_str());
	if (inputFile.is_open() == false)
		throw("Error: File " + filename + " could not be read for a second pass.\n");
	
	vector <vector <string> > groupedReads (numClasses);
	while (true) {
		pair<string, string> read = getAreadFromCSF (inputFile);
		string barcode = read.first;
		string sequence = read.second;
		
		if (barcode.size() == 0) {
			// End of file reached.
			break;
		}
	
		int clanIndex = barcode2intMap.find(barcode)->second;
		if (clanIndex != -1) { // Else it was classified as noise, so skip.
			for (int i=0; i<variableSegLength; i++) {
				char base = sequence.at(i);
				allHistograms.at(clanIndex).at(i).at( base2intMap.find(base)->second )++;
			}
			
			groupedReads.at( clanIndex ).push_back( sequence );
		}
	}
	inputFile.close();
	
	// Output the matrix of all clans
	int parpos = filename.find_last_of(".")+1;
	string path = filename.substr(0,parpos);
	exportHist_as_MatFile(allHistograms, path.substr(0,path.size()-1) + ".hist");
	
	// For insertion libraries, also report the full list of reads in each clan as a list, it might be simpler to process downstream that way.
	exportClusters(groupedReads, path + "reads");
		
	return;
}



// g++ server.cpp -fopenmp -O3 && ./a.out <folder name> [-c]
// Submit to a server using a batch job submission script.
// sbatch submit.sh
// Query:
// squeue -u gkayikc1@jhu.edu

int main (int argc, const char** argv) {
	base2intMap['A'] = 0;
	base2intMap['a'] = 0;
	base2intMap['T'] = 1;
	base2intMap['t'] = 1;
	base2intMap['C'] = 2;
	base2intMap['c'] = 2;
	base2intMap['G'] = 3;
	base2intMap['g'] = 3;

	int2baseMap[0] = 'A';
	int2baseMap[1] = 'T';
	int2baseMap[2] = 'C';
	int2baseMap[3] = 'G';
	
	if (argc < 2) {
		cout << "No directory to process provided." << endl;
		return -1;
	}
	string dirname(argv[1]);
	cout << "Folder to process: " << dirname << endl;
	
	bool compressForServer = false; // true: converts fastq > csf; false: imports csf and analyses.
	if (argc>2 && argv[2][1]=='c')
		compressForServer = true;
		
	try {
		vector <string> dirlist = listDirectory(dirname);
		
		if (compressForServer)
			dirlist = filterDirectoryList(dirlist, ".fastq");
		else
			dirlist = filterDirectoryList(dirlist, ".csf");
		
		for (int i=0; i<dirlist.size(); i++) {
			string filename = dirlist.at(i);
			cout << "Processing " << filename << endl;
		
			double startTime = getCurrentTimeStamp();
			// -------------------------------------------------
			
			if (compressForServer) {
				string filenameReverse = dirlist.at(i+1);
				compressFile(filename, filenameReverse);
				i++;
			}
			else
				analyseCompressedFile(filename);
			
			// -------------------------------------------------
			double endTime = getCurrentTimeStamp();
			printElapsedTime (startTime, endTime, filename);
		}
		cout << "All tasks completed." << endl;	
	}
	
	catch (const char* err) {
		cout << err << endl;
	}
	
	return 0;
}



