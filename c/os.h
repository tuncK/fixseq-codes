#include <iostream>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sstream>
#include <fstream>
#include <algorithm>


using namespace std;



double getCurrentTimeStamp (void) {
	timeval time;
	gettimeofday(&time, NULL);
	return time.tv_sec; 
}


void printElapsedTime (double wallt1, double wallt2, string taskname) {
	float duration = (wallt2-wallt1); // in seconds
	if (duration < 200)
		cout << "Execution time for " << taskname << " was " << duration << " seconds." << endl;
	else if (duration/60 < 200)
		cout << "Execution time for " << taskname << " was " << duration/60 << " minutes." << endl;
	else
		cout << "Execution time for " << taskname << " was " << duration/3600 << " hours." << endl;
}


template <typename T>
string var2string ( T Number ) {
	ostringstream ss;
	ss << Number;
	return ss.str();
}


/*
bool is_file(string fileName) {
	std::ifstream infile(fileName.c_str());
	return infile.good();
}



bool is_folder(string dirName) {
	struct stat filestat;
	stat(dirName.c_str(), &filestat);
	return S_ISDIR(filestat.st_mode);
}
*/

/*
size_t getFilesize(const char* filename) {
	struct stat st;
	if(stat(filename, &st) != 0)
		return 0;
	return st.st_size;
}
*/


vector <string> listDirectory (const string dirname) {
	vector <string> files;			
	struct stat filestat;
	stat(dirname.c_str(), &filestat);
	if (S_ISDIR(filestat.st_mode) == false) {
		if (S_ISREG(filestat.st_mode) == true) {
			files.push_back(dirname);
			return files;
		}
		else {
			cout << "ERROR: " + dirname + " is not a file or directory." << endl;
			throw("ERROR: " + dirname + " is not a file or directory.");
		}
	}
	
	DIR* dir = opendir(dirname.c_str());
	if (dir == NULL)
		throw("ERROR: " + dirname + " could not be opened.");
	
	while (dirent* dirp = readdir(dir)) {
		string filepath = dirname + "/" + dirp->d_name;
		// If it is a hidden file or directory, skip it
		if (dirp->d_name[0] == '.')
			continue;
			
		struct stat filestat2;
		stat(filepath.c_str(), &filestat2);
		// If the file is a directory get the files in the subdirectories
		if (S_ISDIR(filestat2.st_mode)==true) {
			vector <string> temp = listDirectory(filepath);
			for (int k=0; k<temp.size(); k++)
				files.push_back(temp.at(k));
			continue;
		}
		files.push_back(filepath);
	}
	closedir(dir);
	
	// Order the file list alphabetically
	sort(files.begin(), files.end());
	return files;
}



vector <string> filterDirectoryList (const vector <string> in, string extension) {
	vector <string> out;
	for (int i=0; i<in.size(); i++) {
		if (in.at(i).find(extension) != string::npos)
			out.push_back(in.at(i));
	}
	
	return out;	
}



