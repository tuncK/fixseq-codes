// Analysis code that characterises ensemble properties of a NGC output
// Tunc Kayikcioglu


#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include "dbscan.h"
#include "fastq.h"
#include "design.h"
#include "files.h"
#include "os.h"
#include "align.h"


using namespace std;


vector <pair <string,string> > detectFeatures (FASTQfile& fastq, bool analysePairEnd = true) {
	const int numReads = fastq.size();
	if (numReads == 0) {
		cout << "No reads to process." << endl;
		throw(1);
	}

	const int adaptorMatchThreshold = 0.6*leftAdaptorLength; // At least this much of the adaptor should match.
	const int minReadLength = barcodeLength + restrictionOffset + variableSegLength + leftAdaptorLength; // All reads should at least have a length enough for the read and half an adaptor on both sides.
	const int shiftThreshold = 5; // Discrepancy exceeding this will trigger an attempt to detect possible shifts of the sequence of interest

	vector <string> rawreads(2*numReads);
	for (int i=0; i<numReads; i++) {
		rawreads.at(i) = fastq.reads.at(i).rawseq;
		rawreads.at(i+numReads) = fastq.reverseReads.at(i).rawseq;
	}
	vector <pair<string,string> > out;
	out.reserve(2*numReads); // first: barcode, second: variable segment
	
	int numReadsToAnalyse = numReads;
	if (analysePairEnd)
		numReadsToAnalyse *= 2;
	
	vector<int> returnFlags (numReadsToAnalyse);
	int shiftCounter = 0;
	
	# pragma omp parallel for schedule (dynamic, 1000)
	for (int i=0; i<numReadsToAnalyse; i++) {
		returnFlags.at(i) = 1;
		string sequence = rawreads.at(i);
		sequence = trimUnknownBases(sequence);
		
		if (sequence.size() < minReadLength) {
			returnFlags.at(i) = 2;
			continue;
		}
		
		pair <int,int> Wscore = semiglobalAlignment(sequence, leftAdaptor);
		int adaptorEndPos = Wscore.first;
		int score = Wscore.second;
		
		if (score < adaptorMatchThreshold || adaptorEndPos > 30 || adaptorEndPos < 15) { // Adaptor not read accurately enough or misaligned too much
			returnFlags.at(i) = 3;
			continue;
		}
		
		int variableSegmentStartPos = adaptorEndPos+barcodeLength+restrictionOffset;
		string barcode = sequence.substr(adaptorEndPos, barcodeLength);
		string detectedVariableSegment = sequence.substr(variableSegmentStartPos, variableSegLength);		

		// Optional scan for shifts of variable segment
		// If a significant deviation is detected, +/- 1 base shifts are tried and saved if a good match.
		if (HammingDistance(detectedVariableSegment,variableSegment) > shiftThreshold) {
			#pragma omp atomic
			shiftCounter++;
		
			pair<int,int> variableHitParams = biasedAlignment(sequence,variableSegment,variableSegmentStartPos);
			detectedVariableSegment = sequence.substr(variableHitParams.second,variableSegLength);
			if (HammingDistance(detectedVariableSegment,variableSegment) > shiftThreshold) {
				// Observed variable sequence significantly deviates from expectations
				returnFlags.at(i) = 4;
				continue;
			}
		}

		pair <string,string> temp (barcode,detectedVariableSegment);
		#pragma omp critical
		out.push_back(temp);
	}
	
	for (int i=0; i<numReads; i++)
		fastq.reads.at(i).flag = returnFlags.at(i);
	for (int i=numReads; i<numReadsToAnalyse; i++)
		fastq.reverseReads.at(i%2).flag = returnFlags.at(i);
	
	cout << "Number of reads suffering from shifts: " << shiftCounter << endl;
	
	out.reserve(out.size());
	return out;
}


// Extracts the subsequence corresponding to the left ligation site and finds the most likely ligation patterns to facilitate troubleshooting
void analyseLigationPattern (string filename) {
	// Import the fastq files
	int parpos = filename.find_last_of(".")+1;
	string path = filename.substr(0,parpos-1);
	
	// Include the read from the paired end.
	string filenameReverse = filename.substr(0,filename.size()-7) + "2.fastq";
	cout << "Importing " << filename << "..." << endl;
	FASTQfile fastq (filename, filenameReverse);
	
	
	const int numReads = fastq.size();
	const int adaptorMatchThreshold = 0.6*leftAdaptorLength; // At least this much of the adaptor should match.
	const int minReadLength = barcodeLength + restrictionOffset + variableSegLength + leftAdaptorLength; // All reads should at least have a length enough for the read and half an adaptor on both sides.

	vector <string> rawreads(2*numReads);
	for (int i=0; i<numReads; i++) {
		rawreads.at(i) = fastq.reads.at(i).rawseq;
		rawreads.at(i+numReads) = fastq.reverseReads.at(i).rawseq;
	}
	// NOTE: Only forwards reads are considered below!!!
	
	int extractionOffset = 29;
	int length2extract = 16;
	// A redundant tree system to detect and store exact duplicates.
	set < pair<string,int>, operatorLess<string,int> > dataTree;
	
	# pragma omp parallel for schedule (dynamic, 1000)
	for (int i=0; i<numReads; i++) {
		string sequence = rawreads.at(i);
		//sequence = trimUnknownBases(sequence);
		
		if (sequence.size() < minReadLength) {
			continue;
		}
		
		pair <int,int> Wscore = semiglobalAlignment(sequence, leftAdaptor);
		int adaptorEndPos = Wscore.first;
		int score = Wscore.second;
		
		if (score < adaptorMatchThreshold || adaptorEndPos > 30 || adaptorEndPos < 15) { // Adaptor not read accurately enough or misaligned too much
			continue;
		}
		
		int extractionStartPos = adaptorEndPos + extractionOffset;
		// string extractedSeq = sequence.substr(extractionStartPos, length2extract);
		string extractedSeq = sequence.substr(extractionStartPos, 6) + "NNNNNNN" + sequence.substr(extractionStartPos+13, length2extract-13);
		pair <string,int> entry2insert (extractedSeq, 0); // second: keeps track of corresponding element counts.
		
		#pragma omp critical
		{
			pair <set <pair <string,int> >::iterator, bool> ret = dataTree.insert(entry2insert);
			if (ret.second == false) {
				// Report as a duplicate of another entry
				int hitCounts = ret.first->second;
				pair <string,int> newEntry2insert (ret.first->first, hitCounts+1);
				dataTree.erase(ret.first);
				dataTree.insert(newEntry2insert);
			}
		}
	}
	
	// Output reads that are abundant.
	int countThreshold = 1000;
	vector <pair<string,int> > out;
	for ( set <pair <string,int> >::iterator iter = dataTree.begin(); iter!=dataTree.end(); ++iter) {
		int count = iter->second;
		if (count > countThreshold) {
			pair<string,int> temp (iter->first,count);
			out.push_back(temp);
		}
	}
	
	exportPairedList(out, path + ".clustered");	
	return;
}



vector <string> importLib (string filename) {
	vector <string> out;
	out.reserve(5000);
	
	fstream inputFile;
	inputFile.open(filename.c_str());
	while (true) {
		if (inputFile.eof()==true)
			break;
		
		string sequence;
		getline(inputFile, sequence);
		out.push_back(sequence);
	}
	
	inputFile.close();
	out.pop_back();
	return out;
}



void wholeChipClustering (const string filename) {
	// Import the fastq files
	int parpos = filename.find_last_of(".")+1;
	string path = filename.substr(0,parpos-1);
	
	// Include the read from the paired end.
	string filenameReverse = filename.substr(0,filename.size()-7) + "2.fastq";
	cout << "Importing " << filename << "..." << endl;
	FASTQfile fastq (filename, filenameReverse);
	
	int numReads = fastq.size();
	vector <string> allreads (2*numReads);
	# pragma omp parallel for schedule (dynamic, 1000)
	for (int i=0; i<numReads; i++) {
		allreads.at(i) = fastq.reads.at(i).rawseq;
		allreads.at(i+numReads) = fastq.reverseReads.at(i).rawseq;
	}
	
	vector <string> subsetReads = allreads;
	subsetReads.resize(100000);
	
	// Do DBSCAN to find quasi-duplicates
	vector <int> classLabels = findClusters (subsetReads, 10, 5, HammingDistance);
	int numClasses = classLabels.back();
	classLabels.pop_back();
	
	cout << "Number of classes found: " << numClasses << endl;
	
	vector <vector <string> > groupedReads (numClasses);
	vector <string> noiseReads;
	for (int i=0; i<classLabels.size(); i++) {
		int label = classLabels.at(i);
		if (label != -1) {
			groupedReads.at( label ).push_back( allreads.at(i) );
		}
		else {
			noiseReads.push_back( allreads.at(i) );
		}
	}
	
	// Approximate sequence prototype of quasi-duplicates by a histogram
	// To reduce complexity
	vector <string> centroids (numClasses);
	# pragma omp parallel for schedule (dynamic, 1000)
	for (int i=0; i<numClasses; i++) {
		vector <vector <double> > temp = baseHistogram (groupedReads.at(i), 150);
		centroids.at(i) = histogram2DNA(temp);
	}
	
	noiseReads = allreads;
	
	// Check the noise reads against the centroids
	int numCentroids = centroids.size();
	int numNoiseReads = noiseReads.size();
	int distanceThreshold = 10;
	
	# pragma omp parallel for schedule (dynamic, 1000)
	for (int i=0; i<numNoiseReads; i++) {
		string noiseSeq = noiseReads.at(i);
		int bestHitScore = INF;
		int secondBestHitScore = INF;
		int bestHitIdx = -1;
		//int secondBestHitIdx = -1;
		
		for (int j=0; j<numCentroids; j++) {
			int score = NeedlemanWunsch (noiseSeq,centroids.at(j));
			if (score<bestHitScore) {
				secondBestHitScore = bestHitScore;
			//	secondBestHitIdx = bestHitIdx;
				bestHitScore = score;
				bestHitIdx = j;
			}
			else if (score<secondBestHitScore) {
				secondBestHitScore = score;
			//	secondBestHitIdx = j;
			}
			
			if (score == 0)
				break;
		}
		
		double qualityRatio = bestHitScore/(double) secondBestHitScore;
		if (bestHitScore < distanceThreshold && qualityRatio<0.1) {
			#pragma omp critical
			groupedReads.at(bestHitIdx).push_back(noiseSeq);
		}
	}
	
	vector <string> significant;
	for (int i=0; i<numClasses; i++) {
		if (groupedReads.at(i).size()>100) {
			vector <vector <double> > temp = baseHistogram (groupedReads.at(i), 10);
			string DNA = histogram2DNA(temp);
			significant.push_back(DNA);
		}
	}	
	
	exportList(significant, path + ".sig");	
	return;
}




void processFile (const string filename) {
	// Import the fastq files
	int parpos = filename.find_last_of(".")+1;
	string path = filename.substr(0,parpos-1);
	
	// Include the read from the paired end.
	string filenameReverse = filename.substr(0,filename.size()-7) + "2.fastq";
	cout << "Importing " << filename << "..." << endl;
	FASTQfile fastq (filename, filenameReverse);
	detectFeatures(fastq);
	
	// Simple list of reads
	int numReads = fastq.size();
	vector <string> reads (2*numReads);
	for (int i=0; i<numReads; i++) {
		reads.at(i) = fastq.reads.at(i).rawseq;
		reads.at(i+numReads) = fastq.reverseReads.at(i).rawseq;
	}
	reads.resize(numReads); // Only forward reads are considered for simplicity !!!
	
	// Ensemble average sequences
	vector <vector <double> > psuedoSangerSequence = baseHistogram (reads, 150);
	exportMatrix(psuedoSangerSequence, path + "_avg.hist");
	
	
	// List of aligned reads
	vector <pair <string,string> > extractedReads = detectFeatures(fastq, true); // first:barcode, second:variable segment
	int numExtracted = extractedReads.size();
	vector <string> alignedReads (numExtracted);
	for (int i=0; i<numExtracted; i++) {
		alignedReads.at(i) = extractedReads.at(i).second;
	}
	// Ensemble average sequences
	psuedoSangerSequence = baseHistogram (alignedReads, 73);
	exportMatrix(psuedoSangerSequence, path + "_selected.hist");
	
	
	cout << "Starting accuracy analysis of mapping..." << endl;
	// Check the second barcode-observed MM harmony to deduce error rate.
	// Import the barcode-sequence map from file.
	vector<string> idealReads = importLib("../library_ordered_twist.txt");
	int libsize = idealReads.size();

	// Splice the imported library and organise in a searchable map
	map <string,string> barcodeSeqMap;
	for (int i=0; i<libsize; i++) {
		string barcode = idealReads.at(i).substr(33,7);
		string probe = idealReads.at(i).substr(40,66);
		barcodeSeqMap[barcode]=probe;
	}
	
	// Search all detected reads in the tree and report.
	int notFoundCounter = 0;
	int correctMapping = 0;
	int incorrectMapping = 0;
	
	//# pragma omp parallel for schedule (dynamic, 1000)
	for (int i=0; i<numExtracted; i++) {
		string bcd = alignedReads.at(i).substr(0,7);
		string seq = alignedReads.at(i).substr(7,66);
		map<string,string>::iterator it = barcodeSeqMap.find(bcd);

		if (it == barcodeSeqMap.end()) { // Barcode not found
			notFoundCounter++;
		}
		else {
			string expectedSeq = it->second;
			// Barcode found, check if ID is accurate
			if (seq == variableSegment || seq == expectedSeq)
				// Expected read
				correctMapping++;
			else
				// Unexpected read
				incorrectMapping++;
		}
	}
	
	cout << "#Barcodes not found: " << notFoundCounter << endl;
	cout << "#Barcodes-seq does not match: " << incorrectMapping << endl;
	cout << "#Barcodes-seq match: " << correctMapping << endl;
	
	return;
}



// g++ fixseq.cpp -fopenmp && ./a.out <folder name>
int main (int argc, const char** argv) {
	if (argc < 2) {
		cout << "No directory to process provided." << endl;
		return -1;
	}
	string dirname(argv[1]);
	cout << "Folder to process: " << dirname << endl;
	
	try {
		vector <string> dirlist = listDirectory(dirname);
		dirlist = filterDirectoryList(dirlist, ".fastq");
		for (int i=0; i<dirlist.size(); i+=2) {
			string filename = dirlist.at(i);
			cout << "Processing " << filename << endl;
		
			double startTime = getCurrentTimeStamp();
			// -------------------------------------------------
			
			// processFile(filename);
			//wholeChipClustering(filename);
			analyseLigationPattern(filename);
			
			// -------------------------------------------------
			double endTime = getCurrentTimeStamp();
			printElapsedTime (startTime, endTime, filename);
		}
		cout << "All tasks completed." << endl;	
	}
	
	catch (const char* err) {
		cout << err << endl;
	}
	
	return 0;
}


