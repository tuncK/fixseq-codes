#!/bin/bash

#SBATCH --job-name=tunc
#SBATCH --time=3-0:0:0
#SBATCH --partition=shared
#SBATCH --nodes=1

# number of tasks (processes) per node
#SBATCH --ntasks-per-node=24
#SBATCH --mincpus=8

#SBATCH --mail-type=FAIL,END
#SBATCH --mail-user=tkayikci@jhu.edu
#SBATCH -o ./job.stdout



echo "Started processing."

./a.out ~/scratch/hiseq/

echo "Finished with job $SLURM_JOBID"

