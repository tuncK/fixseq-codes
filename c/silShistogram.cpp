// Analysis of the IL libraries after clustering to deduce substitution prevalence histogram
// The groups of reads that belong to the same clan are compared to the fixed/variable strands and counted.
// Tunc Kayikcioglu 2020


#include <iostream>
#include <vector>
#include <fstream>
#include "files.h"
#include "align.h"

using namespace std;


// CATATGCGGTGTGAAATACCGCA NNNNNANNNNNANNNNNANNNNNANNNNN GAGCTC NNNNNNN TTTATTCTTGTAATACTAGTCATCCTCGTGATGCTGGAAACAAGACCACGAGCAGGCCCGCGGGTT CTCGAGGCAAGCTTGGCGT
void analyseReadsFile (const string filename, bool isSIL) {
	const int maxDistLimit = 2;
	
	int barcodeLength = 7;
	if (!isSIL)
		barcodeLength = 8;
	
	// Import the reads file.
	fstream inputFile;
	inputFile.open(filename.c_str());
	if (inputFile.is_open() == false) {
		cout << "Error: File " + filename + " could not be read." << endl;
		throw(0);
	}
	else
		cout << "Importing " << filename << endl;
	
	vector <vector <string> > allClans;
	while (true) {
		vector <string> clan = readAclan (inputFile);
		if (clan.size() == 0) {
			// End of input file was reached.
			break;
		}
		allClans.push_back(clan);
	}
	inputFile.close();
	
	cout << "Starting fixed/variable strand assignments..." << endl;
	string fixedStrand = "TTTATTCTTGTAATACTAGTCATCCTCGTGATGCTGGAAACAAGACCACGAGCAGGCCCGCGGGTT";
	
	// List all possible insertion scenarios.
	vector <string> insertions;
	for (int i=0; i<fixedStrand.size()-2; i++) {
		string temp;
		if (isSIL)
			temp = fixedStrand.substr(0,i) + "N" + fixedStrand.substr(i);
		else
			temp = fixedStrand.substr(0,i) + "NN" + fixedStrand.substr(i);
		insertions.push_back( temp.substr(0,fixedStrand.size()) );
	}
	
	vector <double> sPrevalence (allClans.size());
	# pragma omp parallel for schedule (dynamic, 100)
	for (int clanID=0; clanID<allClans.size(); clanID++) {
		vector <string> clan = allClans.at(clanID);
		
		// Check each member of the clan to decide if it is shifted.
		int Fcount = 0;
		int Vcount = 0;
		vector <int> vIDlist;
		for (int i=0; i<clan.size(); i++) {
			string read = clan[i].substr( barcodeLength, fixedStrand.size() ); // Ignore mapping barcode
			int Fdist = HammingDistance(read, fixedStrand); // fixed strand prototype
			
			// Find insertion position by brute force, 
			// since no napping barcode info available.
			int Vdist = std::numeric_limits<int>::max();
			int vindex = -1;
			for (int j=0; j<insertions.size(); j++) {
				int tempdist = HammingDistance( read, insertions.at(j) );
				if (tempdist < Vdist) {
					Vdist = tempdist;
					vindex = j;
				}
			}
			
			if (Vdist<Fdist && Vdist<=maxDistLimit) {
				Vcount++;
				vIDlist.push_back(vindex);
			}
			
			if (Fdist<Vdist && Fdist<=maxDistLimit)
				Fcount++;
		}
		
		if ( (Fcount+Vcount)>=10 )
			sPrevalence.at(clanID) = Vcount / (double) (Fcount + Vcount);
		else
			sPrevalence.at(clanID) = -1;
	}
	
	// Eliminate skipped clans from the prevalence list
	for (int i=0; i<sPrevalence.size(); i++) {
		if (sPrevalence[i] == -1) {
			sPrevalence[i] = sPrevalence.back();
			sPrevalence.pop_back();
			i--;
		}
	}
	
	// Output the s's of all clans
	int parpos = filename.find_last_of(".")+1;
	string path = filename.substr(0,parpos);
	string outfilename = path.substr(0,path.size()-1) + ".csv";
	ofstream file;
	file.open(outfilename.c_str());
	for (int i=0; i<sPrevalence.size(); i++)
		file << sPrevalence.at(i) << endl;
	file.close();
	
	return;
}



int main (int argc, const char** argv) {
	for (int fileID=1; fileID<=4; fileID++) {
		string filename = "~/sil/" + to_string(fileID) + ".reads";
		analyseReadsFile (filename, true);
	}
	
	{
		int fileID = 11;
		string filename = "~/sil/3cl1/" + to_string(fileID) + ".reads";
		analyseReadsFile (filename, true);
		
		fileID = 12;
		filename = "~/sil/3cl1/" + to_string(fileID) + ".reads";
		analyseReadsFile (filename, true);
		
		fileID = 18;
		filename = "~/sil/3cl1/" + to_string(fileID) + ".reads";
		analyseReadsFile (filename, true);
	}
	
	for (int fileID=5; fileID<=12; fileID++) {
		string filename = "~/dil/" + to_string(fileID) + ".reads";
		analyseReadsFile (filename, false);
	}
	
	{
		int fileID = 11;
		string filename = "~/dil/3cl1/" + to_string(fileID) + ".reads";
		analyseReadsFile (filename, false);
		
		fileID = 12;
		filename = "~/dil/3cl1/" + to_string(fileID) + ".reads";
		analyseReadsFile (filename, false);
		
		fileID = 18;
		filename = "~/dil/3cl1/" + to_string(fileID) + ".reads";
		analyseReadsFile (filename, false);
	}
	
	return 0;
}



