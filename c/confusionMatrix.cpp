// Analysis of the SIL/DIL libraries after clustering.
// The groups of reads that belong to the same clan are compared to the fixed/variable strands and counted.
// Tunc Kayikcioglu 2020


#include <iostream>
#include <vector>
#include <set>
#include <fstream>
#include "files.h"
#include "align.h"

using namespace std;


const string fixedStrand = "TTTATTCTTGTAATACTAGTCATCCTCGTGATGCTGGAAACAAGACCACGAGCAGGCCCGCGGGTTCTCGAG";


// CGCTCCTTGTTGTACTCGCAGAGCTC TACTAGT TTTATTCTTGTAATACTAGTCATCCTCGTGATGCTGGAAACAAGACCACGAGCAGGCCCGCGGGTT CTCGAG ATACCGGCGGTTGTTCAACC
// TGACGGAGGATAGAAGGCCAGAGCTC ATTTCCCG TTTATTCTTGTAATACTAGTCATCCTCGTGATGCTGGAAACAAGACCACGAGCAGGCCCGCGGGTT CTCGAG ATACCGGCGGTTGTTCAACC
vector < map<string,string> > importLibrary (const string filename) {
	fstream inputFile;
	inputFile.open(filename.c_str());
	if (inputFile.is_open() == false) {
		cout << "Error: File " + filename + " could not be read." << endl;
		throw(0);
	}
	
	map<string,string> SILmap;
	map<string,string> DILmap;
	
	for (int linecounter=0; linecounter<3300; linecounter++) {
		string line;
		getline(inputFile, line);
		string barcode = line.substr(26,7);
		string deb3 = line.substr(33,fixedStrand.size());
		SILmap[barcode] = deb3;
	}
	
	for (int linecounter=3300; linecounter<11220; linecounter++) {
		string line;
		getline(inputFile, line);
		string barcode = line.substr(26,8);
		string deb3 = line.substr(34,fixedStrand.size());
		DILmap[barcode] = deb3;
	}
	inputFile.close();
	
	vector < map<string,string> > out (2);
	out[0] = SILmap;
	out[1] = DILmap;
	return out;
}



// Generate a list of all expected sequences w.r.t repair matrix element
vector< pair<string, pair<int,int> > > assignMMids (bool is_SIL) {
	int numBaseIDs, mappingBarcodeLength;
	
	if (is_SIL) {
		numBaseIDs = 4;
		mappingBarcodeLength = 7;
	}
	else {
		numBaseIDs = 16;
		mappingBarcodeLength = 8;
	}
	
	vector< pair<string, pair<int,int> > > out;
	set <string> variableStrands;
	int len = fixedStrand.size();
	
	for (int i=1; i<len-6; i++) {
		for (int baseID=0; baseID<numBaseIDs; baseID++) {
			string variableStrand;
			if (is_SIL) {
				char base = int2baseMap[baseID];
				variableStrand = fixedStrand.substr(0,i) + base + fixedStrand.substr(i);
			}	
			else {
				char base1 = int2baseMap[baseID/4];
				char base2 = int2baseMap[baseID%4];
				variableStrand = fixedStrand.substr(0,i) + base1 + base2 + fixedStrand.substr(i);
			}
				
			variableStrand = variableStrand.substr(0,fixedStrand.size());
			if ( variableStrands.find(variableStrand)==variableStrands.end() ) {
				// Element encountered for the first time, insert it.
				variableStrands.insert(variableStrand);
				
				pair<int,int> temp1 (i, baseID);
				pair<string, pair<int,int> > temp (variableStrand,temp1);
				out.push_back(temp);
			}
		}
	}
	
	return out;
}



void analyseReadsFile (const string filename, bool is_SIL, const map <string,string>& barcodeMap, const vector< pair<string, pair<int,int> > >& varStrandList ) {
	const int maxDistLimit = 2;
	
	// Import the reads file.
	fstream inputFile;
	inputFile.open(filename.c_str());
	if (inputFile.is_open() == false) {
		cout << "Error: File " + filename + " could not be read." << endl;
		throw(0);
	}
	else
		cout << "Importing " << filename << endl;
	
	vector <vector <string> > allClans;
	while (true) {
		vector <string> clan = readAclan (inputFile);
		if (clan.size() == 0) {
			// End of input file was reached.
			break;
		}
		allClans.push_back(clan);
	}
	inputFile.close();
	
	int mappingBarcodeLength;
	if (is_SIL) {
		mappingBarcodeLength = 7;
	}
	else {
		mappingBarcodeLength = 8;
	}
	
	cout << "Starting construction of the confusion matrix..." << endl;
	vector <vector <int> > confusionMatrix (allClans.size());
	
	# pragma omp parallel for schedule (dynamic, 100)
	for (int clanID=0; clanID<allClans.size(); clanID++) {
		vector <string> clan = allClans.at(clanID);
		
		// Calculate the average mapping barcode
		vector <string> mappingBarcodes;
		for (int i=0; i<clan.size(); i++) {
			string barcode = clan[i].substr(0,mappingBarcodeLength);
			mappingBarcodes.push_back(barcode);
		}
		string ensembleBarcode = histogram2DNA(baseHistogram(mappingBarcodes));
		
		// Look-up the barcode map
		if (barcodeMap.find(ensembleBarcode) == barcodeMap.end()) {
			// Mapping barcode not found in the lookup table, skip this clan.
			continue;
		}
		
		// Find the expected position and base based on the mapping barcode
		string expectedVariableStrand = barcodeMap.find(ensembleBarcode)->second.substr(0,fixedStrand.size());
		int expectedPos, expectedBase;
		for (int i=0; i<varStrandList.size(); i++) {
			if (varStrandList.at(i).first == expectedVariableStrand) {
				expectedPos = varStrandList.at(i).second.first;
				expectedBase = varStrandList.at(i).second.second;
				break;
			}
		}
				
		// Check each member of the clan about which position and type they resemble the most.
		vector <string> variableStrandReads;
		for (int i=0; i<clan.size(); i++) {
			string read = clan[i].substr(mappingBarcodeLength, fixedStrand.size());
			
			// Compare the distance of the read to fixed and potential variable strands
			int mindist = HammingDistance(read, fixedStrand); // fixed strand prototype
			int minID = -1;
			for (int j=0; j<varStrandList.size(); j++) {
				int newdist = HammingDistance(read, varStrandList.at(j).first);
				if (newdist < mindist) {
					mindist = newdist;
					minID = j;
				}
			}
			
			if (minID != -1 && mindist < maxDistLimit) {
				// Likely this read is variable strand-derived
				variableStrandReads.push_back(read);
			}
		}
		
		// Filter out clans with very few variable strands.
		if (variableStrandReads.size()<=0.2*double(clan.size()) )
			continue;

		string consensusVar = histogram2DNA (baseHistogram(variableStrandReads));
		int mindist = HammingDistance(consensusVar, fixedStrand); // fixed strand prototype
		int minID = -1;
		for (int j=0; j<varStrandList.size(); j++) {
			int newdist = HammingDistance(consensusVar, varStrandList.at(j).first);
			if (newdist <= mindist) {
				mindist = newdist;
				minID = j;
			}
		}
		
		if (minID!=-1 && mindist<maxDistLimit) {
			int observedPos = varStrandList.at(minID).second.first;
			int observedBase = varStrandList.at(minID).second.second;
		
			vector <int> temp (4);
			temp[0] = expectedPos;
			temp[1] = expectedBase;
			temp[2] = observedPos;
			temp[3] = observedBase;
			
			confusionMatrix[clanID] = temp;
		}
	}
	
	// Eliminate skipped clans from the list
	for (int i=0; i<confusionMatrix.size(); i++) {
		if (confusionMatrix[i].size() == 0) {
			confusionMatrix[i] = confusionMatrix.back();
			confusionMatrix.pop_back();
			i--;
		}
	}
	
	// Output the F V counts of all clans
	int parpos = filename.find_last_of(".")+1;
	string path = filename.substr(0,parpos);
	exportCSV(confusionMatrix, path.substr(0,path.size()-1) + ".csv");
	return;
}



int main (int argc, const char** argv) {
	base2intMap['A'] = 0;
	base2intMap['a'] = 0;
	base2intMap['T'] = 1;
	base2intMap['t'] = 1;
	base2intMap['C'] = 2;
	base2intMap['c'] = 2;
	base2intMap['G'] = 3;
	base2intMap['g'] = 3;

	int2baseMap[0] = 'A';
	int2baseMap[1] = 'T';
	int2baseMap[2] = 'C';
	int2baseMap[3] = 'G';
	
	// Import the 2nd barcode mapping from file
	cout << "Importing maps..." << endl;
	vector < map<string,string> > maps = importLibrary ("/home/tk/fixseq-codes/processHists/SIL-DIL-sequencesOrdered.txt");
	vector< pair<string, pair<int,int> > > silVarStrandMap = assignMMids (true);
	vector< pair<string, pair<int,int> > > dilVarStrandMap = assignMMids (false);
	
	
	for (int fileID=1; fileID<5; fileID++) {
		string filename = "/home/tk/sil/" + to_string(fileID) + ".reads";
		analyseReadsFile (filename, true, maps[0], silVarStrandMap);
	}
	
	/*
	for (int fileID=6; fileID<7; fileID++) {
		string filename = "/home/tk/dil/" + to_string(fileID) + ".reads";
		analyseReadsFile (filename, false, maps[1], dilVarStrandMap);
	}*/
	
	return 0;
}



