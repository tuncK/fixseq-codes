#include <iostream>
#include <sstream>


#define INF 1000000
#define MATCH_GAIN 1
#define GAP_PENALTY 1
#define MAX_DNA_LENGTH 300 // Longest possible MiSeq read is 2x300 cycles

using namespace std;


bool compareBase (const char a, const char b) {
	if (a==b || a=='N' || b=='N')
		return true;
	else
		return false;
}


string trimUnknownBases (const string& x) {
	string out;
	for (int i=0; i<x.size(); i++) {
		char base = x.at(i);
		if (base != 'N')
			out.push_back(base);
	}
	return out;
}


// Returns a score based on the alignment quality. Lower scores are better, 0 indicates identity.
// BUG: table is initialised as int
double NeedlemanWunsch (const string& a, const string& b) {
	int aSize = a.size()+1;
	int bSize = b.size()+1;
	
	//int table [bSize][aSize];
	static int table [MAX_DNA_LENGTH][MAX_DNA_LENGTH];
	static bool firstVisit = true;
	
	#pragma omp threadprivate (table, firstVisit)
	if (firstVisit == true) {
		for (int i=0; i<aSize; i++)
			table[0][i]= -i*GAP_PENALTY;
		for (int i=0; i<bSize; i++)
			table[i][0]= -i*GAP_PENALTY;
		firstVisit = false;
	}	
	
	for (int i=1; i<bSize; i++)
		for (int j=1; j<aSize; j++) {
			int var;
			if (compareBase(a[j-1],b[i-1]))
				var = table[i-1][j-1] + MATCH_GAIN;
			else
				var = table[i-1][j-1] - MATCH_GAIN;
			
			int down = table[i-1][j] - GAP_PENALTY;
			int right = table[i][j-1] - GAP_PENALTY;
			if (down > var)
				var = down;
			if (right > var)
				var = right;
			table[i][j] = var;
		}
	
	double constructLength = min(aSize,bSize)-1;
	return constructLength - table[bSize-1][aSize-1];
}


pair <int,int> semiglobalAlignment (const string& a, const string& b) { 
	// a: main sequence, b: adaptor to find
	// Because of the asymmetric treatment when finding the end position, the order in which the two strings are provided matters.
	// So we ensure the order is correct.
	int asize = a.size()+1;
	int bsize = b.size()+1;
	if (asize < bsize)
		return semiglobalAlignment(b,a);

	int table[bsize][asize];
	for (int i=0; i<bsize; i++)
		table[i][0] = 0;
	for (int i=0; i<asize; i++)
		table[0][i] = 0;
	
	for (int i=1; i<bsize; i++) {
		for (int j=1; j<asize; j++) {
			int diag = table[i-1][j-1];
			if (compareBase(a[j-1],b[i-1]))
				diag += MATCH_GAIN;
			
			int down = table[i-1][j] - GAP_PENALTY;
			int right = table[i][j-1] - GAP_PENALTY;					
			if (j == asize-1 || i == bsize-1) {			
				down += GAP_PENALTY;
				right += GAP_PENALTY;
			}
			
			if (down > diag)
				diag = down;
			if (right > diag)
				diag = right;
			table[i][j] = diag;
		}
	}
	
	// Skip the trailing end to find the end position, which is the maximum in the last row.
	int maxval = 0;
	int endPos = 0;
	for (int i=1; i<asize; i++) {
		if (table[bsize-1][i] > maxval) {
			maxval = table[bsize-1][i];
			endPos = i;
		}
	}

	pair <int,int> out (maxval,endPos); // first:match score, second:end position of the alignment
	return out;
}


// Find potential shifts of position of b within a from the expected position
// Proper hit position is biased in favour 
pair <int,int> biasedAlignment (const string& a, const string& b, const int hitPos) {
	// a: main sequence, b: subsequence to search
	// Because of the asymmetric treatment when finding the end position, the order in which the two strings are provided matters.
	// So we ensure the order is correct.
	int asize = a.size()+1;
	int bsize = b.size()+1;
	if (asize < bsize)
		return biasedAlignment(b, a, hitPos);

	int table[bsize][asize];
	for (int i=0; i<asize; i++)
		table[0][i] = abs(i-hitPos);
	
	for (int i=0; i<bsize; i++)
		table[i][0] = hitPos+i;

	for (int i=1; i<bsize; i++) {
		for (int j=1; j<asize; j++) {
			int diag = table[i-1][j-1];
			if (!compareBase(a[j-1],b[i-1]))
				diag ++;
			
			int down = table[i-1][j] + 1;
			int right = table[i][j-1] + 1;					
					
			if (down < diag)
				diag = down;
			if (right < diag)
				diag = right;
			table[i][j] = diag;
		}
	}
	
	// Skip the trailing end to find the end position, which is the minimum in the last row.
	int minval = asize + bsize;
	int endPos;
	for (int i=asize-1; i>=0; i--) {
		if (table[bsize-1][i] < minval) {
			minval = table[bsize-1][i];
			endPos = i;
		}
	}

	pair <int,int> out (minval,endPos-bsize+1); // first:match score, second:start position of the alignment
	return out;
}



// Counts the basewise difference between two sequences. Lower scores are better.
double HammingDistance (const string& a, const string& b) {
	int size = a.size();
	if (b.size() != size) {
		// throw("Error: string lengths must match.");
		return INF;
	}
		
	int dist = 0;
	for (int i=0; i<size; i++) {
		if (!compareBase(a[i],b[i]))
			dist++;
	}
	
	return dist;
}


// Returns a score based on the alignment quality. Lower scores are better, 0 indicates identity.
// Global alignment enforced
int editDistance (const string& a, const string& b) {
	int aSize = a.size()+1;
	int bSize = b.size()+1;
	
	int table [bSize][aSize];
	for (int i=0; i<aSize; i++)
		table[0][i]= i;
	for (int i=0; i<bSize; i++)
		table[i][0]= i;
	
	for (int i=1; i<bSize; i++) {
		for (int j=1; j<aSize; j++) {
			int var = table[i-1][j-1];
			if (!compareBase(a[j-1],b[i-1]))
				var++;
			
			int down = table[i-1][j] + 1;
			int right = table[i][j-1] + 1;
			if (down < var)
				var = down;
			if (right < var)
				var = right;
			table[i][j] = var;
		}
	}
	
	return table[bSize-1][aSize-1];
}



