// See the algorithm in https://en.wikipedia.org/wiki/DBSCAN


#include <iostream>
#include <vector>
#include <set>

using namespace std;


class dataNode {
	public:
		int isDuplicateOf;
		set <int> neighbours;
		dataNode ();
		dataNode (const dataNode& x);
};


dataNode::dataNode () {
	isDuplicateOf = -1;
}


dataNode::dataNode (const dataNode& x) {
	isDuplicateOf = x.isDuplicateOf;
	neighbours = x.neighbours;
}


template <typename T1, typename T2>
struct operatorLess {
	bool operator() (const pair <T1,T2>& x, const pair <T1,T2>& y) const {
		return x.first < y.first;
	}
};


// Finds neighbours within a given threshold for dbscan purposes. Note that there is a risk of running out of memory for large libraries, and hence we try to avoid storing the distance as a matrix.
template <typename T>
vector <dataNode> findNeighbours (const vector <T>& list, const double threshold, double distanceMetric(const T& x, const T& y) ) {	
	// First organise the data in a new structure.
	int numData = list.size();
	vector <dataNode> datalist (numData); // Data structure to store neighbourship information.
	for (int i=0; i<numData; i++) {
		datalist.at(i).neighbours.insert(i); // Potential loss of memory?
		datalist.at(i).isDuplicateOf = -1;
	}
	
	// Detect exact duplicates.
	set < pair<T,int>, operatorLess<T,int> > dataTree;
	for (int i=0; i<numData; i++) {
		pair <T,int> temp (list.at(i), i); // second: keeps track of corresponding element Idx.
		pair <typename set<pair<T,int> >::iterator, bool> ret = dataTree.insert(temp);
			  
		if (ret.second == false) {
			// Report as a duplicate of another entry
			int hitIndex = ret.first->second;
			datalist.at(i).isDuplicateOf = hitIndex;
			datalist.at(hitIndex).neighbours.insert(i);
		}
	}
	int numUnique = dataTree.size();
	dataTree.clear();
	
	// Indeces of data points that are not duplicate of any previous data in the list.
	// Only distance calculation between these is sufficient.
	vector <int> nonDuplicateIdx;
	nonDuplicateIdx.reserve(numUnique);
	for (int i=0; i<numData; i++) {
		if (datalist.at(i).isDuplicateOf == -1)
			nonDuplicateIdx.push_back(i);
	}

	// Check the neighbours of non-duplicate entries using the rigorous metric function provided.
	// Main bottleneck in computation time, especially if a heavy metric function is given.
	#pragma omp parallel for schedule (dynamic, 1000)
	for (int k=0; k<nonDuplicateIdx.size(); k++) {
		int i = nonDuplicateIdx.at(k);
		for (vector<int>::iterator it = nonDuplicateIdx.begin(); it!=nonDuplicateIdx.end() && *it<i ; ++it) {
			int j = *it;
			double distance = distanceMetric(list[i], list[j]);
			if (distance < threshold) {
				// Close enough, report as neighbours.
				// No race possible, since read i is only processed by the current thread.
				datalist.at(i).neighbours.insert(j);
				
				// Generate the full list of neighbours considering that the transpose matrix element should also be recorded symmetrically.
				// Note: Duplicates need to be looked up from the corresponding element later on.
				// For the transpose of this element, a data race is possible. Avoid.
				#pragma omp critical
				datalist.at(j).neighbours.insert(i);
			}
		}
	}
	
	return datalist;
}



void assign2cluster (const vector <dataNode>& datalist, vector<int>& classlabels, const int idx, const int label, const int minPts) {
	// Assign the label to the current element
	classlabels.at(idx) = label;
	
	if (datalist.at(idx).isDuplicateOf==-1) {
		// A proper neighbourhood tree exists on idx. Now loop over its neighbours and assign them to the same label
		for (set<int>::iterator iter = datalist.at(idx).neighbours.begin(); iter!=datalist.at(idx).neighbours.end(); ++iter) {
			int nidx = *iter;
			if (classlabels.at(nidx)==-1) {
				// This was not processed before
				classlabels.at(nidx) = label;
	
				// If this neighbour point is also a core point, descend down to its neighbours and add them to the current cluster recursively.
				if (datalist.at(nidx).isDuplicateOf==-1 && datalist.at(nidx).neighbours.size() >= minPts)
					assign2cluster (datalist, classlabels, nidx, label, minPts);
				// Else, it is a duplicate of another element and should have been processed before.
			}
		}
	}
	// Otherwise, it was an exact duplicate of nidx<idx'th element, whose neighbourhoud was already processed before. No action necessary.
	
	return;
}


// Neighbour finding function, highly optimised for Hamming distance.
// Provides a significant CPU-time reduction involving very large data sets.
// Finds neighbours within a given threshold for dbscan purposes. Note that there is a risk of running out of memory for large libraries, and hence we try to avoid storing the distance as a matrix.
template <typename T>
vector <dataNode> findNeighbours_HeavyDuty (const vector <T>& list, const double threshold) {	
	// First organise the data in a new structure.
	int numData = list.size();
	double seqLength = list.at(0).size();
	
	vector <dataNode> datalist (numData); // Data structure to store neighbourship information.
	for (int i=0; i<numData; i++) {
		datalist.at(i).neighbours.insert(i); // Potential loss of memory?
		datalist.at(i).isDuplicateOf = -1;
	}
	
	// Detect exact duplicates.
	set < pair<T,int>, operatorLess<T,int> > dataTree;
	for (int i=0; i<numData; i++) {
		pair <T,int> temp (list.at(i), i); // second: keeps track of corresponding element Idx.
		pair <typename set<pair<T,int> >::iterator, bool> ret = dataTree.insert(temp);
			  
		if (ret.second == false) {
			// Report as a duplicate of another entry
			int hitIndex = ret.first->second;
			datalist.at(i).isDuplicateOf = hitIndex;
			datalist.at(hitIndex).neighbours.insert(i);
		}
	}
	int numUnique = dataTree.size();
	dataTree.clear();
	
	// Indeces of data points that are not duplicate of any previous data in the list.
	// Only distance calculation between these is sufficient.
	vector <int> nonDuplicateIdx;
	nonDuplicateIdx.reserve(numUnique);
	for (int i=0; i<numData; i++) {
		if (datalist.at(i).isDuplicateOf == -1)
			nonDuplicateIdx.push_back(i);
	}
	
	// Generate an indexing scheme based on AT content.
	// Pairwise distances will only be compared if their AT content is similar.
	vector<int> ATcontent (numData);
	for (int i=0; i<numData; i++) {
		T seq = list.at(i);
		int ATcount = 0;
		for (int j=0; j<seq.size(); j++)
			if (seq[j]=='A' || seq[j]=='T')
				ATcount++;
		
		ATcontent.at(i) = ATcount;
	}
	
	vector<vector <int> > ATbins (seqLength+1);
	for (vector<int>::iterator it = nonDuplicateIdx.begin(); it!=nonDuplicateIdx.end(); ++it) {
		for (int allowedBins=max(0.0,ATcontent[*it]-threshold); allowedBins<=min(seqLength,ATcontent[*it]+threshold); allowedBins++)
			ATbins.at(allowedBins).push_back(*it);
	}
	
	// Put a very large number to the end of each list, virtually infinity.
	// This ensures that "j<i" always is true at least once per list.
	for (int i=0; i<ATbins.size(); i++)
		ATbins.at(i).push_back(1e9);
	
	// Check the neighbours of non-duplicate entries using the rigorous metric function provided.
	// Main bottleneck in computation time, especially if a heavy metric function is given.
	#pragma omp parallel for schedule (dynamic, 1000)
	for (int k=0; k<nonDuplicateIdx.size(); k++) {
		int i = nonDuplicateIdx[k];
			
		// Only allowed bins to search are [ATcontent1-threshold,ATcontent1-threshold].
		for (vector<int>::iterator it = ATbins[ATcontent[i]].begin(); *it<i; it++) {
			int j = *it;
			int distance = 0;
			for (int m=0; m<seqLength && distance<threshold; m++) {
				if (list[i][m] != list[j][m])
					distance++;
			}
			
			if (distance < threshold) {
				// Close enough, report as neighbours.
				// No race possible, since read i is only processed by the current thread.
				datalist[i].neighbours.insert(j);
				
				// Generate the full list of neighbours considering that the transpose matrix element should also be recorded symmetrically.
				// Note: Duplicates need to be looked up from the corresponding element later on.
				// For the transpose of this element, a data race is possible. Avoid.
				#pragma omp critical
				datalist[j].neighbours.insert(i);
			}
		}
	}
	
	return datalist;
}



// Classify points as neighbour/outlier based on whether it is in the proximity of a core point.
// -1: outlier, else indicates the class number that the data point falls into.
template <typename T>
vector <int> findClusters (const vector <T>& inputlist, const int minPts, const double searchRadius, double distanceMetric(const T& x, const T& y) ) {
	//vector <dataNode> neighbours = findNeighbours(inputlist, searchRadius, distanceMetric);
	vector <dataNode> neighbours = findNeighbours_HeavyDuty(inputlist, searchRadius);
	
	// Allocate and initialise a container to store cluster IDs. By default, every point is regarded as noise, i.e. -1
	vector <int> classlabels (inputlist.size(), -1);
	
	// Process the data points sequentially
	int currentLabel = 0;
	for (int i=0; i<classlabels.size(); i++) {
		// If this data point was already processed, skip.
		if (classlabels.at(i) != -1)
			continue;
		
		// Otherwise, check if it is a core point. If so, seed a new cluster in here. Skip otherwise.
		if (neighbours.at(i).neighbours.size() >= minPts) {
			assign2cluster (neighbours, classlabels, i, currentLabel, minPts);
			currentLabel++;
		}
	}
	
	cout << currentLabel << " clusters were discovered out of " << inputlist.size() << " data points." << endl;
	cout << "\tr= " << searchRadius << endl;
	cout << "\tminPts= " << minPts << endl;
	
	// Every point that is not noise should have been assigned to a cluster at this point, with a corresponding number. Otherwise, -1 = noise. Return this list.
	// Last entry: number of classes detected
	classlabels.push_back(currentLabel);
	return classlabels;
}


