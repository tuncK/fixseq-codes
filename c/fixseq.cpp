// Analysis code for the mismatch repair quantifications.
// Tunc Kayikcioglu


#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include "dbscan.h"
#include "fastq.h"
#include "design.h"
#include "files.h"
#include "os.h"
#include "align.h"

using namespace std;

vector <pair <string,string> > detectFeatures (FASTQfile& fastq, bool analysePairEnd = true) {
	const int numReads = fastq.size();
	if (numReads == 0) {
		cout << "No reads to process." << endl;
		throw(1);
	}

	const int adaptorMatchThreshold = 0.6*leftAdaptorLength; // At least this much of the adaptor should match.
	const int minReadLength = barcodeLength + restrictionOffset + variableSegLength + leftAdaptorLength; // All reads should at least have a length enough for the read and half an adaptor on both sides.
	const int shiftThreshold = 5; // Discrepancy exceeding this will trigger an attempt to detect possible shifts of the sequence of interest

	vector <string> rawreads(2*numReads);
	for (int i=0; i<numReads; i++) {
		rawreads.at(i) = fastq.reads.at(i).rawseq;
		rawreads.at(i+numReads) = fastq.reverseReads.at(i).rawseq;
	}

	int numReadsToAnalyse = numReads;
	if (analysePairEnd)
		numReadsToAnalyse *= 2;
	vector<int> returnFlags (numReadsToAnalyse);
	vector <pair<string,string> > out; // first: barcode, second: variable segment
	out.reserve(numReadsToAnalyse);
	
	int shiftCounter = 0;
	
	
	# pragma omp parallel for schedule (dynamic, 1000)
	for (int i=0; i<numReadsToAnalyse; i++) {
		returnFlags.at(i) = 1;
		string sequence = rawreads.at(i);
		sequence = trimUnknownBases(sequence);
		
		if (sequence.size() < minReadLength) {
			returnFlags.at(i) = 2;
			continue;
		}
		
		pair <int,int> Wscore = semiglobalAlignment(sequence, leftAdaptor);
		int adaptorEndPos = Wscore.first;
		int score = Wscore.second;
		
		if (score < adaptorMatchThreshold || adaptorEndPos > 30 || adaptorEndPos < 15) { // Adaptor not read accurately enough or misaligned too much
			returnFlags.at(i) = 3;
			continue;
		}
		
		int variableSegmentStartPos = adaptorEndPos+barcodeLength+restrictionOffset;
		string barcode = sequence.substr(adaptorEndPos, barcodeLength);
		string detectedVariableSegment = sequence.substr(variableSegmentStartPos, variableSegLength);		

		// Optional scan for shifts of variable segment
		// If a significant deviation is detected, +/- 1 base shifts are tried and saved if a good match.
		// SHOULD BE DISABLED FOR INSERTION/DELETION EXPERIMENTS!
		if (shiftCorrection_ON && HammingDistance(detectedVariableSegment,variableSegment) > shiftThreshold) {
			#pragma omp atomic
			shiftCounter++;
		
			pair<int,int> variableHitParams = biasedAlignment(sequence,variableSegment,variableSegmentStartPos);
			detectedVariableSegment = sequence.substr(variableHitParams.second,variableSegLength);
			if (HammingDistance(detectedVariableSegment,variableSegment) > shiftThreshold) {
				// Observed variable sequence significantly deviates from expectations
				returnFlags.at(i) = 4;
				continue;
			}
		}

		pair <string,string> temp (barcode,detectedVariableSegment);
		#pragma omp critical
		out.push_back(temp);
	}
	
	for (int i=0; i<numReads; i++)
		fastq.reads.at(i).flag = returnFlags.at(i);
	for (int i=numReads; i<numReadsToAnalyse; i++)
		fastq.reverseReads.at(i%2).flag = returnFlags.at(i);
	
	
	cout << "Number of reads suffering from shifts: " << shiftCounter << endl;
	
	out.reserve(out.size());
	return out;
}


// Merge paired reads, to be used for cases that the read length is
// insufficient to cover the wbole region of interest.
// Paired end reads in two separate files
vector <pair <string,string> > detectFeaturesMerge (FASTQfile& fastq) {
	const int numReads = fastq.size();
	if (numReads == 0) {
		cout << "No reads to process." << endl;
		throw(1);
	}

	const int adaptorMatchThreshold = 0.6*leftAdaptorLength; // At least this much of the adaptor should match.
	const int minReadLength = barcodeLength + restrictionOffset + variableSegLength + leftAdaptorLength; // All reads should at least have a length enough for the read and half an adaptor on both sides.
	const int shiftThreshold = 5; // Discrepancy exceeding this will trigger an attempt to detect possible shifts of the sequence of interest

	int numReadsToAnalyse = 2*numReads;
	vector<int> returnFlags (numReadsToAnalyse);
	vector <pair<string,string> > out; // first: barcode, second: variable segment
	out.reserve(numReadsToAnalyse);
	int shiftCounter = 0;
	string rightAdaptorComplement = complementary(rightAdaptor);
	
	# pragma omp parallel for schedule (dynamic, 1000)
	for (int i=0; i<numReadsToAnalyse; i++) {
		returnFlags.at(i) = 1;
		string sequence;
		if (i<numReads)
			sequence = fastq.reads.at(i).rawseq;
		else
			sequence = fastq.reverseReads.at(i%2).rawseq;
		
		sequence = trimUnknownBases(sequence);
		if (sequence.size()<minReadLength) {
			returnFlags.at(i) = 2;
			continue;
		}
		
		int variableSegmentStartPos;
		if (i<numReads) {
			pair <int,int> Wscore = semiglobalAlignment(sequence, leftAdaptor);
			int adaptorEndPos = Wscore.first;
			int score = Wscore.second;
			
			if (score < adaptorMatchThreshold || adaptorEndPos > 30 || adaptorEndPos < 15) { // Adaptor not read accurately enough or misaligned too much
				returnFlags.at(i) = 3;
				continue;
			}
			variableSegmentStartPos = adaptorEndPos+barcodeLength+restrictionOffset;
		}
		else {
			pair <int,int> Wscore = semiglobalAlignment(complementary(sequence), rightAdaptorComplement);
			int adaptorEndPos = Wscore.first;
			int score = Wscore.second;
			
			if (score < adaptorMatchThreshold || adaptorEndPos > 30 || adaptorEndPos < 15) { // Adaptor not read accurately enough or misaligned too much
				returnFlags.at(i) = 3;
				continue;
			}
			variableSegmentStartPos = sequence.size() - (adaptorEndPos+variableSegLength);
		}
		
		string barcode = sequence.substr(variableSegmentStartPos-barcodeLength+1, barcodeLength);
		string detectedVariableSegment = sequence.substr(variableSegmentStartPos, variableSegLength);		

		// Optional scan for shifts of variable segment
		// If a significant deviation is detected, +/- 1 base shifts are tried and saved if a good match.
		// SHOULD BE DISABLED FOR INSERTION/DELETION EXPERIMENTS!
		if (HammingDistance(detectedVariableSegment,variableSegment) > shiftThreshold) {
			#pragma omp atomic
			shiftCounter++;
		
			pair<int,int> variableHitParams = biasedAlignment(sequence,variableSegment,variableSegmentStartPos);
			detectedVariableSegment = sequence.substr(variableHitParams.second,variableSegLength);
			if (HammingDistance(detectedVariableSegment,variableSegment) > shiftThreshold) {
				// Observed variable sequence significantly deviates from expectations
				returnFlags.at(i) = 4;
				continue;
			}
		}

		pair <string,string> temp (barcode,detectedVariableSegment);
		#pragma omp critical
		out.push_back(temp);
	}
	
	for (int i=0; i<numReads; i++)
		fastq.reads.at(i).flag = returnFlags.at(i);
	
	for (int i=numReads; i<numReadsToAnalyse; i++)
		fastq.reverseReads.at(i%2).flag = returnFlags.at(i);
	
	cout << "Number of reads suffering from shifts: " << shiftCounter << endl;
	out.reserve(out.size());
	return out;
}


void filterBarcodes (vector <pair <string,string> >& in, const int sizeTarget) {
	for (int i=0; i<in.size(); i++) {
		if(in.at(i).first.size() != sizeTarget) {
			in.at(i) = in.at(in.size()-1);
			in.pop_back();
			i--;
		}
	}
	return;
}


void processReads (vector <pair <string,string> >& extractedReads, const string path) {
	// (Temporarily) remove barcodes with different length
	filterBarcodes(extractedReads, barcodeLength);
	int numBarcodestoProcess = extractedReads.size();
	cout << numBarcodestoProcess << " barcodes are of correct length." << endl;
	
	// Find clusters using DBscan
	cout << "Starting clustering with DBSCAN..." << endl;
	vector <string> barcodes (numBarcodestoProcess);
	for (int i=0; i<numBarcodestoProcess; i++)
		barcodes.at(i) = extractedReads.at(i).first;

	// pair<vector <int>,int> findClusters (inputlist, minPts, searchRadius, distanceMetric)
	// Choice of metric is of importance, dynamic programing accounting for the shifts might be necessary.
	//vector <int> classLabels = findClusters (barcodes, 10, 3, NeedlemanWunsch);
	vector <int> classLabels = findClusters (barcodes, 10, 3, HammingDistance);
	int numClasses = classLabels.back();
	classLabels.pop_back();
	cout << "Number of barcodes: " << barcodes.size() << endl;
	cout << "Number of clusters: " << numClasses << endl;
	
	vector <vector <string> > groupedBarcodes (numClasses);
	vector <vector <string> > groupedReads (numClasses);
	int nonNoisebarcodes = 0;
	for (int i=0; i<classLabels.size(); i++) {
		int label = classLabels.at(i);
		if (label != -1) {
			groupedBarcodes.at( label ).push_back( extractedReads.at(i).first );
			groupedReads.at( label ).push_back( extractedReads.at(i).second );
			nonNoisebarcodes++;
		}
	}
	cout << classLabels.size()-nonNoisebarcodes << " barcodes were considered noise." << endl;
	
	// Uncomment below to export plain text files listing barcodes and reads individually.
	//exportClusters(groupedBarcodes, path + "barcodes");
	//exportClusters(groupedReads, path + "reads");
	
	// Calculate the histogram within each cluster and export to file
	int numClans = groupedReads.size();
	vector< vector< vector <double> > > allHistograms (numClans);
	
	# pragma omp parallel for schedule (dynamic, 1000)
	for (int i=0; i<numClans; i++) {
		allHistograms.at(i) = baseHistogram( groupedReads.at(i) );
	}
	
	//exportMatrix(allHistograms, path + "hist");
	exportHist_as_MatFile(allHistograms, path.substr(0,path.size()-4) + ".hist");
	return;
}


vector <pair <string,string> > importReads (string filename) {
	// Include the read from the paired end.
	string filenameReverse = filename.substr(0,filename.size()-7) + "2.fastq";

	cout << "Importing " << filename << "..." << endl;
	//FASTQfile fastq (filename);
//	FASTQfile fastq (filename, filenameReverse);
	FASTQfile fastq (filenameReverse, filename); // Use if the sequencing orientation is inverted

	cout << "Splicing features..." << endl;
	vector <pair <string,string> > extractedReads = detectFeatures(fastq, true); // first:barcode, second:variable segment
	cout << extractedReads.size() << " barcodes to process." << endl;
	
	return extractedReads;
}


void processFile (const string filename) {
	int parpos = filename.find_last_of(".")+1;
	string path = filename.substr(0,parpos);
	
	vector <pair <string,string> > extractedReads = importReads(filename);
	processReads (extractedReads, path);
	
	return;
}


void processFile5ML (const string filename) {
	int parpos = filename.find_last_of(".")+1;
	string path = filename.substr(0,parpos);
	
	// 5ML (pentamer context) library contains 13 mixed libraries that need to be analysed individually.
	// Loop over each 13 prototype consecutively.
	vector<string> prototypes (13);
	prototypes.at(0) = "AAAAACAAAAGAAAATAAACCAAACGAAACTAAAGCAAAGGAAAGTAAATCAAATGAAATTAACACAACAGAACATAACCCAAC";
	prototypes.at(1) = "CAACCGAACCTAACGCAACGGAACGTAACTCAACTGAACTTAAGACAAGAGAAGATAAGCCAAGCGAAGCTAAGGCAAGGGAAG";
	prototypes.at(2) = "GAAGGTAAGTCAAGTGAAGTTAATACAATAGAATATAATCCAATCGAATCTAATGCAATGGAATGTAATTCAATTGAATTTACA";
	prototypes.at(3) = "TACACCACACGACACTACAGCACAGGACAGTACATCACATGACATTACCAGACCATACCCCACCCGACCCTACCGCACCGGACC";
	prototypes.at(4) = "GACCGTACCTCACCTGACCTTACGAGACGATACGCCACGCGACGCTACGGCACGGGACGGTACGTCACGTGACGTTACTAGACT";
	prototypes.at(5) = "GACTATACTCCACTCGACTCTACTGCACTGGACTGTACTTCACTTGACTTTAGAGCAGAGGAGAGTAGATCAGATGAGATTAGC";
	prototypes.at(6) = "TAGCATAGCCCAGCCGAGCCTAGCGCAGCGGAGCGTAGCTCAGCTGAGCTTAGGATAGGCCAGGCGAGGCTAGGGCAGGGGAGG";
	prototypes.at(7) = "GAGGGTAGGTCAGGTGAGGTTAGTATAGTCCAGTCGAGTCTAGTGCAGTGGAGTGTAGTTCAGTTGAGTTTATATCATATGATA";
	prototypes.at(8) = "GATATTATCCCATCCGATCCTATCGCATCGGATCGTATCTCATCTGATCTTATGCCATGCGATGCTATGGCATGGGATGGTATG";
	prototypes.at(9) = "TATGTCATGTGATGTTATTCCATTCGATTCTATTGCATTGGATTGTATTTCATTTGATTTTCCCCCGCCCCTCCCGGCCCGTCC";
	prototypes.at(10) = "GTCCCTGCCCTTCCGCGCCGCTCCGGGCCGGTCCGTGCCGTTCCTCGCCTCTCCTGGCCTGTCCTTGCCTTTCGCGGCGCGTCG";
	prototypes.at(11) = "GTCGCTGCGCTTCGGCTCGGGGCGGGTCGGTGCGGTTCGTCTCGTGGCGTGTCGTTGCGTTTCTCTGCTCTTCTGGGCTGGTCT";
	prototypes.at(12) = "GTCTGTGCTGTTCTTGGCTTGTCTTTGCTTTTGGGGGTGGGTTGGTGTGGTTTGTGTTGTTTTTAAAAACAAAAGAAAATAAAC";
	
	// Import the sequencing data, both paired reads simultaneously.
	cout << "Importing " << filename << "..." << endl;
	string filenameReverse = filename.substr(0,filename.size()-7) + "2.fastq";
	FASTQfile fastq (filename, filenameReverse);
	
	
	for (int i=0; i<13; i++) {
		cout << "Splicing features for library 5ML" << i+1 << "..." << endl;
		
		// "+6" accounts for the second barcode
		// wholeReadPrototype = wholeReadPrototype.substr(0,leftAdaptorLength + barcodeLength + restrictionOffset + 6) + prototypes.at(i) + wholeReadPrototype.substr(wholeReadPrototype.size()-rightAdaptorLength, rightAdaptorLength);
		
		vector <pair <string,string> > extractedReads = detectFeaturesMerge(fastq); // first:barcode, second:variable segment
		cout << extractedReads.size() << " barcodes to process." << endl;
		//processReads (extractedReads, path.substr(0,path.size()-4) + "5ML" + to_string(i) + "temp" );
	}
	
	return;
}


// g++ fixseq.cpp -fopenmp -O3 && ./a.out <folder name>
int main (int argc, const char** argv) {
	if (argc < 2) {
		cout << "No directory to process provided." << endl;
		return -1;
	}
	string dirname(argv[1]);
	cout << "Folder to process: " << dirname << endl;
	
	try {
		vector <string> dirlist = listDirectory(dirname);
		dirlist = filterDirectoryList(dirlist, ".fastq");
		for (int i=0; i<dirlist.size(); i+=2) {
			string filename = dirlist.at(i);
			cout << "Processing " << filename << endl;
		
			double startTime = getCurrentTimeStamp();
			// -------------------------------------------------
			
			processFile(filename);
//			processFile5ML(filename);
			
			// -------------------------------------------------
			double endTime = getCurrentTimeStamp();
			printElapsedTime (startTime, endTime, filename);
		}
		cout << "All tasks completed." << endl;	
	}
	
	catch (const char* err) {
		cout << err << endl;
	}
	
	return 0;
}


