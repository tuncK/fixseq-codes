%%
% Plots a clan size histogram

sizes = [];
for i=[1:3 5:7 9:11 13:15 ]
	%filename = sprintf('~/consolidated 3ML1 hists/sample%d.hist', i)
	%filename = sprintf('~/consolidated 5MLx hists/sample%d.hist', i)
	filename = sprintf('~/SSLhists/%d.hist', i)
	
	load(filename); % load mat file with clan histograms
	tempsizes = squeeze(sum(hists(:,1,:),1));
	sizes = [sizes; tempsizes];
end

bins = 1:10:(100*mean(sizes));
pdf = histc(sizes,bins) / length(sizes);
cdf = cumsum(pdf);
endpoint = find(cdf>0.99)(1);

% Find the point at which cdf=50%, i.e. the median size
cdf50 = interp1(cdf,bins,0.5);


close all;
set(0, 'defaultaxesfontsize', 16);
[ax, h1, h2] = plotyy(bins(1:endpoint), pdf(1:endpoint), bins(1:endpoint), cdf(1:endpoint) );
set(gcf, 'currentaxes', ax(2));
hold on;
line([cdf50 bins(endpoint)], [0.5 0.5], 'color', 'k', 'linestyle',  '--');
line([cdf50 cdf50], [0 0.5], 'color', 'k', 'linestyle',  '--');
hold off;
text(cdf50+10, 0.4, sprintf("CDF_{50}=%d",round(cdf50)), 'fontsize', 15);

set([h1 h2], 'linewidth', 2);
%set(gca, 'xtick', 0:(bins(2)-bins(1))*10:bins(end));
xlabel('Clan size');

hleg = legend(ax(1),'PDF', 'CDF');
legend boxoff
legend('location', 'southeast')
lchildren = get(hleg, 'children');
set(lchildren(3), "color", get(lchildren(1),'color') );
set(lchildren(4), "color", get(lchildren(2),'color') );

print('~/clansizedist.png', '-r300');


