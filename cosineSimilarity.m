%%

pkg load statistics;

vectors = [
	% 5ML13 pos 62
	0.820982, 0.721090, 0.170819, NaN, 0.942263, NaN, 0.889290, 0.885511, 0.890705, NaN, 0.919065, 0.912070, 0.948286, NaN, 0.949688, 0.930548, 0.834752, NaN, 0.951348, 0.859297, 0.902047, NaN, 0.958865, 0.620330, NaN, 0.753545, 0.587974, 0.955628;

	% SSL1 pos 16
	0.763349, 0.735643, 0.197506, NaN, 0.793309, NaN, 0.766709, 0.544583, 0.799630, NaN, 0.755759, 0.666367, 0.859570, NaN, 0.704447, 0.713011, 0.837093, NaN, 0.747337, 0.586881, 0.776835, NaN, 0.740204, 0.342250, NaN, 0.513738, 0.187430, 0.634705;

	% SSL3 36 vs 63;
	0.784374, 0.672449, 0.132125, NaN, 0.783299, NaN, 0.805111, 0.364378, 0.433463, NaN, 0.362630, 0.274614, 0.722193, NaN, 0.436997, 0.446216, 0.510779, NaN, 0.375082, 0.335567, 0.387777, NaN, 0.639882, 0.213061, NaN, 0.365002, 0.203263, 0.859054;

	0.846882, 0.429038, 0.245036, NaN, 0.785844, NaN, 0.876973, 0.386050, 0.829707, NaN, 0.508745, 0.469647, 0.839256, NaN, 0.818674, 0.671165, 0.887281, NaN, 0.747780, 0.599548, 0.775230, NaN, 0.840865, 0.265278, NaN, 0.807078, 0.826578, 0.834496;

	% SSL4 pos 48 and 68;
	0.822512, 0.725712, 0.238392, NaN, 0.756214, NaN, 0.758775, 0.491170, 0.788965, NaN, 0.651807, 0.619951, 0.777728, NaN, 0.769052, 0.675840, 0.787630, NaN, 0.728697, 0.727167, 0.798939, NaN, 0.823483, 0.349626, NaN, 0.773176, 0.316761, 0.817643; 

	0.810070, 0.823344, 0.184968, NaN, 0.833634, NaN, 0.805690, 0.546128, 0.799625, NaN, 0.723286, 0.671551, 0.779397, NaN, 0.768307, 0.774141, 0.784800, NaN, 0.794225, 0.745072, 0.786290, NaN, 0.824125, 0.503396, NaN, 0.546711, 0.243034, 0.770973;
];


labels = { 'AT', 'CC', 'AA', 'GG', 'TT', 'CC' };

middle3 = zeros(size(vectors,1),3);
for i=1:size(vectors,1)
	middle3(i,:) = reshape(vectors(i,:),4,7)([1 3 4],4);
end
vectors = middle3;
meanLevel = mean(vectors,1);
vectors -= meanLevel;

cossim = zeros (size(vectors,1), size(vectors,1));
scalarProd = zeros (size(vectors,1), size(vectors,1));
for i=1:size(vectors,1)
	x = vectors(i,:)';
	for j=1:size(vectors,1)
		y = vectors(j,:)';
		cossim(i,j) = (x'*y) / sqrt( (x'*x) * (y'*y) );
		scalarProd(i,j) = sqrt( (x'*x) * (y'*y) );
	end
end

theta = round(real(acos(cossim))*180/pi); % angle in degrees

% Plot a table showing cosine similarity
set(0, 'defaultaxesfontsize', 30)
imagesc(theta);
colormap (gray);
set(gca,'YDir','normal');

% Superimpose matrix entries on the plot
[xgrid,ygrid] = meshgrid(1:size(cossim,1),1:size(cossim,2));
xgrid = reshape(xgrid,1,[]);
ygrid = reshape(ygrid,1,[]);
texts = strtrim(cellstr(num2str(reshape(theta,[],1))));
text(xgrid, ygrid, texts, 'color', 'r', 'HorizontalAlignment','center','VerticalAlignment','middle', 'fontsize', 30, 'FontWeight', 'bold');
	
xlabel('4th nearest neighbors');
ylabel('4th nearest neighbors');
set(gca, 'xtick', 1:length(labels))
set(gca, 'ytick', 1:length(labels))
set(gca, 'xticklabel', labels);
set(gca, 'yticklabel', labels);
print('~/cosineSimilarity.png', '-r300');


randomVectors = [
	0.904717, NaN, 0.968679, 0.875204, 
	0.936104, NaN, 0.941272, 0.936527, 
	0.947145, NaN, 0.957963, 0.847058, 
	0.963530, NaN, 0.959250, 0.939617, 
	0.950596, NaN, 0.962851, 0.919628, 
	0.896060, NaN, 0.899640, 0.925894, 
	0.943754, NaN, 0.888354, 0.922349, 
	0.936104, NaN, 0.898432, 0.934800, 
	0.973040, NaN, 0.944277, 0.893132, 
	0.919729, NaN, 0.903188, 0.954876, 
	0.919324, NaN, 0.903675, 0.897704, 
	0.944917, NaN, 0.877124, 0.742525, 
	0.944917, NaN, 0.926132, 0.939948, 
	0.882072, NaN, 0.944917, 0.950849, 
	0.904304, NaN, 0.892200, 0.947411, 
	0.892552, NaN, 0.958830, 0.917731, 
	0.793279, NaN, 0.935531, 0.372453, 
	0.927937, NaN, 0.826569, 0.425865, 
	0.926725, NaN, 0.953921, 0.847867, 
	0.975425, NaN, 0.960395, 0.388427, 
	0.864167, NaN, 0.919880, 0.814140, 
	0.833605, NaN, 0.898655, 0.522429, 
	0.905105, NaN, 0.908115, 0.886408, 
	0.883572, NaN, 0.896686, 0.884327, 
	0.895121, NaN, 0.988829, 0.680521, 
	0.906036, NaN, 0.932418, 0.841480, 
	0.929527, NaN, 0.896273, 0.458511, 
	0.942263, NaN, 0.889290, 0.885511, 
	0.890705, NaN, 0.919065, 0.912070, 
	0.948286, NaN, 0.949688, 0.930548, 
	0.834752, NaN, 0.951348, 0.859297, 
	0.902047, NaN, 0.958865, 0.620330
];

randomVectors = randomVectors(:,[1 3 4]);
randomVectors -= mean(randomVectors,1); %meanLevel;

cossim = zeros (size(randomVectors,1), size(randomVectors,1));
scalarProdRand = zeros (size(randomVectors,1), size(randomVectors,1));
for i=1:size(randomVectors,1)
	x = randomVectors(i,:)';
	for j=1:size(randomVectors,1)
		y = randomVectors(j,:)';
		cossim(i,j) = (x'*y) / sqrt( (x'*x) * (y'*y) );
		scalarProdRand(i,j) = sqrt( (x'*x) * (y'*y) );
	end
end

theta = round(real(acos(cossim))*180/pi); % angle in degrees
for i=1:1:size(theta,1)
	theta(i,i) = nan;
end
nanmean(reshape(theta,1,[]))
nanstd(reshape(theta,1,[]),1)/sqrt(32*31)

allProducts = reshape(scalarProdRand,1,[]);
bins = 0:1e-3:max(allProducts);
histcounts = histc(allProducts,bins);
pvalues = cumsum(histcounts);
pvalues = 1- pvalues/pvalues(end);

plot(bins, pvalues, 'linewidth', 2);
xlabel('||A||\cdot||B||');
ylabel('p-value')
print('~/cosSim-pvalue.png', '-r300');


pvalInterp = interp1(bins, pvalues, reshape(scalarProd,1,[]), 'linear', 0 );
pval2plot = reshape(pvalInterp,6,6);
imagesc(pval2plot);
colormap (gray);
caxis([0 0.2]);
set(gca,'YDir','normal');

xlabel('4th nearest neighbors');
ylabel('4th nearest neighbors');
set(gca, 'xtick', 1:length(labels))
set(gca, 'ytick', 1:length(labels))
set(gca, 'xticklabel', labels);
set(gca, 'yticklabel', labels);
colorbar('location', 'northoutside');
print('~/cosSim-pvaluesMatrix.png', '-r300');


errorMatrix = [
	0.058, 0.038, 0.092;	
	0.016, 0.023, 0.090;
	0.148, 0.014, 0.037;
	0.033, 0.015, 0.079;
	0.087, 0.097, 0.178;
	0.016, 0.023, 0.090;
];
dr = diag( sqrt( vectors.^2 * errorMatrix'.^2 ) ./ sqrt( vectors*vectors' ) )
diag( sqrt( vectors*vectors' ) )


