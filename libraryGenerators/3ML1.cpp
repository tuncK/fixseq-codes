// Generate and verify a library for a DNA pool to form 3ML1

#include <iostream>
#include <vector>
#include <algorithm>
#include <math.h>
#include <fstream>

using namespace std;


// Counts the basewise difference between two sequences. Lower scores are better.
double HammingDistance (const string& a, const string& b) {
	int size = a.size();
	if (b.size() != size)
		throw("Error: string lengths must match.");
		
	int dist = 0;
	for (int i=0; i<size; i++) {
		if (a[i] != b[i])
			dist++;
	}
	
	return dist;
}


char incrementBase (const char x) {
	switch (x) {
		case 'A':
		case 'a':
			return 'C';
		case 'C':
		case 'c':
			return 'G';
		case 'G':
		case 'g':
			return 'T';
		case 'T':
		case 't':
			return 'A';
		default:
			cout << "Impossible base \"" << x << "\"" << endl;
			throw(1);
	}
}


string incrementBarcode (string in) {
	for (int i=in.size()-1; i<in.size(); i--) {
		switch (in[i]) {
			case 'A':
				in[i] = 'C';
				return in;
			case 'C':
				in[i] = 'G';
				return in;
			case 'G':
				in[i] = 'T';
				return in;
			case 'T':
				in[i] = 'A';
				break;
			default:
				cout << "Impossible base" << endl;
				throw(1);
		}	
	}
	
	return in;
}


// Generate  a list of acceptable barcodes such that all members are sufficiently far apart from each other.
vector <string> possibleBarcodes (int numberBases, int distanceThreshold) {
	vector <string> out;
	int numPossibleBarcodes = pow(4, numberBases);
	string barcode (numberBases, 'T');
	
	for (int i=0; i<numPossibleBarcodes; i++) {
		barcode = incrementBarcode(barcode);
		
		// Check the distance constraint
		bool hitFlag = false;
		# pragma omp parallel for schedule (dynamic, 100)
		for (int j=0; j<out.size(); j++) {
			int distance = HammingDistance (barcode, out[j]);
			if (distance < distanceThreshold) {
				hitFlag = true;
				j = out.size();
			}
		}
		
		if (!hitFlag)
			out.push_back(barcode);
	}
	
	return out;
}



int evaluate (const vector <string>& inlibrary, const string& assembly) {
	for (int i=0; i<inlibrary.size(); i++) {
		int fpos = assembly.find(inlibrary[i]);
		if (fpos == -1)
			return i;
	}
	return -1;	
}



// https://en.wikipedia.org/wiki/Lyndon_word
// List of Lyndon sequences up to length n
vector <string> lydonWordGenerator (int n) {
	vector <string> out;
	out.push_back("A");
	
	while (true) {
		string w = out.back();
		int wsize = w.size();
		string x (n, -1);
		for (int j=0; j<n; j++)
			x.at(j) = w.at(j%wsize);
		
		// Remove all elements that are the last symbol of the alphabet
		for (int j=x.size()-1; j>=0; j--) {
			if (x.at(j) == 'T')
				x.pop_back();
			else
				break;
		}
		
		if (x == "")
			break;
		else {
			x.back() = incrementBase(x.back());
			out.push_back(x);
		}
	}
	
	return out;
}



// Concatenate lyndon words up to length n
string deBruijnGenerator (int n) {
	vector <string> lyndonWords = lydonWordGenerator(n);
	sort(lyndonWords.begin(), lyndonWords.end());
	
	string out;
	for (int i=0; i<lyndonWords.size(); i++) {
		if (n%lyndonWords.at(i).size() == 0)
			out += lyndonWords.at(i);
	}
	
	return out;
}


// Any permutation of characters is still a de Bruijn sequence
// Do permutations to obtain multiple de Bruijn sequences
string generateConsensusSequence (int patternLength, int numSequences) {
	string deBruijn = deBruijnGenerator(patternLength);
	string out = deBruijn;
	
	deBruijn = deBruijn + deBruijn.substr(0, patternLength-1);
	replace( deBruijn.begin(), deBruijn.end(), 'A', 'a');
	replace( deBruijn.begin(), deBruijn.end(), 'C', 'c');
	replace( deBruijn.begin(), deBruijn.end(), 'G', 'g');
	replace( deBruijn.begin(), deBruijn.end(), 'T', 't');
	
	vector <string> inlibrary = possibleBarcodes(patternLength,0);
		
	vector <char> bases(4);
	bases.at(0) = 'A';
	bases.at(1) = 'C';
	bases.at(2) = 'G';
	bases.at(3) = 'T';
	
	for (int i=0; i<numSequences && i<24; i++) {
		string out = deBruijn;
		random_shuffle(bases.begin(), bases.end());
		replace( out.begin(), out.end(), 'a', bases.at(0));
		replace( out.begin(), out.end(), 'c', bases.at(1));
		replace( out.begin(), out.end(), 'g', bases.at(2));
		replace( out.begin(), out.end(), 't', bases.at(3));
		
		cout << out << endl;		
		if (evaluate (inlibrary, out ) != -1) {
			cout << "error: not de Bruijn" << endl;
		}		
	}
	
	return out;
}



void generateSingleLibrary (void) {
	// Run parameters
	ofstream outfile ("./library.txt");
	string leftAdaptor = "TTCTAGACGCTCCTTGTTGTACTCGCA"; // XbaI + SacI
	string rightAdaptor = "ATACCGGCGGTTGTTCAACC"; // XhoI
	string RE1 = "GAGCTC";
	string RE2 = "CTCGAG";
	string RE3 = "TCTAGA";
	string consensus = "TTTATTCTTGTAATACTAGTCATCCTCGTGATGCTGGAAACAAGACCACGAGCAGGCCCGCGGGTT";
	
	// Generate list of barcodes
	int barcodeLength = 7;
	vector <string> barcodes = possibleBarcodes(barcodeLength,2);
	random_shuffle(barcodes.begin(), barcodes.end());
	cout << barcodes.size() << endl;
	
	int RE1StartPos = leftAdaptor.size();
	int leftOffset = leftAdaptor.size() + RE1.size() + barcodeLength;
	int RE2StartPos = leftOffset + consensus.size();
	int mismatchIndex = 0;
	
	int discardCount = 0;
	
	for (int i=0; i<barcodes.size(); i++) {
		string oligo = leftAdaptor + RE1 + barcodes.at(i) + consensus + RE2 + rightAdaptor;
		
		// Quality check: avoid additional cut sites
		if (oligo.find(RE1) == oligo.rfind(RE1) && oligo.find(RE2) == oligo.rfind(RE2) && oligo.find(RE3) == oligo.rfind(RE3) ) {
			// Passed the test.
			int subsPos = (mismatchIndex/3)%consensus.size() + leftOffset;
			int subsType = mismatchIndex%3;
			
			char newBase = oligo.at(subsPos);
			for (int i=0; i<=subsType; i++)
				newBase = incrementBase(newBase);
			 
			oligo.at(subsPos) = newBase;
			outfile << oligo << endl;
			mismatchIndex++;
		}
		// Else, test failed, ignore this barcode
		else
			discardCount++;
	}

	cout << "Number of discarded sequences due to extraneous cut sites was " << discardCount << endl;
	
	outfile.close();
	return;
}



int main () {
	generateSingleLibrary();
	return 0;
}



