// Generate and verify a library for a DNA pool for 13 sublibraries that comprise 5MLx


#include <iostream>
#include <vector>
#include <algorithm>
#include <math.h>
#include <fstream>
#include <map>
#include <set>

using namespace std;


// Counts the basewise difference between two sequences. Lower scores are better.
double HammingDistance (const string& a, const string& b) {
	int size = a.size();
	if (b.size() != size)
		throw("Error: string lengths must match.");
		
	int dist = 0;
	for (int i=0; i<size; i++) {
		if (a[i] != b[i])
			dist++;
	}
	
	return dist;
}


char incrementBase (const char x) {
	switch (x) {
		case 'A':
		case 'a':
			return 'C';
		case 'C':
		case 'c':
			return 'G';
		case 'G':
		case 'g':
			return 'T';
		case 'T':
		case 't':
			return 'A';
		default:
			cout << "Impossible base \"" << x << "\"" << endl;
			throw(1);
	}
}



string incrementBarcode (string in) {
	for (int i=in.size()-1; i<in.size(); i--) {
		switch (in[i]) {
			case 'A':
				in[i] = 'C';
				return in;
			case 'C':
				in[i] = 'G';
				return in;
			case 'G':
				in[i] = 'T';
				return in;
			case 'T':
				in[i] = 'A';
				break;
			default:
				cout << "Impossible base" << endl;
				throw(1);
		}	
	}
	
	return in;
}


// Generate list of acceptable barcodes such that all members are sufficiently far apart from each other.
vector <string> possibleBarcodes (int numberBases, int distanceThreshold) {
	vector <string> out;
	int numPossibleBarcodes = pow(4, numberBases);
	string barcode (numberBases, 'T');
	
	for (int i=0; i<numPossibleBarcodes; i++) {
		barcode = incrementBarcode(barcode);
		
		// Check the distance constraint
		bool hitFlag = false;
		# pragma omp parallel for schedule (dynamic, 100)
		for (int j=0; j<out.size(); j++) {
			int distance = HammingDistance (barcode, out[j]);
			if (distance < distanceThreshold) {
				hitFlag = true;
				j = out.size();
			}
		}
		
		if (!hitFlag)
			out.push_back(barcode);
	}
	
	return out;
}



int evaluate (const vector <string>& inlibrary, const string& assembly) {
	for (int i=0; i<inlibrary.size(); i++) {
		int fpos = assembly.find(inlibrary[i]);
		if (fpos == -1)
			return i;
	}
	return -1;	
}



// https://en.wikipedia.org/wiki/Lyndon_word
// List of Lyndon sequences up to length n
vector <string> lydonWordGenerator (int n) {
	vector <string> out;
	out.push_back("A");
	
	while (true) {
		string w = out.back();
		int wsize = w.size();
		string x (n, -1);
		for (int j=0; j<n; j++)
			x.at(j) = w.at(j%wsize);
		
		// Remove all elements that are the last symbol of the alphabet from the end
		for (int j=x.size()-1; j>=0; j--) {
			if (x.at(j) == 'T')
				x.pop_back();
			else
				break;
		}
		
		if (x == "")
			break;
		else {
			x.back() = incrementBase(x.back());
			out.push_back(x);
		}
	}
	
	return out;
}



// Concatenate lyndon words up to length n
string deBruijnGenerator (int n) {
	vector <string> lyndonWords = lydonWordGenerator(n);
	sort(lyndonWords.begin(), lyndonWords.end());
	
	string out;
	for (int i=0; i<lyndonWords.size(); i++) {
		if (n%lyndonWords.at(i).size() == 0)
			out += lyndonWords.at(i);
	}
	
	return out;
}



// Any permutation of characters is still a de Bruijn sequence
// Do permutations to obtain multiple de Bruijn sequences
string generateConsensusSequence (int patternLength, int numSequences) {
	string deBruijn = deBruijnGenerator(patternLength);
	string out = deBruijn;
	
	deBruijn = deBruijn + deBruijn.substr(0, patternLength-1);
	replace( deBruijn.begin(), deBruijn.end(), 'A', 'a');
	replace( deBruijn.begin(), deBruijn.end(), 'C', 'c');
	replace( deBruijn.begin(), deBruijn.end(), 'G', 'g');
	replace( deBruijn.begin(), deBruijn.end(), 'T', 't');
	
	vector <string> inlibrary = possibleBarcodes(patternLength,0);
		
	vector <char> bases(4);
	bases.at(0) = 'A';
	bases.at(1) = 'C';
	bases.at(2) = 'G';
	bases.at(3) = 'T';
	
	for (int i=0; i<numSequences && i<24; i++) {
		string out = deBruijn;
		random_shuffle(bases.begin(), bases.end());
		replace( out.begin(), out.end(), 'a', bases.at(0));
		replace( out.begin(), out.end(), 'c', bases.at(1));
		replace( out.begin(), out.end(), 'g', bases.at(2));
		replace( out.begin(), out.end(), 't', bases.at(3));
		
		cout << out << endl;		
		if (evaluate (inlibrary, out ) != -1) {
			cout << "Error: not de Bruijn" << endl;
		}		
	}
	
	return out;
}



void generateSingleLibrary (const string consensus, const string leftAdaptor, const string rightAdaptor, const string filename) {
	// Run parameters
	ofstream outfile (filename.c_str());
	string RE1 = "GAGCTC";
	string RE2 = "CTCGAG";
	
	// Generate list of barcodes
	int barcodeLength = 6;
	int barcode2barcodeDist = 2;
	vector <string> barcodes = possibleBarcodes(barcodeLength,barcode2barcodeDist);
	random_shuffle(barcodes.begin(), barcodes.end());
	cout << barcodes.size() << endl;
	
	int RE1StartPos = leftAdaptor.size();
	int leftOffset = leftAdaptor.size() + RE1.size() + barcodeLength;
	int RE2StartPos = leftOffset + consensus.size();
	int mismatchIndex = 0;
	
	int discardCount = 0;
	
	for (int i=0; i<barcodes.size() && mismatchIndex < 9*consensus.size(); i++) {
		string oligo = leftAdaptor + RE1 + barcodes.at(i) + consensus + RE2 + rightAdaptor;
		
		// Quality check: avoid additional cut sites
		if (oligo.find(RE1) == oligo.rfind(RE1) && oligo.find(RE2) == oligo.rfind(RE2) ) {
			// Passed the test.
			int subsPos = (mismatchIndex/3)%consensus.size() + leftOffset;
			int subsType = mismatchIndex%3;
			
			char newBase = oligo.at(subsPos);
			for (int i=0; i<=subsType; i++)
				newBase = incrementBase(newBase);
			 
			oligo.at(subsPos) = newBase;
			outfile << oligo << endl;
			mismatchIndex++;
		}
		// Else, test failed, ignore this barcode
		else 
			discardCount++;
	}
	
	cout << "Number of discarded sequences due to extraneous cut sites was " << discardCount << endl;
	
	outfile.close();
	return;
}



int main () {
	vector<string> consensus (13);
	consensus.at(0) = "AAAAACAAAAGAAAATAAACCAAACGAAACTAAAGCAAAGGAAAGTAAATCAAATGAAATTAACACAACAGAACATAACCCAAC";
	consensus.at(1) = "CAACCGAACCTAACGCAACGGAACGTAACTCAACTGAACTTAAGACAAGAGAAGATAAGCCAAGCGAAGCTAAGGCAAGGGAAG";
	consensus.at(2) = "GAAGGTAAGTCAAGTGAAGTTAATACAATAGAATATAATCCAATCGAATCTAATGCAATGGAATGTAATTCAATTGAATTTACA";
	consensus.at(3) = "TACACCACACGACACTACAGCACAGGACAGTACATCACATGACATTACCAGACCATACCCCACCCGACCCTACCGCACCGGACC";
	consensus.at(4) = "GACCGTACCTCACCTGACCTTACGAGACGATACGCCACGCGACGCTACGGCACGGGACGGTACGTCACGTGACGTTACTAGACT";	
	consensus.at(5) = "GACTATACTCCACTCGACTCTACTGCACTGGACTGTACTTCACTTGACTTTAGAGCAGAGGAGAGTAGATCAGATGAGATTAGC";
	consensus.at(6) = "TAGCATAGCCCAGCCGAGCCTAGCGCAGCGGAGCGTAGCTCAGCTGAGCTTAGGATAGGCCAGGCGAGGCTAGGGCAGGGGAGG";
	consensus.at(7) = "GAGGGTAGGTCAGGTGAGGTTAGTATAGTCCAGTCGAGTCTAGTGCAGTGGAGTGTAGTTCAGTTGAGTTTATATCATATGATA";
	consensus.at(8) = "GATATTATCCCATCCGATCCTATCGCATCGGATCGTATCTCATCTGATCTTATGCCATGCGATGCTATGGCATGGGATGGTATG";
	consensus.at(9) = "TATGTCATGTGATGTTATTCCATTCGATTCTATTGCATTGGATTGTATTTCATTTGATTTTCCCCCGCCCCTCCCGGCCCGTCC";
	consensus.at(10) = "GTCCCTGCCCTTCCGCGCCGCTCCGGGCCGGTCCGTGCCGTTCCTCGCCTCTCCTGGCCTGTCCTTGCCTTTCGCGGCGCGTCG";
	consensus.at(11) = "GTCGCTGCGCTTCGGCTCGGGGCGGGTCGGTGCGGTTCGTCTCGTGGCGTGTCGTTGCGTTTCTCTGCTCTTCTGGGCTGGTCT";
	consensus.at(12) = "GTCTGTGCTGTTCTTGGCTTGTCTTTGCTTTTGGGGGTGGGTTGGTGTGGTTTGTGTTGTTTTTAAAAACAAAAGAAAATAAAC";
	
	string Z13 = "CGCTCCTTGTTGTACTCGCA";
	string Z14 = "TGACGGAGGATAGAAGGCCA";

	vector<string> rightAdaptor (13);
	rightAdaptor.at(0) = "AAGCTACGATGCGGTCCCAT";
	rightAdaptor.at(1) = "TATCGGGTACCATTCAGCCG";
	rightAdaptor.at(2) = "TGTCTACACCAGCTGCGCTC";
	rightAdaptor.at(3) = "ACGTGCTGAATTCGAAGTCG";
	rightAdaptor.at(4) = "ATAAGGGATCGAGCGGGTGT";
	rightAdaptor.at(5) = "ATACCGGCGGTTGTTCAACC";
	rightAdaptor.at(6) = "GAAATCCGCAACGGCAATTT";
	rightAdaptor.at(7) = rightAdaptor.at(0);
	rightAdaptor.at(8) = rightAdaptor.at(1);
	rightAdaptor.at(9) = rightAdaptor.at(2);
	rightAdaptor.at(10) = rightAdaptor.at(3);
	rightAdaptor.at(11) = rightAdaptor.at(4);
	rightAdaptor.at(12) = rightAdaptor.at(5);
	
	
	for (int i=0; i<consensus.size(); i++) {
		if (i<7)
			generateSingleLibrary(consensus.at(i), Z13, rightAdaptor.at(i), "./5ML/5ML" + to_string(i+1) + ".txt" );
		else
			generateSingleLibrary(consensus.at(i), Z14, rightAdaptor.at(i), "./5ML/5ML" + to_string(i+1) + ".txt" );
	}
	
	return 0;
}



