// Generates 10 new libraries that subsample the highly variable sequence motifs in the dataset.
// Variable motifs are read from motifs2include file and buried in a larger sequence context.

#include <iostream>
#include <vector>
#include <fstream>
#include <map>
#include <set>
#include <math.h>
#include <algorithm>

using namespace std;

#define INF 1e8
#define MAX_SUBLIB_LENGTH 2000

map<int,string> int2baseMap;


vector<string> findAllContexts (const int seqContext2add) {
	vector <string> contexts;
	for (int i=0; i<4; i++) {
		contexts.push_back(int2baseMap[i]);
	}
	
	for (int k=1; k<seqContext2add; k++) {
		vector <string> temp = contexts;
		contexts.clear();
		for (int j=0; j<temp.size(); j++) {
			for (int i=0; i<4; i++) {
				contexts.push_back(int2baseMap[i] + temp.at(j));
			}
		}
	}	
	return contexts;
}



vector<string> importNodes(void) {
	int seqContext2add = 1;
	vector <string> allContexts = findAllContexts(seqContext2add);
	
	set<string> motifsSet;
	ifstream infile;
	infile.open("./motifs2include");
	while (infile.peek() != EOF) {
		string line;
		infile >> line;	
		if (line.size()<2)
			break;
		if (seqContext2add > 0) {
			for (int i=0; i<allContexts.size(); i++) {
				for (int j=0; j<allContexts.size(); j++) {
					string lineWcontext = allContexts[i] + line + allContexts[j];
					motifsSet.insert(lineWcontext);
				}
			}
		}
		else
			motifsSet.insert(line);
	}
	infile.close();
	vector <string> motifs (motifsSet.begin(), motifsSet.end());
	return motifs;
}


// Compute distances by number of bases that cannot be made to overlap.
int shiftDistance (const string& x, const string& y) {
	int shift = 0;
	while ( shift<x.size() ) {
		string subx = x.substr(shift,y.size());
		if ( subx != y.substr(0,subx.size()) )
			shift++;
		else
			break;
	}
	
	// If the total length of the proposed combined oligo is too long, disallow.
	int distance;
	if (shift+(int)y.size() > (MAX_SUBLIB_LENGTH+5) ) 
		distance = MAX_SUBLIB_LENGTH;
	else
		if ( (shift+(int)y.size()-(int)x.size()) >= 0)
			distance = shift + (int)y.size() - (int)x.size();
		else
			distance = 0;
	
	return distance;		
}


// At each iteration, join the two nodes that are separated by the shortest edge.
// The node pairs are chosen independently at each iteration, till all nodes are visited.
// This approach produced a slightly shorter path.
vector<string> tourGraph (vector<string> nodes) {
	int numNodes = nodes.size();
	vector <int> idNext (numNodes,-1);
	vector <int> idPrev (numNodes,-1);
	
	// Store the pre-computed distances on a list to speed-up computation
	vector< set <vector<int> > > distList (MAX_SUBLIB_LENGTH+1);
	for (int i=0; i<numNodes; i++) {
		for (int j=0; j<numNodes; j++) {
			if (i!=j) {
				int tempdist = shiftDistance(nodes[i],nodes[j]);
				vector <int> temppair(2);
				temppair[0] = i;
				temppair[1] = j;
				distList[tempdist].insert(temppair);
			}
		}
	}
	
	while (true) {		
		// Find a smallest distance edge.
		int mindist = 0;
		while(mindist<distList.size() && distList.at(mindist).size()==0)
			mindist++;
		
		// All edges processed, quit.
		// Return the entry on any edge, as they are now all concatenated.
		if (mindist>=MAX_SUBLIB_LENGTH) {
			set<string> fragmentsBuilt;
			for (int j=0; j<numNodes; j++)
				fragmentsBuilt.insert( nodes[j] );
							
			vector <string> outputs (fragmentsBuilt.begin(), fragmentsBuilt.end());
			return outputs;
		}
		
		int imin = distList[mindist].begin()->at(0);
		int jmin = distList[mindist].begin()->at(1);
		
		// Join the nodes that are closest.
		string newnode;
		
		if (mindist>0)
			newnode = nodes[imin] + nodes[jmin].substr((int)nodes[jmin].size()-mindist,INF);
		else
			newnode = nodes[imin];

		cout << nodes[imin] << " " << nodes[jmin] << " " << newnode << " " << imin << " " << jmin << " " << mindist << endl;
		
		// Record the new node information at each node along the selected DNA
		idNext[imin] = jmin;
		int lastEdgeID = imin;
		while (idNext[lastEdgeID]!=-1) {
			lastEdgeID = idNext[lastEdgeID];
			nodes[lastEdgeID] = newnode;
		}
		
		idPrev[jmin] = imin;		
		int firstEdgeID = jmin;
		while (idPrev[firstEdgeID]!=-1) {
			firstEdgeID = idPrev[firstEdgeID];
			nodes[firstEdgeID] = newnode;
		}
		
		// Update all neighbours from the list.
		for (int i=0; i<distList.size(); i++) {
			// Remove the potential edge betwen first and last nodes
			// This avoids forming loops
			vector<int> terminalEdge (2);
			terminalEdge[0]=lastEdgeID;
			terminalEdge[1]=firstEdgeID;
			distList[i].erase(terminalEdge);
			
			// No edges to process at this distance (yet).
			if (distList[i].size() == 0)
				continue;
			
			for (int j=0; j<numNodes; j++) {
				// Remove the outgoing edges from imin
				// Includes removing the current edge
				vector<int> edgeOI (2);
				edgeOI[0]=imin;
				edgeOI[1]=j;
				distList[i].erase(edgeOI);
				
				// Remove all incoming edges to jmin
				edgeOI[0]=j;
				edgeOI[1]=jmin;
				distList[i].erase(edgeOI);
				
				// Update the distances on affected neighbours due to merging
				// Those that are incoming to imin
				int newdistIN = shiftDistance(nodes[j],newnode);
				int newdistOUT = shiftDistance(newnode,nodes[j]);
								
				edgeOI[0]=j;
				edgeOI[1]=imin;
				int isDeleted = distList[i].erase(edgeOI);
				if (isDeleted==1)
					distList[newdistIN].insert(edgeOI);
				
				// Those that are outgoing from jmin
				edgeOI[0]=jmin;
				edgeOI[1]=j;
				isDeleted = distList[i].erase(edgeOI);
				if (isDeleted==1)
					distList[newdistOUT].insert(edgeOI);
				
				// Those that are incoming to firstEdgeID
				edgeOI[0]=j;
				edgeOI[1]=firstEdgeID;
				isDeleted = distList[i].erase(edgeOI);
				if (isDeleted==1)
					distList[newdistIN].insert(edgeOI);
				
				// Those that are outgoing from lastEdgeID
				edgeOI[0]=lastEdgeID;
				edgeOI[1]=j;
				isDeleted = distList[i].erase(edgeOI);
				if (isDeleted==1)
					distList[newdistOUT].insert(edgeOI);
			}
		}

		// If a long enough construct is reached, record it as a sublibrary and remove all relevant edges onto it.
		if (newnode.size() >= MAX_SUBLIB_LENGTH-10) {		
			// Remove the affected neighbours due to merging
			for (int i=0; i<distList.size(); i++) {
				for (int j=0; j<numNodes; j++) {
					vector<int> edgeOI (2);
					edgeOI[0]=j;
					edgeOI[1]=firstEdgeID;
					distList[i].erase(edgeOI);
					
					edgeOI[0]=lastEdgeID;
					edgeOI[1]=j;
					distList[i].erase(edgeOI);
				}
			}
		}
	}
}


// Pick a node, then advance to the nearest node. Iterate till all nodes are visited.
// This produced a slightly longer path
string tourGraph_NearestJump (const vector<string> nodes, const int firstChosenNode=0) {
	vector<string> unvisitedNodes = nodes;
	string travelledSequence = unvisitedNodes.at(firstChosenNode);
	unvisitedNodes.at(firstChosenNode) = unvisitedNodes.back();
	unvisitedNodes.pop_back();
	
	while (unvisitedNodes.size()>0) {
		// Calculate all the distances, and record the smallest one.
		int minIdx = -1;
		int mindist = INF;
		for (int i=0; i<unvisitedNodes.size(); i++) {
			int distance = shiftDistance(travelledSequence,unvisitedNodes[i]);
			if (distance < mindist) {
				mindist = distance;
				minIdx = i;
			}
		}
		
		if (mindist > 0)
			travelledSequence = travelledSequence + unvisitedNodes[minIdx].substr((int)nodes[minIdx].size()-mindist,INF);
		
		// Mark the node as visited
		unvisitedNodes.at(minIdx) = unvisitedNodes.back();
		unvisitedNodes.pop_back();
	}
	
	return travelledSequence;
}



// Counts the basewise difference between two sequences. Lower scores are better.
double HammingDistance (const string& a, const string& b) {
	int size = a.size();
	if (b.size() != size)
		throw("Error: string lengths must match.");
		
	int dist = 0;
	for (int i=0; i<size; i++) {
		if (a[i] != b[i])
			dist++;
	}
	
	return dist;
}


char incrementBase (const char x) {
	switch (x) {
		case 'A':
		case 'a':
			return 'C';
		case 'C':
		case 'c':
			return 'G';
		case 'G':
		case 'g':
			return 'T';
		case 'T':
		case 't':
			return 'A';
		default:
			cout << "Impossible base \"" << x << "\"" << endl;
			throw(1);
	}
}


string incrementBarcode (string in) {
	for (int i=in.size()-1; i<in.size(); i--) {
		switch (in[i]) {
			case 'A':
				in[i] = 'C';
				return in;
			case 'C':
				in[i] = 'G';
				return in;
			case 'G':
				in[i] = 'T';
				return in;
			case 'T':
				in[i] = 'A';
				break;
			default:
				cout << "Impossible base" << endl;
				throw(1);
		}	
	}
	
	return in;
}


// Generate list of acceptable barcodes such that all members are sufficiently far apart from each other.
vector <string> possibleBarcodes (int numberBases, int distanceThreshold) {
	vector <string> out;
	int numPossibleBarcodes = pow(4, numberBases);
	string barcode (numberBases, 'T');
	
	for (int i=0; i<numPossibleBarcodes; i++) {
		barcode = incrementBarcode(barcode);
		
		// Check the distance constraint
		bool hitFlag = false;
		# pragma omp parallel for schedule (dynamic, 100)
		for (int j=0; j<out.size(); j++) {
			int distance = HammingDistance (barcode, out[j]);
			if (distance < distanceThreshold) {
				hitFlag = true;
				j = out.size();
			}
		}
		
		if (!hitFlag)
			out.push_back(barcode);
	}
	
	return out;
}


void generateSingleLibrary (const string consensus, const string leftAdaptor, const string rightAdaptor, const string filename) {
	// Run parameters
	ofstream outfile (filename.c_str());
	string RE1 = "GAGCTC";
	string RE2 = "CTCGAG";
	
	// Generate list of barcodes
	int barcodeLength = 7;
	int barcode2barcodeDist = 2;
	vector <string> barcodes = possibleBarcodes(barcodeLength,barcode2barcodeDist);
	random_shuffle(barcodes.begin(), barcodes.end());
	cout << barcodes.size() << endl;
	
	int RE1StartPos = leftAdaptor.size();
	int leftOffset = leftAdaptor.size() + RE1.size() + barcodeLength;
	int RE2StartPos = leftOffset + consensus.size();
	int mismatchIndex = 0;
	
	int discardCount = 0;
	
	for (int i=0; i<barcodes.size() && mismatchIndex < 9*consensus.size(); i++) {
		string oligo = leftAdaptor + RE1 + barcodes.at(i) + consensus + RE2 + rightAdaptor;
		
		// Quality check: avoid additional cut sites
		if (oligo.find(RE1) == oligo.rfind(RE1) && oligo.find(RE2) == oligo.rfind(RE2) ) {
			// Passed the test.
			int subsPos = (mismatchIndex/3)%consensus.size() + leftOffset;
			int subsType = mismatchIndex%3;
			
			char newBase = oligo.at(subsPos);
			for (int i=0; i<=subsType; i++)
				newBase = incrementBase(newBase);
			 
			oligo.at(subsPos) = newBase;
			outfile << oligo << endl;
			mismatchIndex++;
		}
		// Else, test failed, ignore this barcode
		else 
			discardCount++;
	}
	
	cout << "Number of discarded sequences due to extraneous cut sites was " << discardCount << endl;
	
	outfile.close();
	return;
}


void splitIntoSublibraries (const string consensus) {
	string Z13 = "CGCTCCTTGTTGTACTCGCA";
	string Z14 = "TGACGGAGGATAGAAGGCCA";
	
	vector<string> rightAdaptor (7);
	rightAdaptor.at(0) = "AAGCTACGATGCGGTCCCAT";
	rightAdaptor.at(1) = "TATCGGGTACCATTCAGCCG";
	rightAdaptor.at(2) = "TGTCTACACCAGCTGCGCTC";
	rightAdaptor.at(3) = "ACGTGCTGAATTCGAAGTCG";
	rightAdaptor.at(4) = "ATAAGGGATCGAGCGGGTGT";
	rightAdaptor.at(5) = "ATACCGGCGGTTGTTCAACC";
	rightAdaptor.at(6) = "GAAATCCGCAACGGCAATTT";
	
	int sublibIndex = 0;
	for (int pos=0; pos<consensus.size(); pos+=64) {
		string sublibPrototype = (consensus+consensus).substr(pos,80);
		cout << "SSL" << sublibIndex + 1 << endl;
		cout << sublibPrototype << endl;
		cout << endl;
		
		if (sublibIndex<7)
			generateSingleLibrary(sublibPrototype, Z13, rightAdaptor.at(sublibIndex), "./SSL" + to_string(sublibIndex+1) + ".txt" );
		else
			generateSingleLibrary(sublibPrototype, Z14, rightAdaptor.at(sublibIndex-7), "./SSL" + to_string(sublibIndex+1) + ".txt" );
	
		sublibIndex++;
	}
	
	return;
}


bool verifyContents(const string sequence, const vector<string> motifs) {
	for (int i=0; i<motifs.size(); i++) {
		int fpos = sequence.find(motifs[i]);
		if (fpos == -1)
			return false;
	}
	return true;
}


int main(int argc, char *argv[]) {
	int2baseMap[0] = "A";
	int2baseMap[1] = "T";
	int2baseMap[2] = "C";
	int2baseMap[3] = "G";
	
	// Distance matrix
	const vector<string> motifs = importNodes();
	const vector<string> NNapprox = tourGraph(motifs);
	
	/*vector<string> NNapprox;
	for (int i=0; i<motifs.size(); i++) {
		NNapprox.push_back( tourGraph_NearestJump (motifs, i) );
	}*/
	
	cout << "#########################################" << endl;
	int totalLength = 0;
	for (int i=0; i<NNapprox.size(); i++) {
		cout << NNapprox[i] << endl;
		totalLength += NNapprox[i].size();
		
		if (verifyContents(NNapprox.at(i), motifs))
			cout << "Indeed " << NNapprox[i].size() << " contains all desired motifs." << endl;
		else
			cout << "Something is wrong, some motifs are missing." << endl;		
	}
	cout << "Shortest DNA containing these motifs is " << totalLength << " bases long." << endl;
	
	// Now generate a list of oligos to be purchased as part of the pool.
	splitIntoSublibraries(NNapprox.at(0));
	
	return 0;
}


