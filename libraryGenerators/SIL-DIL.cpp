// Generate and verify a library for a DNA pool to generate insertion libraries SIL and DIL


#include <iostream>
#include <vector>
#include <algorithm>
#include <math.h>
#include <fstream>
#include <map>
#include <set>

using namespace std;


// Checks in a w-base long substring of a de Bruijn sequence of degree n, how many motifs of degree m (m>n) exists.
vector <int> deBruijnChecker (const string dbn, const int m, const int w) {
	vector <int> density (dbn.size());
	string superstring = dbn + dbn + dbn;
	
	for (int i=0; i<dbn.size(); i++) {
		set <string> tree;
		for (int j=0; j<w; j++) {
			string chunk = superstring.substr(i+j,m);
			tree.insert(chunk);
		}
		density.at(i) = tree.size();
	}
	
	exportList(density, "/home/tk/rho.txt");	
	return density;
}


// Counts the basewise difference between two sequences. Lower scores are better.
double HammingDistance (const string& a, const string& b) {
	int size = a.size();
	if (b.size() != size)
		throw("Error: string lengths must match.");
		
	int dist = 0;
	for (int i=0; i<size; i++) {
		if (a[i] != b[i])
			dist++;
	}
	
	return dist;
}


char incrementBase (const char x) {
	switch (x) {
		case 'A':
		case 'a':
			return 'C';
		case 'C':
		case 'c':
			return 'G';
		case 'G':
		case 'g':
			return 'T';
		case 'T':
		case 't':
			return 'A';
		default:
			cout << "Impossible base \"" << x << "\"" << endl;
			throw(1);
	}
}




string incrementBarcode (string in) {
	for (int i=in.size()-1; i<in.size(); i--) {
		switch (in[i]) {
			case 'A':
				in[i] = 'C';
				return in;
			case 'C':
				in[i] = 'G';
				return in;
			case 'G':
				in[i] = 'T';
				return in;
			case 'T':
				in[i] = 'A';
				break;
			default:
				cout << "Impossible base" << endl;
				throw(1);
		}	
	}
	
	return in;
}


// Generate list of acceptable barcodes such that all members are sufficiently far apart from each other.
vector <string> possibleBarcodes (int numberBases, int distanceThreshold) {
	vector <string> out;
	int numPossibleBarcodes = pow(4, numberBases);
	string barcode (numberBases, 'T');
	
	for (int i=0; i<numPossibleBarcodes; i++) {
		barcode = incrementBarcode(barcode);
		
		// Check the distance constraint
		bool hitFlag = false;
		# pragma omp parallel for schedule (dynamic, 100)
		for (int j=0; j<out.size(); j++) {
			int distance = HammingDistance (barcode, out[j]);
			if (distance < distanceThreshold) {
				hitFlag = true;
				j = out.size();
			}
		}
		
		if (!hitFlag) {
			out.push_back(barcode);
		}
	}
	
	if (false) {
		cout << out.size() << endl;
		for (int i=0; i<out.size(); i++) 
			cout << out.at(i) << endl;
	}
	
	return out;
}



int evaluate (const vector <string>& inlibrary, const string& assembly) {
	for (int i=0; i<inlibrary.size(); i++) {
		int fpos = assembly.find(inlibrary[i]);
		if (fpos == -1)
			return i;
	}
	return -1;	
}



// https://en.wikipedia.org/wiki/Lyndon_word
// List of Lyndon sequences up to length n
vector <string> lydonWordGenerator (int n) {
	vector <string> out;
	out.push_back("A");
	
	while (true) {
		string w = out.back();
		int wsize = w.size();
		string x (n, -1);
		for (int j=0; j<n; j++)
			x.at(j) = w.at(j%wsize);
		
		// Remove all elements that are the last symbol of the alphabet
		for (int j=x.size()-1; j>=0; j--) {
			if (x.at(j) == 'T')
				x.pop_back();
			else
				break;
		}
		
		if (x == "")
			break;
		else {
			x.back() = incrementBase(x.back());
			out.push_back(x);
		}
	}
	
	return out;
}



// Concatenate lyndon words up to length n
string deBruijnGenerator (int n) {
	vector <string> lyndonWords = lydonWordGenerator(n);
	sort(lyndonWords.begin(), lyndonWords.end());
	
	string out;
	for (int i=0; i<lyndonWords.size(); i++) {
		if (n%lyndonWords.at(i).size() == 0)
			out += lyndonWords.at(i);
	}
	
	return out;
}



// Any permutation of characters is still a de Bruijn sequence
// Do permutations to obtain multiple de Bruijn sequences
string generateConsensusSequence (int patternLength, int numSequences) {
	string deBruijn = deBruijnGenerator(patternLength);
	string out = deBruijn;
	
	deBruijn = deBruijn + deBruijn.substr(0, patternLength-1);
	replace( deBruijn.begin(), deBruijn.end(), 'A', 'a');
	replace( deBruijn.begin(), deBruijn.end(), 'C', 'c');
	replace( deBruijn.begin(), deBruijn.end(), 'G', 'g');
	replace( deBruijn.begin(), deBruijn.end(), 'T', 't');
	
	vector <string> inlibrary = possibleBarcodes(patternLength,0);
		
	vector <char> bases(4);
	bases.at(0) = 'A';
	bases.at(1) = 'C';
	bases.at(2) = 'G';
	bases.at(3) = 'T';
	
	for (int i=0; i<numSequences && i<24; i++) {
		string out = deBruijn;
		random_shuffle(bases.begin(), bases.end());
		replace( out.begin(), out.end(), 'a', bases.at(0));
		replace( out.begin(), out.end(), 'c', bases.at(1));
		replace( out.begin(), out.end(), 'g', bases.at(2));
		replace( out.begin(), out.end(), 't', bases.at(3));
		
		cout << out << endl;		
		if (evaluate (inlibrary, out ) != -1) {
			cout << "Error: not de Bruijn" << endl;
		}		
	}
	
	return out;
}



void generateSingleLibrary (const string consensus, const string leftAdaptor, const string rightAdaptor, const string filename) {
	// Run parameters
	ofstream outfile (filename.c_str());
	string RE1 = "GAGCTC";
	string RE2 = "CTCGAG";
	
	// Generate list of barcodes
	int barcodeLength = 7;
	int barcode2barcodeDist = 2;
	vector <string> barcodes = possibleBarcodes(barcodeLength,barcode2barcodeDist);
	random_shuffle(barcodes.begin(), barcodes.end());
	cout << barcodes.size() << endl;
	
	int RE1StartPos = leftAdaptor.size();
	int leftOffset = leftAdaptor.size() + RE1.size() + barcodeLength;
	int RE2StartPos = leftOffset + consensus.size();
	int mismatchIndex = 0;
	
	int discardCount = 0;
	
	for (int i=0; i<barcodes.size() && mismatchIndex < 9*consensus.size(); i++) {
		string oligo = leftAdaptor + RE1 + barcodes.at(i) + consensus + RE2 + rightAdaptor;
		
		// Quality check: avoid additional cut sites
		if (oligo.find(RE1) == oligo.rfind(RE1) && oligo.find(RE2) == oligo.rfind(RE2) ) {
			// Passed the test.
			int subsPos = (mismatchIndex/3)%consensus.size() + leftOffset;
			int subsType = mismatchIndex%3;
			
			char newBase = oligo.at(subsPos);
			for (int i=0; i<=subsType; i++)
				newBase = incrementBase(newBase);
			 
			oligo.at(subsPos) = newBase;
			outfile << oligo << endl;
			mismatchIndex++;
		}
		// Else, test failed, ignore this barcode
		else 
			discardCount++;
	}
	
	cout << "Number of discarded sequences due to extraneous cut sites was " << discardCount << endl;
	
	outfile.close();
	return;
}


void generateSingleInsertionLibrary (const string consensus, const string leftAdaptor, const string rightAdaptor, const string filename) {
	// Run parameters
	ofstream outfile (filename.c_str());
	string RE1 = "GAGCTC";
	string RE2 = "CTCGAG";
	string bases = "ACGT";
	
	// Generate list of barcodes
	int barcodeLength = 7;
	int barcode2barcodeDist = 2;
	vector <string> barcodes = possibleBarcodes(barcodeLength,barcode2barcodeDist);
	random_shuffle(barcodes.begin(), barcodes.end());
	cout << barcodes.size() << endl;
	
	int RE1StartPos = leftAdaptor.size();
	int leftOffset = leftAdaptor.size() + RE1.size() + barcodeLength;
	int RE2StartPos = leftOffset + consensus.size();
	int mismatchIndex = 0;
	int discardCount = 0;
	
	for (int i=0; i<barcodes.size() && mismatchIndex < 50*consensus.size(); i++) {
		string oligo = leftAdaptor + RE1 + barcodes.at(i) + consensus + RE2 + rightAdaptor;
		
		// Quality check: avoid additional cut sites
		if (oligo.find(RE1) == oligo.rfind(RE1) && oligo.find(RE2) == oligo.rfind(RE2) ) {
			// Passed the test.
			int insertionPos = (mismatchIndex/4)%(consensus.size()-1) + leftOffset;
			char insertedBase = bases.at(mismatchIndex%4);
			oligo = oligo.substr(0,insertionPos+1) + insertedBase + oligo.substr(insertionPos+1,1000);
			outfile << oligo << endl;
			mismatchIndex++;
		}
		// Else, test failed, ignore this barcode
		else
			discardCount++;
	}
	
	cout << "Number of discarded sequences due to extraneous cut sites was " << discardCount << endl;
	outfile.close();
	return;
}



void generateDoubleInsertionLibrary (const string consensus, const string leftAdaptor, const string rightAdaptor, const string filename) {
	// Run parameters
	ofstream outfile (filename.c_str());
	string RE1 = "GAGCTC";
	string RE2 = "CTCGAG";
	string bases = "ACGT";
	
	// Generate list of barcodes
	int barcodeLength = 8;
	int barcode2barcodeDist = 2;
	vector <string> barcodes = possibleBarcodes(barcodeLength,barcode2barcodeDist);
	random_shuffle(barcodes.begin(), barcodes.end());
	cout << barcodes.size() << endl;
	
	int RE1StartPos = leftAdaptor.size();
	int leftOffset = leftAdaptor.size() + RE1.size() + barcodeLength;
	int RE2StartPos = leftOffset + consensus.size();
	int mismatchIndex = 0;
	int discardCount = 0;
	
	for (int i=0; i<barcodes.size() && mismatchIndex < 120*consensus.size(); i++) {
		string oligo = leftAdaptor + RE1 + barcodes.at(i) + consensus + RE2 + rightAdaptor;
		
		// Quality check: avoid additional cut sites
		if (oligo.find(RE1) == oligo.rfind(RE1) && oligo.find(RE2) == oligo.rfind(RE2) ) {
			// Passed the test.
			int insertionPos = (mismatchIndex/16)%(consensus.size()-1) + leftOffset;
			char insertedBase1 = bases.at((mismatchIndex%16)%4);
			char insertedBase2 = bases.at((mismatchIndex%16)/4);
			oligo = oligo.substr(0,insertionPos+1) + insertedBase1 + insertedBase2 + oligo.substr(insertionPos+1,1000);
			outfile << oligo << endl;
			mismatchIndex++;
		}
		// Else, test failed, ignore this barcode
		else
			discardCount++;
	}
	
	cout << "Number of discarded sequences due to extraneous cut sites was " << discardCount << endl;
	outfile.close();
	return;
}


int main () {
	string Z13 = "CGCTCCTTGTTGTACTCGCA";
	string Z14 = "TGACGGAGGATAGAAGGCCA";

	vector<string> rightAdaptor (13);
	rightAdaptor.at(0) = "AAGCTACGATGCGGTCCCAT";
	rightAdaptor.at(1) = "TATCGGGTACCATTCAGCCG";
	rightAdaptor.at(2) = "TGTCTACACCAGCTGCGCTC";
	rightAdaptor.at(3) = "ACGTGCTGAATTCGAAGTCG";
	rightAdaptor.at(4) = "ATAAGGGATCGAGCGGGTGT";
	rightAdaptor.at(5) = "ATACCGGCGGTTGTTCAACC";
	rightAdaptor.at(6) = "GAAATCCGCAACGGCAATTT";
	
	string consensus = "TTTATTCTTGTAATACTAGTCATCCTCGTGATGCTGGAAACAAGACCACGAGCAGGCCCGCGGGTT";
	generateSingleInsertionLibrary (consensus, Z13, rightAdaptor.at(5), "./singleInsertion.txt");
	generateDoubleInsertionLibrary (consensus, Z14, rightAdaptor.at(5), "./doubleInsertion.txt");
	
	return 0;
}



