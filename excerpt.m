%%
% Daws a zoomed-in portion of the repair efficieny graphs using the provided *.mat file


matfilename = "/home/tk/5MLx matfiles/combined29-30.mat";
libname = "5ML13";


addpath ./common
repairInit;
load(matfilename);
backgroundEfficiency = 0.37398; % minumum repair efficiency of combined3-4.mat for 3ML1 mutS deficient.
reprate = (reprate - backgroundEfficiency) / (1-backgroundEfficiency);


window = 1; % 4

set(0,'DefaultAxesFontSize', 34);
for MMlocation = 5:76  % SSL1 pos 16; SSL3 36 vs 63; SSL4 pos 48 and 68; 5ML13 pos 62 or [ 8 14 58 ]
	temp = reprate(:,MMlocation-window:MMlocation+window);
	vector = reshape(reprate(:,(MMlocation-(window-1)):(MMlocation+(window-1))), 1, []);
	
	if sequence(MMlocation)=='T'
		fprintf('%f, ', vector);
		fprintf('\n');
	end
		continue;
	
	close all;
	imagesc(temp);
	set(gca,'Xtick',1:(2*window+1));
	set(gca,'YTickLabel', {'A','T','C','G'} )
	set(gca,'Ytick',1:4);
	
	set(gca,'XTickLabel', seqLabel(MMlocation-window:MMlocation+window) )
	set(gca,'TickLength',[0 0]);
	axis equal tight
	caxis([0 1])
	colormap(hot(256));
	
	if false
		% Make a contour around TT and  CT mismatches of interest
		% linewidth property of Octave does not work, hence the work around 
		xpos = [ 2.5 3.5 3.5 2.5 2.5 ];
		ypos1 = [ 1.5 1.5 2.46 2.46 1.5 ];
		ypos2 = [ 2.53 2.53 3.5 3.5 2.53 ];
		line(xpos,ypos1,"color","g","linewidth", 7);
		line(0.99*xpos,0.99*ypos1,"color","g","linewidth", 7);
		line(1.01*xpos,1.01*ypos1,"color","g","linewidth", 7);
		line(0.99*xpos,0.99*ypos2, "color","m","linewidth", 7);
		line(xpos,ypos2,"color","m","linewidth", 7);
		line(1.01*xpos,1.01*ypos2,"color","m","linewidth", 7);
	end
	
	print("./temp.png", '-r300');
	in = imread("./temp.png");
	delete ./temp.png
	collapsed = sum(in,3);
	binary = (collapsed<=1.2*min(min(collapsed)));
	
	n = 20;
	mask = ones(n,n);
	filtered = conv2(binary,mask,'same');
	filtered = (filtered>250);

	% generate hatching pattern
	[xx,yy] = meshgrid(1:size(in,2),1:size(in,1));
	hatching = (mod(xx-yy, 50)<20);
	red = in(:,:,1);
	red(filtered) = 255*hatching(filtered);
	green = in(:,:,2);
	green(filtered) = 255*hatching(filtered);
	blue = in(:,:,3);
	blue(filtered) = 255*hatching(filtered);

	out = zeros(size(red,1),size(red,2),3);
	out(:,:,1) = red;
	out(:,:,2) = green;
	out(:,:,3) = blue;
	
	outfilename = sprintf('~/efficiencyExcerptpos%d.png', MMlocation);
	imwrite(uint8(out), outfilename);
end


