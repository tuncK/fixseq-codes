%%%
% Analyses the outliers that are difficult to repair for double barcoded libraries.

addpath ./common/
more off;

allTriplets = {};
allPentamers = {};
tripletEfficiency = [];
pentamerEfficiency = [];

for libID=1:1:13
	% Set the parameters depending on the chosen library
	libname = sprintf("5ML%d", libID);
	repairInit;
	fixedStrand = [fixedStrand "GAGCTC"]; % Add the information about the fixed XhoI cut site.	
	filename = sprintf("~/5MLx matfiles/combined%d-%d.mat", 2*libID+3, 2*libID+4);
	load(filename);
	
	backgroundEfficiency = 0.37398; % minumum repair efficiency of combined3-4.mat for 3ML1 mutS deficient.
	reprate = (reprate - backgroundEfficiency) / (1-backgroundEfficiency);

	for pos=2:1:len
		for base = 1:1:4
			if isnan(reprate(base,pos))
				continue
			end
			
			outlierTriplet = [ fixedStrand( (pos+1):-1:(pos-1) ) '-' num2dna(base) ];
			allTriplets{end+1} = outlierTriplet;
			tripletEfficiency = [tripletEfficiency; reprate(base,pos) ];
		end
	end
	
	for pos=3:1:len
		for base = 1:1:4
			if isnan(reprate(base,pos))
				continue
			end
			
			outlierPentamer = [ fixedStrand( (pos+2):-1:(pos-2) ) '-' num2dna(base) ];
			allPentamers{end+1} = outlierPentamer;
			pentamerEfficiency = [pentamerEfficiency; reprate(base,pos) ];
		end
	end
end


file3 = fopen("trimer.csv", "wt");
fprintf(file3, "MMType,Strand1(5>3),Strand2(3>5),Mean,STDEV,Number,\n");
			
file5 = fopen("pentamer.csv", "wt");
fprintf(file5, "MMType,Strand1(5>3),Strand2(3>5),Mean,STDEV,Number,\n");

%for MMtype = {"AA", "AC", "AG", "CC", "CT", "GG", "GT", "TT" }
for MMtype = { "AA", "AC", "AG", "CA", "CC", "CT", "GA", "GG", "GT", "TC", "TG", "TT" }
	for base1="ACGT"
		for base2="ACGT"
			hits1 = strcmp(allTriplets, sprintf("%s%s%s-%s", base1, MMtype{}(1), base2, MMtype{}(2)) );
			hits2 = strcmp(allTriplets, sprintf("%s%s%s-%s", complementary(base2), MMtype{}(2), complementary(base1), MMtype{}(1)) );
			selection = ( hits1 | hits2 )';
			
			selectedRates = tripletEfficiency(selection);
			numSelected = length(selectedRates);
			avg = mean(selectedRates);
			stdev = std(selectedRates,1);
			strand1 = sprintf("%s%s%s",base1, MMtype{}(1), base2);
			strand2 = sprintf("%s%s%s", complementary(base1), MMtype{}(2), complementary(base2) );
			fprintf(file3, "%s, %s, %s, %.3f, %.3f, %d,\n", MMtype{}, strand1, strand2, avg, stdev, numSelected);
			
			% Now calculate for the pentamer context
			for base3="ACGT"
				for base4="ACGT"
					hits1 = strcmp( allPentamers, sprintf("%s%s%s%s%s-%s", base1, base2, MMtype{}(1), base3, base4, MMtype{}(2)) );
					hits2 = strcmp( allPentamers, sprintf("%s%s%s%s%s-%s", complementary(base4), complementary(base3), MMtype{}(2), complementary(base2), complementary(base1), MMtype{}(1)) );
					selection = ( hits1 | hits2 )';
					
					selectedRates = pentamerEfficiency(selection);
					numSelected = length(selectedRates);
					avg = mean(selectedRates);
					stdev = std(selectedRates,1);
					strand1 = sprintf("%s%s%s",base1, base2, MMtype{}(1), base3, base4);
					strand2 = sprintf("%s%s%s", complementary(base1), complementary(base2), MMtype{}(2), complementary(base3), complementary(base4) );
					
					fprintf(file5, "%s, %s, %s, %.3f, %.3f, %d,\n", MMtype{}, strand1, strand2, avg, stdev, numSelected);
				end
			end
		end	
	end
end

fclose(file3);
fclose(file5);


