%%%
% Analyses the outliers that are difficult to repair for double barcoded libraries.

more off;

allTriplets = {};
tripletEfficiency = [];


for libID=1:1:13
	% Set the parameters depending on the chosen library
	libname = sprintf("5ML%d", libID);
	repairInit;
	fixedStrand = [fixedStrand "GAGCTC"]; % Add the information about the fixed XhoI cut site.
	
	filename = sprintf("~/5MLx matfiles/combined%d-%d.mat", 2*libID+3, 2*libID+4);
	load(filename);
	backgroundEfficiency = 0.37398; % minumum repair efficiency of combined3-4.mat for 3ML1 mutS deficient.
	reprate = (reprate - backgroundEfficiency) / (1-backgroundEfficiency);

	for pos=3:1:len
		for base = 1:1:4
			if isnan(reprate(base,pos))
				continue
			end
			
			outlierTriplet = [ fixedStrand( (pos+2):-1:(pos-2) ) '-' num2dna(base) ];
			allTriplets{end+1} = outlierTriplet;
			tripletEfficiency = [tripletEfficiency; reprate(base,pos) ];
		end
	end
end


% Calculation of global mean and stdev
globalMean = mean(tripletEfficiency);
globalSEM = std(tripletEfficiency,1) / sqrt(length(tripletEfficiency));


% Label the mismatches w.r.t. their MM type or context and plot
set(0, 'defaultaxesfontsize', 22);
contextLabels = {}; 
%  R = A,G; Y = C,T; N = A,C,G,T
close all;
caseID = 0;
hold on;
for base1 = "N"
	for base2 = "N"
		for base3 = "N"
			for base4 = "N"
				query = sprintf("%s%sN%s%s-N", base1, base2, base3, base4);
				query = strrep(query,"R","[AG]");
				query = strrep(query,"Y","[CT]");
				query = strrep(query,"N","[ACGT]");
				
				query2 = sprintf("%s%sN%s%s-N", complementary(base4), complementary(base3), complementary(base2), complementary(base1) );
				query2 = strrep(query2,"R","[AG]");
				query2 = strrep(query2,"Y","[CT]");
				query2 = strrep(query2,"N","[ACGT]");
				selection = (~cellfun( @isempty, regexp(allTriplets,query) ) | ~cellfun( @isempty, regexp(allTriplets,query2) ) );
				
				selectedRates = tripletEfficiency(selection);
				numSelected = length(selectedRates);
				avg = mean(selectedRates);
				dev = std(selectedRates,1);
				sem = dev/sqrt(length(selectedRates));
				z = abs(avg-globalMean) / sqrt(sem^2+globalSEM^2);
				pvalue = erfc(z/sqrt(2))*0.5;
				selectedRates(selectedRates<0) = 0;
				
				caseID++;
				plot(caseID*ones(numSelected,1)+0.4*rand(numSelected,1)-0.2, selectedRates, "ro", "linewidth", 1.5);
				h=errorbar(caseID, avg, dev, 'k^');
				set(h, 'linewidth', 2, "markerfacecolor", "auto");
				text(caseID-0.25, 1.1, sprintf("%.0E", pvalue), "fontsize", 16 );
				contextLabels{end+1} = sprintf("%s%s-%s%s",base1,base2,base3,base4);
			end
		end
	end
end

plot(0:caseID+1, globalMean*ones(caseID+2,1), "b-");

hold off;
xlabel('MM context');
ylabel('Scaled \eta');
set(gca,'XTick', 1:caseID)
set(gca,'XTickLabel', contextLabels)
xlim([0 caseID+1]);
ylim([0 1.1]);
pause(0.1)
%print('~/YNNR.png','-r300');



