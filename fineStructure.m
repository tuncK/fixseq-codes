%%


len = 49;
count = 29; % out of 49 for p-value calculation

% Probability of observing by random chance
pvalue = 0;
for n = count:1:len;
	pvalue += nchoosek(len,n) .* (1/2).^len;
end
pvalue


repairInit;
bins = 0.01:0.02:0.99;
N = length(bins);


histsCumulated = [];
for i=74:77
%	filename = sprintf("/home/tk/hists/DEB3L/sample%d.hist",i);
	filename = sprintf("/home/tk/hists/SML/sample%d.hist",i);
	load(filename); % load mat file with clan histograms
	histsCumulated = cat(3,histsCumulated,hists);
end
hists = histsCumulated;
singleMM; % provides sizes, biases and repair efficiencies

% Calculation of the real histogram
biases = biases(~isnan(biases));
cumulativeBiases = biases;
numData = length(biases);
overallUnrepairedPrct = sum( (biases>0.1) & (biases<0.9) )/length(biases)
realHist = hist(biases, N);
	

if false
biasesMean = biases / numData;
tempHist = zeros(length(realHist),4);
for idx=74:77
%	filename = sprintf("/home/tk/hists/DEB3L/sample%d.hist",idx);
	filename = sprintf("/home/tk/hists/SML/sample%d.hist",idx);
	load(filename); % load mat file with clan histograms
	histsCumulated = cat(3,histsCumulated,hists);
	hists = histsCumulated;
	singleMM; % provides sizes, biases and repair efficiencies

	% Calculation of the real histogram
	biases = biases(~isnan(biases));
	thist = hist(biases, N);
	tempHist(:,idx) = thist / sum(thist);
end
biasesSTD = std(tempHist, [], 2) / sqrt(4);

set(0, 'defaultaxesfontsize', 20);
close all;
hold on
bar(bins, realHist/sum(realHist), 'k', 'edgecolor', 'none');
h=errorbar(bins, realHist/sum(realHist), biasesSTD, 'r.');
hold off

set(h,'linewidth', 2);
xlabel('Intra-clan substitution prevalence')
ylabel('Fraction of clans');
ylim([0 0.04]);
print('~/mutSObservedCounts.png', '-r300');
end


if true
	close all
	set(0, 'defaultaxesfontsize', 25);	
	hist(cumulativeBiases, N, 'k', 'edgecolor','none')
	topLim = get(gca,'ylim')(2);
	fill( [0.1 0.9 0.9 0.1], [0 0 topLim topLim], [0.8 0.9 1], 'edgecolor', 'none'); % makes a shaded region
	hold on
%	hist(biasesMean, N, 'k', 'edgecolor','none');
	bar(bins, realHist/1000, 'k', 'edgecolor', 'none');
	hold off
	
	yt = get(gca, 'YTick');
	%set(gca, 'YTick', yt, 'YTickLabel', yt/1000) % Rescale y-axis by 1000 fold

	xlabel('Intra-clan substitution prevalence')
	ylabel('# Clans observed (1000)')
	
	yrange = get(gca,'ylim');
	text(0.36, 0.7*yrange(2), sprintf('%.1f%%', overallUnrepairedPrct*100), 'fontsize', 40);
		
	print('~/mutSNPLCounts.png', '-r300');
	return
end




MinClanSize = min(sizes);
MaxClanSize = max(sizes);
sizes(sizes>MaxClanSize) = [];
pdf = histc(sizes,1:1:MaxClanSize);
pdf /= max(pdf);


pooled1 = zeros(1,N);
pooled2 = pooled1;
templist = unique(sizes);
for i = MinClanSize:1:MaxClanSize
	if pdf(i) == 0
		continue;
	end

	% Calculation of binomial coefficients
	n = floor(i/2);
	index = 1:1:n;
	c = zeros(i+1,1);
	if rem(i,2) == 0 % case i even
		factors = (n-index+1)./(n+index);
		factors = cumprod(factors);
		c(1:n) = fliplr(factors);
		c(n+1) = 1;
		c(n+2:end) = factors;
	else
		factors = (n-index+1)./(n+index+1);
		factors = cumprod(factors);
		c(1:n) = fliplr(factors);
		c(n+1:n+2) = 1;
		c(n+3:end) = factors;
	end
	
	c = c/sum(c);
	counts = floor(100*c);
	totalCount = sum(counts);
	expandedEntries = zeros(totalCount,1);
	
	Mhead = 1;
	prevalence = (0:i)/i;
	for j=1:i+1
		expandedEntries(Mhead:Mhead+counts(j)-1) = prevalence(j);
		Mhead += counts(j);
	end
	
	temp = hist(expandedEntries,bins) * pdf(i); %Binomial weights
	pooled1 += temp;
	temp = hist(prevalence,bins) * pdf(i); %Uniform weights
	pooled2 += temp;
end

pooled1 = pooled1/sum(pooled1);
pooled2 = pooled2/sum(pooled2);

realHist = hist(biases, bins);
realHist = realHist / sum(realHist);
realChange = ([realHist] - [0 realHist(1:end-1)]) * 2 ./  ([realHist] + [0 realHist(1:end-1)]) ;
theory1Change = ([pooled1] - [0 pooled1(1:end-1)]) * 2 ./ ([pooled1] + [0 pooled1(1:end-1)]);
theory2Change = ([pooled2] - [0 pooled2(1:end-1)]) * 2 ./ ([pooled2] + [0 pooled2(1:end-1)]);

sum(sign(realChange) == sign(theory1Change))
sum(sign(realChange) == sign(theory2Change))


% Comparison of observed - expected changes
set(0, 'defaultaxesfontsize', 25)
p(1) = bar(bins, realChange, 'k', 'edgecolor', 'none', 'barwidth', 1.1);
hold on;
p(2) = plot(bins, theory1Change, 'rd', 'linewidth', 1.5, "markersize", 5, 'MarkerFaceColor','r');
plot(bins, theory1Change, 'r-', 'linewidth', 1);
p(3) = plot(bins, theory2Change, 'g^', 'linewidth', 1.5, "markersize", 5, 'MarkerFaceColor','g');
plot(bins, theory2Change, 'g-', 'linewidth', 1);
hold off;

%ylim([-0.03 0.03])
xlabel('Intra-clan substitution prevalence')
ylabel('Change in clan count');
xlim([-0.05 1.05])
set(gca, 'xtick', 0:0.2:1);

HLEG = legend(p, 'Data', 'Binomial', 'Uniform');
legend boxoff;
set(HLEG,'color','none');
legend('location', 'south', 'orientation', 'horizontal');
lchildren = get(HLEG, 'children');
set(lchildren(5), "color", 'r');
set(lchildren(4), "color", 'g');
set(lchildren(1:2), "markersize", 13);
print('~/changeOverlay.png', '-r300')


%% Histograms of expectations-observations
bar(bins, realHist, 'k');
xlabel('Intra-clan substitution prevalence')
ylabel('# Clans observed')
ylim([0 0.03])
print('~/mutSObservedCounts.png', '-r300')


bar(bins, pooled1, 'k');
xlabel('Intra-clan substitution prevalence')
ylabel('Fraction of clans')
print('~/binomialModel.png', '-r300')


bar(bins, pooled2, 'k');
xlabel('Intra-clan substitution prevalence')
ylabel('Fraction of clans')
print('~/uniformModel.png', '-r300')


