%%%
% Analysis script for double MM library

repairInit;

lowCutoff = 0.1;
highCutoff = 1-lowCutoff;


countsConsolidated = [];
bothRepairedHistConsolidated = [];
mixedHistConsolidated = [];
bothUnrepairedHistConsolidated = [];

for firstFileID = 36:1:50
	histsCumulated = [];
	for i=firstFileID:1:firstFileID
		filename = sprintf("/home/tk/hists/DML/sample%d.hist",i);
		load(filename); % load mat file with clan histograms
		histsCumulated = cat(3, histsCumulated, hists);
	end
	hists = histsCumulated;

	discardCount = 0;
	unmappableCount = 0;
	singleRepairedCount = 0;
	singleUnrepairedCount = 0;
	doubleBothRepairedHist = zeros(1,60);
	doubleMixedHist = zeros(1,60);
	doubleUnrepairedHist = zeros(1,60);

	numHists = size(hists,3);
	for i=1:1:numHists
		block = hists(:,:,i);
		numreads = sum(block(:,1));
		if numreads < 10
			continue;
		end	
		
		block /= numreads;
		difference = block - original;
		difference (difference < 0) = 0;
			
		detectedChanges = (difference > lowCutoff);
		changedPos = (sum(detectedChanges,1) > 0);
		changeCount = sum( changedPos > 0);
		
		switch changeCount
			case 0 % No peaks found. MM type not deducible
				unmappableCount++;
				
			case 1 % One peak only, one of the mismatches repaired correctly 
				% Indeterminacy.
				pos = find(changedPos == 1);
				[freq,base] = max(difference(:,pos));
				
				if freq >= highCutoff
					% conclusion =  'Single repaired';
					singleRepairedCount++;
				elseif freq > lowCutoff
					% conclusion = 'Single unrepaired';
					singleUnrepairedCount++;
				end
			
			case 2 % two peaks, double mismatch detected
				pos = find(changedPos == 1);
				dpos = pos(2)-pos(1);
				
				[freq1,base1] = max(difference(:,pos(1)));
				[freq2,base2] = max(difference(:,pos(2)));
				
				if freq1 >= highCutoff && freq2 >= highCutoff
					%conclusion =  'Both repaired';
					doubleBothRepairedHist(dpos)++;
				elseif (freq1 > lowCutoff && freq2 >= highCutoff) || (freq1 >= highCutoff && freq2 > lowCutoff)
					%conclusion = 'One repaired, one unrepaired';
					doubleMixedHist(dpos)++;
				elseif freq1 > lowCutoff && freq2 > lowCutoff
					%conclusion = 'Both unrepaired';
					doubleUnrepairedHist(dpos)++;
				end
			
			otherwise
				% Too many substitutions, disregard this clan.
				% conclusion = 'Indeterminate';
				discardCount++;
		end
	end

	countsConsolidated = [countsConsolidated; discardCount, unmappableCount, singleRepairedCount, singleUnrepairedCount, sum(doubleBothRepairedHist), sum(doubleMixedHist), sum(doubleUnrepairedHist)];
	
	bothRepairedHistConsolidated = [bothRepairedHistConsolidated; doubleBothRepairedHist];
	mixedHistConsolidated = [mixedHistConsolidated; doubleMixedHist];
	bothUnrepairedHistConsolidated = [bothUnrepairedHistConsolidated; doubleUnrepairedHist];
end


countsConsolidated = countsConsolidated ./ sum(countsConsolidated,2);
countsConsolidatedMean = [ mean(countsConsolidated(1:3,:)); mean(countsConsolidated(4:6,:)); mean(countsConsolidated(7:9,:)); mean(countsConsolidated(10:12,:)); mean(countsConsolidated(13:15,:)) ];
countsConsolidatedStd = [ std(countsConsolidated(1:3,:),0,1); std(countsConsolidated(4:6,:),0,1); std(countsConsolidated(7:9,:),0,1); std(countsConsolidated(10:12,:),0,1); std(countsConsolidated(13:15,:),0,1) ];


% Bar graph, position and type averaged
set(0, 'defaultaxesfontsize', 18);
b = bar(countsConsolidatedMean', 'edgecolor', 'none');
hold on;

% Put error bars
for k1 = 1:length(b)
	hb = get(get(b(k1),'Children'), 'XData');
	midbar = mean(hb);
	h = errorbar(midbar, countsConsolidatedMean(k1,:), countsConsolidatedStd(k1,:)/sqrt(3), 'k.');
	set(h, 'linewidth', 1.5);
end
hold off;

set(gca,'XTickLabel', {'Noise', '?+?', 'R+?', 'U+?', 'R+R', 'R+U', 'U+U'} )
ylabel('Frequency of clans');
set(b(1), 'facecolor', 'r');
set(b(2), 'facecolor', 'm');
set(b(3), 'facecolor', 'b');
set(b(4), 'facecolor', 'c');
set(b(5), 'facecolor', 'k');

HLEG = legend('wt', 'wt-nicked', '\DeltamutS', '\DeltamutS-nicked', '\DeltamutL-nicked');
legend boxoff;
legend('location', 'northwest');
set(HLEG,'color','none');
lchildren = get(HLEG, 'children');
set(lchildren(6), "color", get(lchildren(1),'facecolor') );
set(lchildren(7), "color", get(lchildren(2),'facecolor') );
set(lchildren(8), "color", get(lchildren(3),'facecolor') );
set(lchildren(9), "color", get(lchildren(4),'facecolor') );
set(lchildren(10), "color", get(lchildren(5),'facecolor') );

ylim([0 0.5]);
set(HLEG, 'position', [0.27 0.63 0.2 0.28])
print('/home/tk/eventTypeHist.png', '-r300');




total = 2*bothRepairedHistConsolidated + 2*mixedHistConsolidated + bothUnrepairedHistConsolidated;
bothRepairedHistConsolidated = bothRepairedHistConsolidated ./ total;
mixedHistConsolidated = mixedHistConsolidated ./ total;
bothUnrepairedHistConsolidated = bothUnrepairedHistConsolidated ./ total;


% Compensate for the oscillations due to library design
doubleSim;
expectedDist = bins/sum(bins);
invalid = (expectedDist == 0);
bothRepairedHistConsolidated(:, invalid) = nan;
mixedHistConsolidated(:, invalid) = nan;
bothUnrepairedHistConsolidated(:, invalid) = nan;


set(0, 'defaultaxesfontsize', 25);
filenameLabels = {'wt', 'wtNicked', 'mutS', 'mutSnicked', 'mutLnicked'};

for i=1:5
	bothRepaired = bothRepairedHistConsolidated(3*i-2:3*i,:);
	mixed = mixedHistConsolidated(3*i-2:3*i,:);
	bothUnrepaired = bothUnrepairedHistConsolidated(3*i-2:3*i,:);

	close all;
	h(1) = errorbar(mean(bothRepaired), std(bothRepaired,0,1)/sqrt(3), 'ro');
	hold on
	h(2) = errorbar(mean(mixed), std(mixed,0,1)/sqrt(3), 'bv');
	h(3) = errorbar(mean(bothUnrepaired), std(bothUnrepaired,0,1)/sqrt(3), 'k^');
	hold off
	set(h, 'linewidth', 1.5);
	set(h, 'markerfacecolor', 'auto');

	xlabel('Inter-mismatch distance (bp)');
	ylabel('Fraction of clans');
	HLEG = legend('R+R', 'R+U', 'U+U');
	legend('orientation', 'horizontal');
	legend boxoff;
	lchildren = get(HLEG, 'children');
	set(lchildren(4), "color", get(lchildren(1),'color') );
	set(lchildren(5), "color", get(lchildren(2),'color') );
	set(lchildren(6), "color", get(lchildren(3),'color') );
	set(lchildren(1:3), "markersize", 13);
	ylim([0 1]);
	xlim([0 55]);
	print(sprintf('/home/tk/%s.png', filenameLabels{i}), '-r300');
end


% Expected correction factors
measured = reshape((total./sum(total,2))',[],1);
expected = repmat(expectedDist, 15, 1);
R2 = ( sum((measured-mean(measured)).*(expected-mean(expected)))/(std(measured)*std(expected)*(length(measured)-1)) )^2;
expected = expected + 0.005*(randn(size(expected)));
measured = measured + 0.005*(randn(size(expected)));

plot(expected, measured, 'bo', 'markersize', 2, 'markerfacecolor', 'auto');
hold on;
plot(-0.05:0.01:0.2, -0.05:0.01:0.2, 'k-', 'linewidth', 2);
hold off;

xlabel('Expected abundance');
ylabel('Observed abundance');
text(0.02, 0.15, sprintf('R^2=%.3f', R2), 'fontsize', 25);
print('/home/tk/fluctuationFit.png', '-r300');


