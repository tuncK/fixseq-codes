%%
% Combines the repaired/unrepaired counts obtained from multiple experimental replicates

fileIDs = 19:21;

filename = sprintf("/home/tk/3ML1 matfiles/sample%d.mat", fileIDs(1) );
load(filename);
totalrepairedF = repairedF;
totalrepairedV = repairedV;
totalunrepaired = unrepaired;


for ii = 2:length(fileIDs)
filename = sprintf("/home/tk/3ML1 matfiles/sample%d.mat", fileIDs(ii) );
	load(filename);
	totalrepairedF += repairedF;
	totalrepairedV += repairedV;
	totalunrepaired += unrepaired;
end

% Recalculate the repair rate/bias based on the cumulated counts
reprate = (totalrepairedF + totalrepairedV) ./ (totalrepairedF + totalrepairedV + totalunrepaired);
bias = (totalrepairedV-totalrepairedF) ./ (totalrepairedF + totalrepairedV);
repairedF = totalrepairedF;
repairedV = totalrepairedV;
unrepaired = totalunrepaired;

save( sprintf('~/combined%d-%d.mat', fileIDs(1), fileIDs(end)), 'reprate', 'bias', 'repairedF', 'repairedV', 'unrepaired', 'libname');


