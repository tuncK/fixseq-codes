%% 
% Classify outliers based on the general trend in the data
% 1 = H: Higher than the overall trend
% 0 = M: follows the trend
% -1 = L: Lower than the trend



set(0,'DefaultAxesFontSize',25);
%acceptableRange = 1:50; % for SML
%acceptableRange = 4:57; % for DEB3
acceptableRange = 1:66; % for 3ML1
%acceptableRange = 1:84 % for 5MLx


% Comparison of DEB3L/R with 3ML1 results
% DEB3L
% sequence = "TTTATTCTTGTAATACTAGTCATCCTCGTGATGCTGGAAACAAGACCACGAGCAGGCCCG";

%DEB3R = "GCTGGAAACAAGACCACGAGCAGGCCCGCGGGTTTATTCTTGTAATACTAGTCATCCTCG";
%3ML1 = "TTTATTCTTGTAATACTAGTCATCCTCGTGATGCTGGAAACAAGACCACGAGCAGGCCCGCGGGTT";
%acceptableRange1 = [36:65 2:25];


for i=1
	filenamex = '~/3ML1 matfiles/combined1-2.mat';
	filenamey = '~/3ML1 matfiles/combined7-8.mat';
	xlabeltext = 'wt, tUNC19';
	ylabeltext = 'wt, 91CNUt';
	outfilename = '~/wttunc19-wthm19.png';
	
	load(filenamex);
	datax = reshape(reprate(:,acceptableRange)', 1, []);

	load(filenamey);
	datay = reshape(reprate(:,acceptableRange)', 1, []);

	% Ignore entries that are NaN
	isValid = (~isnan(datax) & ~isnan(datay));
	datax = datax(isValid);
	datay = datay(isValid);
	range = repmat(acceptableRange,1,4);
	range = range(isValid);

	% Classify as low/medium/highly repaired compared to the background.
	degree = 4;
	backgroundCoeff = 0.5; % was 0.5
	px = polyfit(range, datax, degree);
	trendx = polyval(px, range);
	deviationx = datax - trendx;
	cutoffx = backgroundCoeff*mean(abs(deviationx));
	labelsx = 2*ones(length(range), 1);
	labelsx(deviationx > cutoffx) = 3;
	labelsx(deviationx < -cutoffx) = 1;

	py = polyfit(range, datay, degree);
	trendy = polyval(py, range);
	deviationy = datay - trendy;
	cutoffy = backgroundCoeff*mean(abs(deviationy));
	labelsy = 2*ones(length(range), 1);
	labelsy(deviationy > cutoffy) = 3;
	labelsy(deviationy < -cutoffy) = 1;

	% Optionally show the rank assignments
	if true
		close all
		set(0, 'defaultaxesfontsize', 25)
		plot(range(labelsx==3), datax(labelsx==3), 'rd', 'MarkerFaceColor', 'auto'); % High efficiency
		hold on;
		plot(range(labelsx==2), datax(labelsx==2), 'go', 'MarkerFaceColor', 'auto'); % intermediate efficiency
		plot(range(labelsx==1), datax(labelsx==1), 'b^', 'MarkerFaceColor', 'auto'); % low efficiency
		n = range(end);
		plot(1:n, polyval(px, 1:n), 'k-', 'linewidth', 2);
		hold off;

		ylim([-0.1 1.1]);
		xlim([0 range(end)*1.05]);
		xlabel('Base position');
		ylabel('Repair efficiency (\eta)');
		
		HLEG = legend('High', 'Medium', 'Low');
		legend boxoff;
		legend('location', 'southwest')
		set(HLEG,'color','none');
		lchildren = get(HLEG, 'children');
		set(lchildren(4), "color", get(lchildren(1),'color') );
		set(lchildren(5), "color", get(lchildren(2),'color') );
		set(lchildren(6), "color", get(lchildren(3),'color') );
		set(lchildren(1:3), "markersize", 10);

		print('~/rankAssignmentCurve.png', '-r300')
		return
	end
	
	% Compare the ranks of the two datasets
	pkg load statistics
	N = hist3([labelsx,labelsy], 'ctrs', {1:3 1:3});
	% Toy examples illustrating the ideal cases
	% N = 20*ones(3,3);
	% N = 60*eye(3,3);
	% N = [57, 1, 2; 1, 55, 4; 2, 4, 54]
	
	% Image generation
	close all;
	set(0, 'defaultaxesfontsize', 30)
	imagesc(N);
	colormap("hot");
	caxis([0 max(max(max(N))-5,1)])
	set(gca,'YDir','normal');

	% Superimpose matrix entries on the plot
	[xgrid,ygrid] = meshgrid(1:3,1:3);
	xgrid = reshape(xgrid,1,[]);
	ygrid = reshape(ygrid,1,[]);
	labels = strtrim(cellstr(num2str(reshape(N,[],1))));
	text(xgrid, ygrid, labels, 'color', [0 0.9 0.2], 'HorizontalAlignment','center','VerticalAlignment','middle', 'fontsize', 50, 'FontWeight', 'bold');
	
	xlabel(xlabeltext);
	ylabel(ylabeltext);
	set(gca, 'xtick', 1:3)
	set(gca, 'ytick', 1:3)
	set(gca, 'xticklabel', {'L', 'M', 'H'});
	set(gca, 'yticklabel', {'L', 'M', 'H'});

	print(outfilename, '-r300');
	

	% one versus others
	% https://en.wikipedia.org/wiki/Phi_coefficient
	phi1 = ( N(1,1)*(N(2,2)+N(2,3)+N(3,2)+N(3,3)) - (N(1,2)+N(1,3))*(N(3,1)+N(2,1)) ) / sqrt( sum(sum(N(2:3,:)))*sum(N(1,:))*sum(sum(N(:,2:3)))*sum(N(:,1)) );
	phi2 = ( N(2,2)*(N(1,1)+N(1,3)+N(3,1)+N(3,3)) - (N(2,1)+N(2,3))*(N(1,2)+N(3,2)) ) / sqrt( sum(sum(N([1,3],:)))*sum(N(2,:))*sum(sum(N(:,[1,3])))*sum(N(:,2)) );
	phi3 = ( N(3,3)*(N(1,1)+N(1,2)+N(2,1)+N(2,2)) - (N(1,3)+N(2,3))*(N(3,1)+N(3,2)) ) / sqrt( sum(sum(N(1:2,:)))*sum(N(3,:))*sum(sum(N(:,1:2)))*sum(N(:,3)) );

	% Output the statistically relevant parameters to the console
	round([phi1, phi2, phi3]*100)/100
	sum(sum(N))
end



